/*jshint esversion: 6 */
module.exports = function (app, addon) {
    app.get('/automation/setting.do', addon.authenticate(), function (req, res) {
        res.render('setting', {
            title: "Automation Setting"
        });
    });
};