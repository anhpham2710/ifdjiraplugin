/*jshint esversion: 6 */
module.exports = function (app, addon) {
    var COMMON = require('../common/common');
    var projectURL = COMMON.TMS_API_URL + "/project/";
    var issueURL = COMMON.TMS_API_URL + "/issue/";
    var fs = require("fs");

    // Root route. This route will serve the `atlassian-connect.json` unless the
    // documentation url inside `atlassian-connect.json` is set
    app.get('/', function (req, res) {
        fs.readFile("./atlassian-connect.json", "utf-8", function (error, data) {
            if (error) throw error;
            res.setHeader('Content-Type', 'application/json');
            res.send(data);
        });
        // res.format({
        //     // If the COMMON.request content-type is text-html, it will decide which to serve up
        //     'text/html': function () {
        //         res.redirect('/atlassian-connect.json');
        //     },
        //     // This logic is here to make sure that the `atlassian-connect.json` is always
        //     // served up when COMMON.requested by the host
        //     'application/json': function () {
        //         res.redirect('/atlassian-connect.json');
        //     }
        // });
    });

    app.get('/dialog-add-issue-to-cycle', addon.authenticate(), function (req, res) {
        res.render('dialog_add_issue_to_testcycle', {
            title: "Add Issue To Test Cycle"
        });
    });

    app.get('/manual_testcase_management.do', addon.authenticate(), function (req, res) {
        var hostBaseUrl = req.context.hostBaseUrl;
        var company_name = hostBaseUrl.match(/\/\/(.+)\.atlassian.net/)[1];
        var JIRAprojectId = req.query.projectId;
        var JIRAprojectKey = req.query.pkey;
        var userId = req.query.user_id;
        var queryProjectByCompanyByProjectID = projectURL + "?company_name=" + company_name + "&JIRAproject_id=" + JIRAprojectId;
        var clientKey = req.context.clientKey;
        var httpClient = addon.httpClient(req);
        var objTestCase = [];
        var stringObjTestCase = null;
        COMMON.generateToken(queryProjectByCompanyByProjectID, clientKey, "get").then((body) => {
            var options = {
                headers: {
                    "Authorization": "JWT " + body.token
                }
            };
            COMMON.request.get(queryProjectByCompanyByProjectID, options, (err, response, project_body) => {
                projectId = JSON.parse(project_body)[0].id;
                httpClient.get('/rest/api/2/search?jql=project=\'' + JIRAprojectKey + '\'AND issuetype IN (MANUAL-TEST)&maxResults=100', (err, response, testcase_body) => {
                    var tescaseBody = JSON.parse(testcase_body);
                    if (tescaseBody.code === 404) {
                        stringObjTestCase = null;
                    } else {
                        var testcaseIssue = tescaseBody.issues;
                        testcaseIssue.forEach((value) => {
                            var JIRAticket_key = value.key;
                            var JIRAticket_title = value.fields.summary;
                            var JIRAticket_id = value.id;

                            //check issue exist in database
                            var queryIssue = issueURL + "?project_id=" + projectId + "&JIRAticket_key=" + JIRAticket_key;
                            COMMON.generateToken(queryIssue, clientKey, "get").then(function (body) {
                                var options = {
                                    headers: {
                                        "Authorization": "JWT " + body.token
                                    }
                                };
                                COMMON.request.get(queryIssue, options, function (err, response, issue_body) {
                                    if (response.statusCode == 404) {
                                        // Create new issue if this issue don't exist in database
                                        COMMON.generateToken(issueURL, clientKey, "post").then(function (body) {
                                            COMMON.request.post({
                                                url: issueURL,
                                                headers: {
                                                    "Authorization": "JWT " + body.token
                                                },
                                                json: {
                                                    "JIRAticket_id": JIRAticket_id,
                                                    "JIRAticket_key": JIRAticket_key,
                                                    "JIRAticket_title": JIRAticket_title,
                                                    "type_id": 1,
                                                    "project_id": projectId,
                                                    "created_by": userId
                                                }
                                            });
                                        });
                                    }
                                });
                            });
                            //
                            var params = JSON.stringify({
                                projectId: projectId,
                                jiraTicketId: JIRAticket_id,
                                key: JIRAticket_key,
                                summary: JIRAticket_title
                            });
                            objTestCase.push(JSON.parse(params));
                        });
                        stringObjTestCase = JSON.stringify(objTestCase);
                    }
                    res.render('manual_testcase', {
                        title: "Manual Test Case Management",
                        projectId: projectId,
                        JIRAprojectKey: JIRAprojectKey,
                        objTestCase: objTestCase,
                        stringObjTestCase: stringObjTestCase,
                    });
                });
            });
        });
    });

    app.get('/infodation-tms', addon.authenticate(), function (req, res) {
        res.render('dashboard', {
            title: "INFOdation TMS"
        });
    });

    app.get('/manual-execution', addon.authenticate(), function (req, res) {
        res.render('manual_execution', {
            title: "manual execution"
        });
    });

    app.get('/manual-testcase-panel', addon.authenticate(), function (req, res) {
        res.render('manual_testcase_panel', {
            title: "Manual"
        });
    });

    app.get('/manual-testcase-execution-panel', addon.authenticate(), function (req, res) {
        res.render('manual_testcase_execution_panel', {
            title: "Manual Execution"
        });
    });

    app.get('/manual/createManualIssue.do', addon.authenticate(), function (req, res) {
        var hostBaseUrl = req.context.hostBaseUrl;
        var company_name = hostBaseUrl.match(/\/\/(.+)\.atlassian.net/)[1];
        var JIRAprojectId = req.query.projectId;
        var JIRAprojectKey = req.query.pkey;
        var userId = req.query.user_id;
        var queryProjectByCompanyByProjectID = projectURL + "?company_name=" + company_name + "&JIRAproject_id=" + JIRAprojectId;
        var clientKey = req.context.clientKey;
        var httpClient = addon.httpClient(req);
        var objTestCase = [];
        // console.log("========== Create manual worked ============");
        COMMON.generateToken(queryProjectByCompanyByProjectID, clientKey, "get").then((body) => {

            var options = {
                headers: {
                    "Authorization": "JWT " + body.token
                }
            };
            COMMON.request.get(queryProjectByCompanyByProjectID, options, (err, response, project_body) => {
                // console.log("========== queryProjectByCompanyByProjectID ============\n");
                // console.log(queryProjectByCompanyByProjectID);
                // console.log(response);
                // console.log("========== Error ============\n");
                // console.log(err);
                // console.log("========== response ============\n");
                // console.log(response);
                // console.log("========== Project body ============\n");
                // console.log(project_body);
                projectId = JSON.parse(project_body)[0].id;
                var params = JSON.stringify({
                    projectId: projectId,
                });
                objTestCase.push(JSON.parse(params));
                res.render('create_manual_issue_dialog', {
                    projectId: objTestCase[0].projectId,
                });
            });



        });
    });
    app.get('/testcase_history.do', addon.authenticate(), function (req, res) {
        res.render('testcase_history');
    });

    // app.get('/iTMS/importer.do', addon.authenticate(), function (req, res) {
    //     res.render('importer');
    // });

    // load any additional files you have in routes and apply those to the app
    {
        // var fs = require('fs');
        var path = require('path');
        var files = fs.readdirSync("routes");
        for (var index in files) {
            var file = files[index];
            if (file === "index.js") continue;
            // skip non-javascript files
            if (path.extname(file) != ".js") continue;

            var routes = require("./" + path.basename(file));

            if (typeof routes === "function") {
                routes(app, addon);
            }
        }
    }
};