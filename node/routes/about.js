/*jshint esversion: 6 */
module.exports = function (app, addon) {
    app.get('/about-page', addon.authenticate(), function (req, res) {
        res.render('about_page', {
            title: "Comming Soon"
        });
    });
};
