/*jshint esversion: 6 */
var COMMON = require('../common/common');
module.exports = function (app, addon) {
    app.post('/addon/installed', function (req, res) {
        var settings = req.body;
        clientKey = settings.clientKey;
        addonKey = settings.key;
        sharedSecret = settings.sharedSecret;
        productType = settings.productType;
        baseUrl = settings.baseUrl;
        COMMON.request.post({
            url: COMMON.TMS_API_URL + "/addon/installed",
            headers: {
                "Authorization": req.headers.authorization
            },
            json: {
                "client_key": clientKey,
                "addon_key": addonKey,
                "shared_secret": sharedSecret,
                "product_type": productType,
                "base_url": baseUrl
            }
        });

        res.redirect('/installed');
    });

    app.post('/addon/uninstalled', addon.authenticate(), function (req, res) {
        // console.log("Removing mongo DB");
        var MongoClient = require('mongodb').MongoClient;
        MongoClient.connect('mongodb://mongodb:27017/ifdtms', function (err, db) {
            if (err) throw err;
            var collection = db.collection('AddonSettings');
            collection.remove({
                "clientKey": req.context.clientKey
            });
        });
    });
};