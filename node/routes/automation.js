/*jshint esversion: 6 */
module.exports = function (app, addon) {
    "use strict";
    var COMMON = require('../common/common');
    var ticketService = require('../services/ticket');
    var projectURL = COMMON.TMS_API_URL + "/project/";
    var issueURL = COMMON.TMS_API_URL + "/issue/";

    app.get('/automation_testcase_management.do', addon.authenticate(), function (req, res) {
        // console.log('go to automation_testcase_management.do');
        COMMON.httpClient = addon.httpClient(req);
        COMMON.clientKey = req.context.clientKey;
        COMMON.hostBaseUrl = req.context.hostBaseUrl;
        // console.log('Parameters', req.query);
        var hostBaseUrl = req.context.hostBaseUrl;
        var company_name = hostBaseUrl.match(/\/\/(.+)\.atlassian.net/)[1];
        var JIRAprojectId = req.query.projectId;
        var JIRAprojectKey = req.query.pkey;
        var userId = req.query.user_id;
        var urlProjectByCompanyByProjectID = projectURL + "?company_name=" + company_name + "&JIRAproject_id=" + JIRAprojectId;
        var clientKey = req.context.clientKey;
        var httpClient = addon.httpClient(req);
        var objTestCase = [];
        var stringObjTestCase = null;

        var arrMainAutomationDeferred = [];
        // ticketService.getIssueFromJira(urlProjectByCompanyByProjectID, clientKey)
        //     .done()
        //     .catch();
        COMMON.generateToken(urlProjectByCompanyByProjectID, clientKey, "get").then(function (body) {
            var options = {
                headers: {
                    "Authorization": "JWT " + body.token
                }
            };

            //get issue from JIRA
            COMMON.request
                .get(urlProjectByCompanyByProjectID, options, function (err, response, project_body) {
                    // console.log('go to get step definitions');
                    var projectId = JSON.parse(project_body)[0].id;
                    var urlQueryAllStepsByProjectId = COMMON.TMS_API_URL + "/steps_definition?project_id=" + projectId;

                    // get Issues
                    var defGetIssues = COMMON.Q.defer();
                    arrMainAutomationDeferred.push(defGetIssues.promise);
                    httpClient.get('/rest/api/2/search?jql=project=\'' + JIRAprojectKey + '\'AND issuetype IN (AUTO-TEST)&maxResults=100', (err, response, testcase_body) => {
                        defGetIssues.resolve();
                        var tescaseBody = JSON.parse(testcase_body);
                        if (tescaseBody.code === 404) {
                            stringObjTestCase = null;
                        } else {
                            var testcaseIssue = tescaseBody.issues;
                            testcaseIssue.forEach((value) => {
                                var JIRAticket_key = value.key;
                                var JIRAticket_title = value.fields.summary;
                                var JIRAticket_id = value.id;
                                //check issue exist in database
                                var queryIssue = issueURL + "?project_id=" + projectId + "&JIRAticket_key=" + JIRAticket_key;
                                COMMON.generateToken(queryIssue, clientKey, "get").then(function (body) {
                                    var options = {
                                        headers: {
                                            "Authorization": "JWT " + body.token
                                        }
                                    };
                                    COMMON.request.get(queryIssue, options, function (err, response, issue_body) {
                                        if (response.statusCode == 404) {
                                            // Create new issue if issue don't exist in database
                                            COMMON.generateToken(issueURL, clientKey, "post").then(function (body) {
                                                COMMON.request.post({
                                                    url: issueURL,
                                                    headers: {
                                                        "Authorization": "JWT " + body.token
                                                    },
                                                    json: {
                                                        "JIRAticket_id": JIRAticket_id,
                                                        "JIRAticket_key": JIRAticket_key,
                                                        "JIRAticket_title": JIRAticket_title,
                                                        "type_id": 2,
                                                        "project_id": projectId,
                                                        "created_by": userId
                                                    }
                                                });
                                            });
                                        }
                                    });
                                });
                                var params = JSON.stringify({
                                    projectId: projectId,
                                    jiraTicketId: JIRAticket_id,
                                    key: JIRAticket_key,
                                    summary: JIRAticket_title
                                });
                                objTestCase.push(JSON.parse(params));
                            });
                            stringObjTestCase = JSON.stringify(objTestCase);
                        }
                    });
                    var objSteps = null;
                    var steps_body = null;
                    //get step_definition from database to render GUI
                    var defGetStepDefinition = COMMON.Q.defer();
                    arrMainAutomationDeferred.push(defGetStepDefinition.promise);
                    COMMON.getAllStepDefinitionByProjectId(urlQueryAllStepsByProjectId, clientKey)
                        .then((steps) => {
                            // console.log('step definition', steps);
                            objSteps = steps;
                            steps_body = JSON.stringify(steps);
                            defGetStepDefinition.resolve();
                        }).catch(function (err) {
                            // console.log('there are error', err);
                            defGetStepDefinition.resolve();
                        });

                    COMMON.Q.all(arrMainAutomationDeferred).then(function () {
                        res.render('automation_testcase_management', {
                            title: "Automation Test Case Management",
                            projectId: projectId,
                            JIRAprojectKey: JIRAprojectKey,
                            objSteps: objSteps,
                            objTestCase: objTestCase,
                            stringObjTestCase: stringObjTestCase,
                            stringObjStep: steps_body
                        });
                    });
                });
        });
    });

    app.get('/auto-execution', addon.authenticate(), function (req, res) {
        res.render('automation_execution', {
            title: "automation execution"
        });
    });

    app.get('/automation-testcase-panel', addon.authenticate(), function (req, res) {
        var httpClient = addon.httpClient(req);
        var hostBaseUrl = req.context.hostBaseUrl;
        var company_name = hostBaseUrl.match(/\/\/(.+)\.atlassian.net/)[1];
        var JIRAprojectId = req.query.projectId;
        var JIRAprojectKey = req.query.pkey;
        var issueKey = req.query.issueKey;
        var userKey = req.query.user_key;
        var userId = req.query.user_id;
        var projectId = null;


        var queryProjectByCompanyByProjectID = projectURL + "?company_name=" + company_name + "&JIRAproject_id=" + JIRAprojectId;
        var clientKey = req.context.clientKey;

        COMMON.generateToken(queryProjectByCompanyByProjectID, clientKey, "get").then((body) => {

            var options = {
                headers: {
                    "Authorization": "JWT " + body.token
                }
            };
            COMMON.request.get(queryProjectByCompanyByProjectID, options, (err, response, project_body) => {
                projectId = JSON.parse(project_body)[0].id;
                var queryGetIssueId = issueURL + "?project_id=" + projectId + "&JIRAticket_key=" + issueKey;
                httpClient.get('/rest/api/2/issue/' + issueKey, (err, response, issue_body) => {
                    var issueBody = JSON.parse(issue_body),
                        jiraTicket_id = issueBody.id,
                        JIRAticket_title = issueBody.fields.summary;

                    // Create new issue if issue don't exist in database
                    COMMON.generateToken(issueURL, clientKey, "post").then(function (body) {
                        COMMON.request.post({
                            url: issueURL,
                            headers: {
                                "Authorization": "JWT " + body.token
                            },
                            json: {
                                "JIRAticket_id": jiraTicket_id,
                                "JIRAticket_key": issueKey,
                                "JIRAticket_title": JIRAticket_title,
                                "type_id": 2,
                                "project_id": projectId,
                                "created_by": userId
                            }
                        });
                    });
                });

                COMMON.generateToken(queryGetIssueId, clientKey, "get").then((body) => {
                    var options = {
                        headers: {
                            "Authorization": "JWT " + body.token
                        }
                    };
                    COMMON.request.get(queryGetIssueId, options, (err, response, issue_body) => {
                        var issueBody = JSON.parse(issue_body)[0];
                        var gherkinContent = '';
                        if (issueBody.cases.length > 0) {
                            gherkinContent = issueBody.cases[0].gherkin_content;
                        }
                        res.render('automation_testcase_panel', {
                            title: "Automation",
                            hostBaseUrl: hostBaseUrl,
                            userKey: userKey,
                            issueId: issueBody.id,
                            issueKey: issueKey,
                            projectKey: JIRAprojectKey,
                            JIRAprojectId: JIRAprojectId,
                            userId: userId,
                            gherkinContent: gherkinContent
                        });
                    });
                });
            });
        });
    });

    app.get('/automation-testcase-execution-panel', addon.authenticate(), function (req, res) {
        res.render('automation_testcase_execution_panel', {
            title: "Automation Execution"
        });
    });

    app.get('/automation/createAutoIssue.do', addon.authenticate(), function (req, res) {
        var hostBaseUrl = req.context.hostBaseUrl;
        var company_name = hostBaseUrl.match(/\/\/(.+)\.atlassian.net/)[1];
        var JIRAprojectId = req.query.projectId;
        var JIRAprojectKey = req.query.pkey;
        var userId = req.query.user_id;
        var queryProjectByCompanyByProjectID = projectURL + "?company_name=" + company_name + "&JIRAproject_id=" + JIRAprojectId;
        var clientKey = req.context.clientKey;
        var httpClient = addon.httpClient(req);
        var objTestCase = [];
        COMMON.generateToken(queryProjectByCompanyByProjectID, clientKey, "get").then((body) => {

            var options = {
                headers: {
                    "Authorization": "JWT " + body.token
                }
            };
            COMMON.request.get(queryProjectByCompanyByProjectID, options, (err, response, project_body) => {
                projectId = JSON.parse(project_body)[0].id;
                var params = JSON.stringify({
                    projectId: projectId,
                });
                objTestCase.push(JSON.parse(params));
                res.render('create_auto_issue_dialog', {
                    projectId: objTestCase[0].projectId,
                });
            });



        });
    });
};