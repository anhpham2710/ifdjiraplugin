/*jshint esversion: 6 */
module.exports = function (app, addon) {
    app.get('/welcome-page', addon.authenticate(), function (req, res) {
        res.render('welcome_page', {
            title: "Comming Soon"
        });
    });
};
