/*jshint esversion: 6 */
var COMMON = require('../common/common');
var companyService = require('../services/company');
var projectService = require('../services/project');
var fs = require("fs");

function createIssueType(httpClient, type) {
    var logoData;
    var IssueType;
    var ticket_name;
    var ticket_condition;
    if (type == "auto") {
        ticket_name = "AUTO-TEST";
        ticket_condition = {
            "automation": "true"
        };
        logoData = fs.createReadStream('./public/images/auto.png');
        IssueType = {
            "name": "AUTO-TEST",
            "description": "This JIRA Issue Type is used to create INFOdation Automation Tests",
            "type": "standard"
        };
    } else if (type == "manual") {
        ticket_name = "MANUAL-TEST";
        ticket_condition = {
            "manual": "true"
        };
        logoData = fs.createReadStream('./public/images/manual.png');
        IssueType = {
            "name": "MANUAL-TEST",
            "description": "This JIRA Issue Type is used to create INFOdation Manual Tests",
            "type": "standard"
        };
    }
    const ISSUE_TYPE_PATH = '/rest/api/2/issuetype/';
    httpClient.post({
        uri: ISSUE_TYPE_PATH,
        json: IssueType
    }, function (err, response, body) {
        if (err) {
            res.send("Error: " + response.statusCode + ": " + err);
        } else {
            var issueTypeId = body.id;
            httpClient.post({
                url: ISSUE_TYPE_PATH + issueTypeId + '/avatar2',
                body: logoData,
                headers: {
                    'X-Atlassian-Token': 'no-check',
                    'Content-Type': 'image/png',
                    'Content-Length': logoData.length
                }
            }, function optionalCallback(err, httpResponse, body) {
                if (err) {
                    return console.error('upload failed:', err);
                }
                var uploadedAvatarId = JSON.parse(body).id;
                httpClient.put({
                    url: ISSUE_TYPE_PATH + issueTypeId,
                    headers: {
                        'X-Atlassian-Token': 'no-check',
                        'Content-Type': 'application/json'
                    },
                    json: {
                        "avatarId": uploadedAvatarId
                    }
                }, function optionalCallback(err, res, body) {
                    if (err) {
                        return console.error('upload failed:', err);
                    }
                });
            });
            httpClient.put({
                uri: '/rest/api/2/issuetype/' + issueTypeId + '/properties/' + ticket_name,
                json: ticket_condition
            });
        }
    });
}

function outwardIssue(issue) {
    return issue
        .filter(function (obj) {
            return obj.outwardIssue;
        })
        .map(function (obj) {
            return obj.outwardIssue;
        });
}

/**
 * TODO: 
 *  need improve/clean code
 * IMPROVED: pluginEnabled.do
 */

module.exports = function (app, addon) {
    app.post('/webhook/pluginEnabled.do', addon.authenticate(), function (req, res) {
        var settings = req.body;
        var clientKey = settings.clientKey;
        var baseUrl = req.body.baseUrl;
        var company_name = baseUrl.match(/\/\/(.+)\.atlassian.net/)[1];
        var httpClient = addon.httpClient(req);
        var queryCompanyByName = COMMON.companyURL + "?company_name=" + company_name;
        // check company existed in DB
        companyService.getCompany(queryCompanyByName, clientKey)
            .done((company) => {
                    // console.log('company existed');
                    companyService.updateCompany(company_name, COMMON.companyURL, clientKey)
                        .done((company2) => {
                            // console.log('Update the status company success');
                            projectService.getAllProjectFromJira(httpClient)
                                .done((projectJira) => {
                                    // console.log('project from jira', projectJira);
                                    // console.log('==== project from jira =======');
                                    // console.log(projectJira);
                                    
                                    projectJira.forEach(element => {
                                        // console.log('==== Element ===-===');
                                        // console.log(element);
                                        var queryProjectByCompanyByProjectID = COMMON.projectURL + "?company_name=" + company_name + "&JIRAproject_id=" + element.id;
                                        projectService.checkProjectInDB(queryProjectByCompanyByProjectID, element, clientKey);
                                    });
                                });
                        });
                },
                (res) => {
                    // console.log('company not existed');
                    companyService.createCompany(company_name, COMMON.companyURL, clientKey)
                        .done((res) => {
                            // console.log('Created company success');
                            projectService.getAllProjectFromJira(httpClient)
                                .done((projectJira) => {
                                    // console.log('project from jira', projectJira);
                                    projectJira.forEach(element => {
                                        var queryProjectByCompanyByProjectID = COMMON.projectURL + "?company_name=" + company_name + "&JIRAproject_id=" + element.id;
                                        projectService.checkProjectInDB(queryProjectByCompanyByProjectID, element, clientKey);
                                    });
                                });
                        });
                });

        httpClient.get('/rest/api/2/search?jql=issuetype = "AUTO-TEST"', function (err, res, body) {
            if (res.statusCode == 400) {
                createIssueType(httpClient, "auto");
            }
        });
        httpClient.get('/rest/api/2/search?jql=issuetype = "MANUAL-TEST"', function (err, res, body) {
            if (res.statusCode == 400) {
                createIssueType(httpClient, "manual");
            }
        });
    });

    app.post('/webhook/pluginDisabled.do', addon.authenticate(), function (req, res) {
        var baseUrl = req.body.baseUrl;
        var company_name = baseUrl.match(/\/\/(.+)\.atlassian.net/)[1];
        queryCompanyByName = COMMON.companyURL + "?company_name=" + company_name;
        var clientKey = req.context.clientKey;
        COMMON.generateToken(queryCompanyByName, clientKey, "get").then(function (body) {
            var options = {
                headers: {
                    "Authorization": "JWT " + body.token
                }
            };
            COMMON.request.get(queryCompanyByName, options, function (err, response, body) {
                if (response.statusCode == 200) {
                    companyId = JSON.parse(body)[0].id;
                    var updateCompanyURL = COMMON.companyURL + companyId;
                    COMMON.generateToken(updateCompanyURL, clientKey, "put").then(function (body) {
                        COMMON.request.put({
                            url: updateCompanyURL,
                            headers: {
                                "Authorization": "JWT " + body.token
                            },
                            json: {
                                "status": "archived"
                            }
                        });
                    });

                }
            });
        });


    });

    app.post('/webhook/project_updated.do', addon.authenticate(), function (req, res) {
        var JIRAprojectId = req.body.project.id;
        var JIRAprojectKey = req.body.project.key;
        var JIRAprojectName = req.body.project.name;
        var hostBaseUrl = req.context.hostBaseUrl;
        var company_name = hostBaseUrl.match(/\/\/(.+)\.atlassian.net/)[1];
        var queryProjectByCompanyByProjectID = COMMON.projectURL + "?company_name=" + company_name + "&JIRAproject_id=" + JIRAprojectId;
        var clientKey = req.context.clientKey;

        COMMON.generateToken(queryProjectByCompanyByProjectID, clientKey, "get").then(function (body) {
            var options = {
                headers: {
                    "Authorization": "JWT " + body.token
                }
            };
            COMMON.request.get(queryProjectByCompanyByProjectID, options, function (err, response, body) {
                projectId = JSON.parse(body)[0].id;
                var updateProjectUrl = COMMON.projectURL + projectId;
                COMMON.generateToken(updateProjectUrl, clientKey, "put").then(function (body) {
                    COMMON.request.put({
                        url: updateProjectUrl,
                        headers: {
                            "Authorization": "JWT " + body.token
                        },
                        json: {
                            "JIRAproject_key": JIRAprojectKey,
                            "JIRAproject_name": JIRAprojectName
                        }
                    });
                });

            });
        });

    });

    app.post('/webhook/project_created.do', addon.authenticate(), function (req, res) {
        var JIRAprojectId = req.body.project.id;
        var JIRAprojectKey = req.body.project.key;
        var JIRAprojectName = req.body.project.name;
        var hostBaseUrl = req.context.hostBaseUrl;
        var company_name = hostBaseUrl.match(/\/\/(.+)\.atlassian.net/)[1];
        var queryCompanyByName = COMMON.companyURL + "?company_name=" + company_name;
        var clientKey = req.context.clientKey;
        COMMON.generateToken(queryCompanyByName, clientKey, "get").then(function (body) {
            var options = {
                headers: {
                    "Authorization": "JWT " + body.token
                }
            };
            COMMON.request.get(queryCompanyByName, options, function (err, response, body) {
                var companyId = JSON.parse(body)[0].id;
                COMMON.generateToken(COMMON.projectURL, clientKey, "post").then(function (body) {
                    COMMON.request.post({
                        url: COMMON.projectURL,
                        headers: {
                            "Authorization": "JWT " + body.token
                        },
                        json: {
                            "JIRAproject_key": JIRAprojectKey,
                            "JIRAproject_name": JIRAprojectName,
                            "JIRAproject_id": JIRAprojectId,
                            "company_id": companyId
                        }
                    });
                });
            });
        });

    });

    app.post('/webhook/project_deleted.do', addon.authenticate(), function (req, res) {
        var JIRAprojectId = req.body.project.id;
        var JIRAprojectKey = req.body.project.key;
        var JIRAprojectName = req.body.project.name;
        var hostBaseUrl = req.context.hostBaseUrl;
        var clientKey = req.context.clientKey;
        var company_name = hostBaseUrl.match(/\/\/(.+)\.atlassian.net/)[1];
        var queryProjectByCompanyByProjectID = COMMON.projectURL + "?company_name=" + company_name + "&JIRAproject_id=" + JIRAprojectId;
        COMMON.generateToken(queryProjectByCompanyByProjectID, clientKey, "get").then(function (body) {
            var options = {
                headers: {
                    "Authorization": "JWT " + body.token
                }
            };
            COMMON.request.get(queryProjectByCompanyByProjectID, options, function (err, response, body) {
                projectId = JSON.parse(body)[0].id;
                COMMON.generateToken(COMMON.projectURL + projectId, clientKey, "delete").then(function (body) {
                    COMMON.request.delete({
                        url: COMMON.projectURL + projectId,
                        headers: {
                            "Authorization": "JWT " + body.token
                        },
                        json: {
                            "JIRAproject_key": JIRAprojectKey,
                            "JIRAproject_name": JIRAprojectName
                        }
                    });
                });
            });
        });

    });

    app.post('/webhook/issue_updated.do', addon.authenticate(), function (req, res) {
        var hostBaseUrl = req.context.hostBaseUrl;
        var company_name = hostBaseUrl.match(/\/\/(.+)\.atlassian.net/)[1];
        userId = req.context.userId;
        JIRAprojectId = req.query.projectId;
        JIRAticket_id = req.query.issueId;
        var JIRAticket_key = req.query.issueKey;
        var JIRAticket_title = req.body.issue.fields.summary;
        issue_changelog = req.body.changelog.items;
        var issue_type_id;
        var clientKey = req.context.clientKey;
        issue_changelog.forEach(function (element, index) {

            if (element.field == "issuetype") {
                old_value = element.fromString;
                new_value = element.toString;
                if (new_value == "AUTO-TEST") {
                    issue_type_id = 2;
                } else if (new_value == "MANUAL-TEST") {
                    issue_type_id = 1;
                } else {
                    issue_type_id = 3;
                }
            }
            if (element.field == "summary") {
                JIRAticket_title = element.toString;
            }
        });

        var queryProjectByCompanyByProjectID = COMMON.projectURL + "?company_name=" + company_name + "&JIRAproject_id=" + JIRAprojectId;
        COMMON.generateToken(queryProjectByCompanyByProjectID, clientKey, "get").then(function (body) {
            var options = {
                headers: {
                    "Authorization": "JWT " + body.token
                }
            };
            COMMON.request.get(queryProjectByCompanyByProjectID, options, function (err, response, project_body) {
                projectId = JSON.parse(project_body)[0].id;
                var queryIssueByProjectIDTicketKey = issueURL + "?project_id=" + projectId + "&JIRAticket_key=" + JIRAticket_key;
                COMMON.generateToken(queryIssueByProjectIDTicketKey, clientKey, "get").then(function (body) {
                    var options = {
                        headers: {
                            "Authorization": "JWT " + body.token
                        }
                    };
                    COMMON.request.get(queryIssueByProjectIDTicketKey, options, function (err, response, issue_body) {
                        if (response.statusCode == 404 && issue_type_id != 3) {
                            // Create New Issue
                            COMMON.generateToken(issueURL, clientKey, "post").then(function (body) {
                                COMMON.request.post({
                                    url: issueURL,
                                    headers: {
                                        "Authorization": "JWT " + body.token
                                    },
                                    json: {
                                        "JIRAticket_id": JIRAticket_id,
                                        "JIRAticket_key": JIRAticket_key,
                                        "JIRAticket_title": JIRAticket_title,
                                        "type_id": issue_type_id,
                                        "project_id": projectId,
                                        "created_by": userId
                                    }
                                });
                            });

                        } else if (issue_type_id == 3) {
                            // Delete Issue
                            issueId = JSON.parse(issue_body)[0].id;
                            COMMON.generateToken(issueURL + issueId, clientKey, "delete").then(function (body) {
                                COMMON.request.delete({
                                    url: issueURL + issueId,
                                    headers: {
                                        "Authorization": "JWT " + body.token
                                    }
                                });
                            });

                        } else {
                            // Update Issue
                            issueId = JSON.parse(issue_body)[0].id;
                            COMMON.generateToken(issueURL + issueId, clientKey, "put").then(function (body) {
                                COMMON.request.put({
                                    url: COMMON.TMS_API_URL + "/issue/" + issueId,
                                    headers: {
                                        "Authorization": "JWT " + body.token
                                    },
                                    json: {
                                        "JIRAticket_title": JIRAticket_title,
                                        "type_id": issue_type_id,
                                        "modified_by": userId
                                    }
                                });
                            });

                        }
                    });
                });

            });
        });

    });

    app.post('/webhook/issue_created.do', addon.authenticate(), function (req, res) {
        var hostBaseUrl = req.context.hostBaseUrl;
        var company_name = hostBaseUrl.match(/\/\/(.+)\.atlassian.net/)[1];
        userId = req.context.userId;
        JIRAprojectId = req.query.projectId;
        var httpClient = addon.httpClient(req);
        var current_select_issue_link = req.body.issue.self;
        // console.log("current_select_issue_link", current_select_issue_link);
        var clientKey = req.context.clientKey;
        var queryProjectByCompanyByProjectID = COMMON.projectURL + "?company_name=" + company_name + "&JIRAproject_id=" + JIRAprojectId;
        COMMON.generateToken(queryProjectByCompanyByProjectID, clientKey, "get").then(function (body) {
            var options = {
                headers: {
                    "Authorization": "JWT " + body.token
                }
            };
            COMMON.request.get(queryProjectByCompanyByProjectID, options, function (err, response, body) {
                projectId = JSON.parse(body)[0].id;
                httpClient.get(current_select_issue_link, function (err, res, body) {
                    parsed_issue = JSON.parse(body);
                    issue_link = parsed_issue.fields.issuelinks;
                    issue_type = parsed_issue.fields.issuetype.name;
                    var issue_type_id;
                    if (issue_type == "AUTO-TEST") {
                        issue_type_id = 2;
                    } else if (issue_type == "MANUAL-TEST") {
                        issue_type_id = 1;
                    } else {
                        issue_type_id = 3;
                    }
                    if (Object.keys(issue_link).length === 0) {
                        // Create New Issue
                        // console.log("Create new issue ...");
                        JIRAticket_title = parsed_issue.fields.summary;
                        JIRAticket_key = parsed_issue.key;
                        JIRAticket_id = parsed_issue.id;
                        COMMON.generateToken(issueURL, clientKey, "post").then(function (body) {
                            COMMON.request.post({
                                url: issueURL,
                                headers: {
                                    "Authorization": "JWT " + body.token
                                },
                                json: {
                                    "JIRAticket_id": JIRAticket_id,
                                    "JIRAticket_key": JIRAticket_key,
                                    "JIRAticket_title": JIRAticket_title,
                                    "type_id": issue_type_id,
                                    "project_id": projectId,
                                    "created_by": userId
                                }
                            });
                        });
                    } else {
                        // Clone Issue
                        // console.log("Clone the exisitng issue ...");
                        clone_issue_link = _lodash.filter(issue_link, function (item) {
                            return item.type.name == "Cloners";
                        });
                        // console.log("clone:", clone_issue_link[0]);
                        var current_select_issue = outwardIssue(clone_issue_link)[0];
                        // console.log("current:",current_select_issue);
                        old_JIRAticket_key = current_select_issue.key;
                        old_JIRAticket_id = current_select_issue.id;
                        JIRAticket_title = parsed_issue.fields.summary;
                        var queryIssueByProjectIDTicketKey = issueURL + "?project_id=" + projectId + "&JIRAticket_id=" + old_JIRAticket_id + "&JIRAticket_key=" + old_JIRAticket_key;
                        COMMON.generateToken(queryIssueByProjectIDTicketKey, clientKey, "get").then(function (body) {
                            var options = {
                                headers: {
                                    "Authorization": "JWT " + body.token
                                }
                            };
                            COMMON.request.get(queryIssueByProjectIDTicketKey, options, function (err, response, body) {
                                if (response.statusCode !== 404) {
                                    issueID = JSON.parse(body)[0].id;
                                    new_JIRAticket_key = parsed_issue.key;
                                    new_JIRAticket_id = parsed_issue.id;
                                    COMMON.generateToken(issueURL + issueID + "/clone", clientKey, "post").then(function (body) {
                                        COMMON.request.post({
                                            url: issueURL + issueID + "/clone",
                                            headers: {
                                                "Authorization": "JWT " + body.token
                                            },
                                            json: {
                                                "JIRAticket_id": new_JIRAticket_id,
                                                "JIRAticket_key": new_JIRAticket_key,
                                                "JIRAticket_title": JIRAticket_title,
                                                "type_id": issue_type_id,
                                                "project_id": projectId,
                                                "created_by": userId
                                            }
                                        });
                                    });
                                }
                            });
                        });
                    }
                });
            });
        });

    });

    app.post('/webhook/issue_deleted.do', addon.authenticate(), function (req, res) {
        var hostBaseUrl = req.context.hostBaseUrl;
        var company_name = hostBaseUrl.match(/\/\/(.+)\.atlassian.net/)[1];
        userId = req.context.userId;
        issue_type = req.body.issue.fields.issuetype.name;
        JIRAprojectId = req.query.projectId;
        project_key = req.query.pkey;
        JIRAticket_id = req.query.issueId;
        JIRAticket_key = req.query.issueKey;
        var queryProjectByCompanyByProjectID = COMMON.projectURL + "?company_name=" + company_name + "&JIRAproject_id=" + JIRAprojectId;
        var clientKey = req.context.clientKey;
        COMMON.generateToken(queryProjectByCompanyByProjectID, clientKey, "get").then(function (body) {
            var options = {
                headers: {
                    "Authorization": "JWT " + body.token
                }
            };
            COMMON.request.get(queryProjectByCompanyByProjectID, options, function (err, response, body) {
                projectId = JSON.parse(body)[0].id;
                var querytIssueByProjectIDByTicketKey = issueURL + "?project_id=" + projectId + "&JIRAticket_key=" + JIRAticket_key;
                COMMON.generateToken(querytIssueByProjectIDByTicketKey, clientKey, "get").then(function (body) {
                    COMMON.request.get(querytIssueByProjectIDByTicketKey, options, function (err, response, issue_body) {
                        issueId = JSON.parse(issue_body)[0].id;
                        COMMON.generateToken(issueURL + issue_id, clientKey, "delete").then(function (body) {
                            COMMON.request.delete({
                                url: issueURL + issue_id,
                                headers: {
                                    "Authorization": "JWT " + body.token
                                }
                            });
                        });
                    });
                });

            });
        });

    });
};