/// <reference path="configuration_services.js" />
var typeParam = 'Parameters';
var typeObject = 'ObjectRepositories';
var typeTestData = 'TestData';
var typeStepsDefinition = 'StepsDefinition';
const topNumberRow = 5;
var tmplChildRowView = '';
var tmplDataSteps = '';
const regexForName = /do \|.*\|/g;
const regexForContent = /{.*}/g;
const localStorageStepsDefinition = 'AllStepsDefinition';
var tmplSearhResult = '';
var isSearchingSteps = false;
dataInSearch = null;
const optionCodeMirrorTestData = {
    lineNumbers: false,
    mode: "text/x-sql",
    theme: "eclipse",
    autoRefresh: true
};
var arrParams = null,
    arrObjects = null,
    arrTestData = null,
    arrSteps = null,
    editorTestData = null;
const optionsFuzzySearch = {
    shouldSort: true,
    threshold: 0.6,
    location: 0,
    distance: 100,
    maxPatternLength: 32,
    minMatchCharLength: 3,
    keys: [
        "name",
        "content"
    ]
};
$(function () {
    firstLoadPage();
    function firstLoadPage(){
        $.get(urlTemplateRender + 'paging.html', function (template) {
            tmpPaging = template;
        });
        //get html template for child tab [Parameters,Objec repositories]    
        $.get(urlTemplateRender + 'auto-config-params-objects.html', function (template) {
            // tmplChildPanel = template;
            $('#parameter-content').html(Mustache.render(template, {
                placeholder: 'Search parameters...',
                idElementSearchGlobalData: 'txtSearchParams'
            }));
            $('#object-repositories-content').html(Mustache.render(template, {
                placeholder: 'Search object repositories...',
                idElementSearchGlobalData: 'txtSearchObjects'
            }));
            //get template row
            $.get(urlTemplateRender + 'auto-config-view-params-objects.html', function (template) {          
                tmplChildRowView = '{{#objects}}' + template + '{{/objects}}';
            });
        });
        $.get(urlTemplateRender + 'auto-config-data-steps.html', function (template) {
            $('#test-data-content').html(Mustache.render(template, {
                placeholder: 'Search test data...',
                idElementSearchGlobalData: 'txtSearchTestData'
            }));
            $('#steps-definition-content').html(Mustache.render(template, {
                placeholder: 'Search steps definition...',
                idElementSearchGlobalData: 'txtSearchStep'
            }));
        //     //get template row
            $.get(urlTemplateRender + 'auto-config-view-data-steps.html', function (template) {
                tmplDataSteps = '{{#objects}}' + template + '{{/objects}}';
            });
        });
        $.get(urlTemplateRender + 'no-data.html', function (template) {
            tmpNodata = template;
        });
        $.get(urlTemplateRender + 'list-result-search.html', function (template) {
            tmplSearhResult = template;
        });
    }
    InitService();
    function InitService() {

        getProjectDetail().done(function (project_response) {
            project_id = project_response[0].id;
            
            getParameters(project_id).done(function(res){
                loadAllParams(res);
            }).catch(function (err) {
                if (err.status == 404) {
                    $('#parameter-content').find('.grid-body .spinner').hide();
                    $('#parameter-content').find('.grid-body .no-data').append('<p>No data to show</p>');
                }
            });

            getObjectRepos(project_id).done(function(res){
                loadAllObjectRepos(res);
            }).catch(function (err) {
                if (err.status == 404) {
                    $('#object-repositories-content').find('.grid-body .spinner').hide();
                    $('#object-repositories-content').find('.grid-body .no-data').append('<p>No data to show</p>');
                }
            });
            
            getTestData(project_id).done(function(res){
                loadAllTestData(res);
            }).catch(function (err) {
                if (err.status == 404) {
                    $('#test-data-content').find('.grid-body .spinner').hide();
                    $('#test-data-content').find('.grid-body .no-data').append('<p>No data to show</p>');
                }
            });

            getStepsDefinitionByProject(project_id).done(function (res) {
                loadAllStepsDefinition(res);
                $('#refreshConfiguration .fas').removeClass('fa-spin');
            }).catch(function (err) {
                $('#refreshConfiguration .fas').removeClass('fa-spin');
                if (err.status == 404) {
                    $('#steps-definition-content').find('.grid-body .spinner').hide();
                    $('#steps-definition-content').find('.grid-body .no-data').append('<p>No data to show</p>');
                }
            });
        });

        $('body').on('keyup', '#txtSearchParams', function(){
            searchGlobalData('Parameters');
        });

        $('body').on('keyup', '#txtSearchObjects', function(){
            searchGlobalData('ObjectRepositories');
        });

        $('body').on('keyup', '#txtSearchTestData', function(){
            searchGlobalData('TestData');
        });

        $('body').on('keyup', '#txtSearchStep', function(){
            searchGlobalData('StepsDefinition');
        });
    }

    function getParameters(project_id){
        return ajaxRequest(urlParameter, 'GET', {
            project_id: project_id
        });
    }

    function loadAllParams(res){
        var _res = _.forEach(res, function (o) {
            o.globalType = typeParam;
        });
        arrParams = _res;
        var objListData = {
            objects: paginate(_res, topNumberRow, 1)
        };
        var rendered = Mustache.render(tmplChildRowView, objListData);
        $('#parameter-content').find('.grid-body').html(rendered);
        calculatePaging('#parameter-content', typeParam, arrParams);
    }

    function getObjectRepos(project_id){
        return ajaxRequest(requestURL + '/object_repository', 'GET',{
            project_id: project_id
        });
    }
    
    function loadAllObjectRepos(res){
        var _res = _.forEach(res, function (o) {
            o.globalType = typeObject;
        });
        arrObjects = _res;
        var objListData = {
            objects: paginate(_res, topNumberRow, 1)
        };
        var rendered = Mustache.render(tmplChildRowView, objListData);
        $('#object-repositories-content').find('.grid-body').html(rendered);
        calculatePaging('#object-repositories-content', typeObject, arrObjects);
    }

    function getTestData(res){
        return ajaxRequest(requestURL + '/data_test', 'GET',{
            project_id: project_id
        });
    }

    function loadAllTestData(res){
        var _res = _.forEach(res, function (o) {
            o.globalType = typeTestData;
        });
        var objListData = {
            objects: paginate(_res, topNumberRow, 1)
        };
        arrTestData = _res;
        var rendered = Mustache.render(tmplDataSteps, objListData);
        $('#test-data-content').find('.grid-body').html(rendered);
        calculatePaging('#test-data-content', typeTestData, arrTestData);
        renderCodeMirrorTestData($('#test-data-content'));
    }

    function getStepsDefinitionByProject(project_id) {
        return ajaxRequest(requestURL + '/steps_definition', 'GET', {
            project_id: project_id
        });
    }

    function loadAllStepsDefinition(res) {
        var _res = _.forEach(res, function (o) {
            o.globalType = typeStepsDefinition;
        });
        var objListData = {
            objects: paginate(_res, topNumberRow, 1)
        };
        arrSteps= _res;
        var rendered = Mustache.render(tmplDataSteps, objListData);
        $('#steps-definition-content').find('.grid-body').html(rendered);
        calculatePaging('#steps-definition-content', typeStepsDefinition, arrSteps);
        renderCodeMirrorTestData($('#steps-definition-content'));
    }

    function reloadConfiguration() {
        $('#refreshConfiguration .fas').addClass('fa-spin');
        firstLoadPage();
        InitService();
    }
    
    $('#refreshConfiguration').on('click', function(){
        reloadConfiguration();
    })
});

function viewTestDataContent(that) {
    var form = $(that).closest('form');
    // $(that).toggleClass('active');
    form.find('.row-test-data-content').slideToggle();
}

function renderCodeMirrorTestData(ele) {
    var textareas = ele.find('.textarea-test-data');
    var arrCodeMirror = [];
    for (var i = 0; i < textareas.length; i++) {
        var mirror = CodeMirror.fromTextArea(textareas[i], optionCodeMirrorTestData);
        mirror.setOption("readOnly", true);
        arrCodeMirror.push(mirror);
    }
    arrCodeMirror.forEach(function (editor) {
        editor.on('keyup', function () {
            editor.save();
            $(editor.getTextArea()).trigger('keyup');
        });
    });
}

function searchGlobalData(globalType) {
    'use strict';
    var fuseSearch = null;
    var result = null;
    var rendered = null;
    var objListData = null;

    function showResult(element, result) {
        var _res = _.forEach(result, function (o) {
            o.globalType = typeTestData;
        });

        objListData = {
            objects: paginate(_res, topNumberRow, 1)
        };

        if (result.length > 0) {
            switch (globalType) {
                case typeParam:
                    rendered = Mustache.render(tmplChildRowView, objListData);
                    $(element).find('.grid-body').html(rendered);
                    break;
                case typeObject:
                    rendered = Mustache.render(tmplChildRowView, objListData);
                    $(element).find('.grid-body').html(rendered);
                    break;
                case typeTestData:
                    rendered = Mustache.render(tmplDataSteps, objListData);
                    $(element).find('.grid-body').html(rendered);
                    break;
                case typeStepsDefinition:
                    rendered = Mustache.render(tmplDataSteps, objListData);
                    $(element).find('.grid-body').html(rendered);
                    break;
            }
        } else {
            $(element).find('.grid-body').html($(tmpNodata));
        }
    }

    switch (globalType) {
        case typeParam:
            fuseSearch = new Fuse(arrParams, optionsFuzzySearch);
            if ($('#txtSearchParams').val().trim() != '') {
                result = fuseSearch.search($('#txtSearchParams').val());
            } else{
                result = arrParams;
            }
            showResult('#parameter-content', result);
            dataInSearch = result;
            calculatePaging('#parameter-content', typeParam, result);
            break;
        case typeObject:
            fuseSearch = new Fuse(arrObjects, optionsFuzzySearch);
            if ($('#txtSearchObjects').val().trim() != '') {
                result = fuseSearch.search($('#txtSearchObjects').val());
            } else
                result = arrObjects;
            showResult('#object-repositories-content', result);
            dataInSearch = result;
            calculatePaging('#object-repositories-content', typeObject, result);
            break;
        case typeTestData:
            fuseSearch = new Fuse(arrTestData, optionsFuzzySearch);
            if ($('#txtSearchTestData').val().trim() != '') {
                result = fuseSearch.search($('#txtSearchTestData').val());
            } else
                result = arrTestData;
            showResult('#test-data-content', result);
            dataInSearch = result;
            calculatePaging('#test-data-content', typeTestData, result);
            renderCodeMirrorTestData('#test-data-content');
            break;
        case typeStepsDefinition:
            fuseSearch = new Fuse(arrSteps, optionsFuzzySearch);
            if ($('#txtSearchStep').val().trim() != '') {
                result = fuseSearch.search($('#txtSearchStep').val());
            } else
                result = arrSteps;
            showResult('#steps-definition-content', result);
            dataInSearch = result;
            calculatePaging('#steps-definition-content', typeStepsDefinition, result);
            renderCodeMirrorTestData('#steps-definition-content');
            break;
    }
}

function coppyClipboard(that){
    'use strict';
    var valueInput = $(that).parent().find('input').select();
    document.execCommand("copy");
    $(that).parent().find('input').blur();
    showNotify('Copied!',"custom-1");
}

function displayPage(that) {
    'use strict';
    var globalType = $(that).data('type');
    var index = $(that).data('index');
    var data = null;

    function showData(element, dataShow) {
        var rendered = '';
        if (globalType == typeTestData || globalType === typeStepsDefinition) {
            rendered = Mustache.render(tmplDataSteps, dataShow);
            $(element).find('.grid-body').html(rendered);
            var textareas = document.getElementsByClassName('textarea-test-data');
            renderCodeMirrorTestData(element);
        } else {
            rendered = Mustache.render(tmplChildRowView, dataShow);
            $(element).find('.grid-body').html(rendered);
        }
    }

    $(that).closest('.pagination').find('.isactive').removeClass('isactive');
    $(that).parent().addClass('isactive');
    // showHideBtnNextPage(that);
    // showHideBtnPrevPage(that);

    switch (globalType) {
        case typeParam:
            if (dataInSearch != null) {
                data = paginate(dataInSearch, topNumberRow, index);
            } else {
                data = paginate(arrParams, topNumberRow, index);
            }
            showData('#parameter-content', {objects: data});
            break;
        case typeObject:
            if (dataInSearch != null) {
                data = paginate(dataInSearch, topNumberRow, index);
            } else {
                data = paginate(arrObjects, topNumberRow, index);
            }
            showData('#object-repositories-content', {objects: data});
            break;
        case typeTestData:
            if (dataInSearch != null) {
                data = paginate(dataInSearch, topNumberRow, index);
            } else {
                data = paginate(arrTestData, topNumberRow, index);
            }
            showData('#test-data-content', {objects: data});          
            break;
        case typeStepsDefinition:
            if (dataInSearch != null) {
                data = paginate(dataInSearch, topNumberRow, index);
            } else {
                data = paginate(arrSteps, topNumberRow, index);
            }
            showData('#steps-definition-content', {objects: data});           
            break;
    }
}

function calculatePaging(ele, globalType, dataArray) {
    'use strict';
    if (dataArray.length == 0 || dataArray.length <= topNumberRow) {
        $(ele).find('.paging nav').remove();
        return;
    } 
    var paging = '';
    var totalPage = parseInt(dataArray.length / topNumberRow) + (dataArray.length % topNumberRow > 0 ? 1 : 0);
    for (var i = 1; i <= totalPage; i++) {
        var _active = '';
        if (i == 1) {
            _active = 'isactive';
        }
        paging += '<li class="page-item ' + _active + '"><a onclick="displayPage(this)" data-index="' + i + '" data-type="' + globalType + '" class="page-link" href="javascript:;">' + i + '</a></li>';
    }

    var rendered = Mustache.to_html(tmpPaging, {
        pagings: paging,
        totalPage : totalPage,
        globalType: globalType
    });
    $(ele).find('.paging').html(rendered);
}

function nextPage(that) {
    var ul = $(that).closest('.pagination');
    var indexActive = ul.find('.isactive').find('.page-link').data('index') + 1;
    var nextEleActive = ul.find('.page-link[data-index="' + indexActive + '"]');
    displayPage(nextEleActive);
}

function previousPage(that) {
    var ul = $(that).closest('.pagination');
    var indexActive = ul.find('.isactive').find('.page-link').data('index') - 1;
    var nextEleActive = ul.find('.page-link[data-index="' + indexActive + '"]');
    displayPage(nextEleActive);
}

function showHideBtnNextPage(that){
    var ul = $(that).closest('.pagination');
    var totalPage = ul.attr('data-total-page');
    var indexActive = ul.find('.isactive').find('.page-link').data('index');
    if(indexActive >= totalPage ){
       ul.find('li:last-child').hide();
    } else{
        ul.find('li:last-child').show();
    }
} 

function showHideBtnPrevPage(that){
    var ul = $(that).closest('.pagination');
    var indexActive = ul.find('.isactive').find('.page-link').data('index');
    if(indexActive <= 1 ){
        ul.find('li:first-child').hide();
    } else{
        ul.find('li:first-child').show();
    }
}