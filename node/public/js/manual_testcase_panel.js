/// <reference path="./common/constants.js" />
$(function () {
    var issueId = null;
    var project_id = null;
    var userId = paramsUrl.user_id;

    var tableVersion = $('#tableVersion');
    var addNewStep = $('#addNewStep');
    var currentIndex = null;
    var datatable_tableversion = null;

    init();

    function init() {
        // get project detail
        getProjectDetail().done(function (res) {
            var project = res[0];
            datatable_tableversion = $('#main-tableVersion').DataTable({
                data: null,
                paging: false,
                searching: false,
                info: false,
                ordering: false,
                autoWidth: false,
                language: {
                    emptyTable: '',
                    ZeroRecords: ''
                },
                rowReorder: true,
                columns: [{
                        data: null,
                        class: 'order_id',
                        defaultContent: ''
                    },
                    {
                        className: 'test_step',
                        data: 'test_step',
                        render: function (data, type, row, meta) {
                            if (data != null) {
                                var rplData = data.replace(/(?:\r\n|\r|\n)/g, '<br />');
                                return '<p class="m-0 p-0">' + rplData + '</p>';
                            } else {
                                return '<p class="m-0 p-0">' + data + '</p>';
                            }
                        }
                    },
                    {
                        className: 'test_data',
                        data: 'test_data',
                        render: function (data, type, row, meta) {
                            if (data != null) {
                                var rplData = data.replace(/(?:\r\n|\r|\n)/g, '<br />');
                                return '<p class="m-0 p-0">' + rplData + '</p>';
                            } else {
                                return '<p class="m-0 p-0">' + data + '</p>';
                            }
                        }
                    },
                    {
                        className: 'expected_result',
                        data: 'expected_result',
                        render: function (data, type, row, meta) {
                            if (data != null) {
                                var rplData = data.replace(/(?:\r\n|\r|\n)/g, '<br />');
                                return '<p class="m-0 p-0">' + rplData + '</p>';
                            } else {
                                return '<p class="m-0 p-0">' + data + '</p>';
                            }
                        }
                    },
                    {
                        data: null,
                        width: '41px',
                        className: 'text-right functional-button',
                        defaultContent: addButtonsFunctional('normal')
                    }
                ],
                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).on('click', 'td:not(:first-child):not(:last-child)', function () {
                        var tr = $(this).parent('tr');
                        if (tr.hasClass('editing')) {
                            return;
                        }
                        tr.addClass('editing');
                        var heightStep = tr.find('.test_step').height();
                        var heightData = tr.find('.test_data').height();
                        var heightResult = tr.find('.expected_result').height();
                        tr.find('p').hide();
                        var data = datatable_tableversion.row(tr).data();
                        tr.find('.test_step').append('<textarea style="resize: auto; min-height: ' + heightStep + 'px;  max-width: 250px; min-width:190px"; position: fixed; name="step" data-field-name="step" class="form-control">' + $.trim(data.test_step) + '</textarea>');
                        tr.find('.test_data').append('<textarea style="resize: auto; min-height: ' + heightData + 'px;  max-width: 250px; min-width:190px"; position: fixed; name="data" data-field-name="data" class="form-control">' + $.trim(data.test_data) + '</textarea>');
                        tr.find('.expected_result').append('<textarea style="resize: auto; min-height: ' + heightResult + 'px;  max-width: 250px; min-width:190px"; position: fixed; name="result" data-field-name="result" class="form-control">' + $.trim(data.expected_result) + '</textarea>');
                        tr.find('.dropdownMenuButton').hide();
                        tr.find('.functional-button').append(addButtonsFunctional('editing'));
                    });

                    $(nRow).on('click', '#btnCancelStep', function () {
                        var tr = $(this).parents('tr');
                        resetRow(tr);
                    });

                    $(nRow).on('click', '#btnCancelDelete', function () {
                        var tr = $(this).parents('tr');
                        resetRow(tr);
                    });

                    var index = iDisplayIndex + 1;
                    $('td:eq(0)', nRow).html(index);

                    return nRow;
                }
            });
            // get issue detail
            projectId = project.id;
            project_id = project.id;
            console.log('projectid', projectId);
            getIssueDetailTestCase(project.id);
            showPanelDetail();
            showSpinner(false);
            $('#spinnerScenario').addClass('spinner-content');
        });
    }

    function getIssueDetailTestCase(_projectId) {
        $('#refresh-data .fas').addClass('fa-spin');
        ajaxRequest(requestAPI.getIssueDetail, 'GET', {
            project_id: _projectId,
            JIRAticket_key: issueKey
        }).done(function (res) {
            issueId = res[0].id;
            var testcase = res[0].cases;
            datatable_tableversion.clear();
            datatable_tableversion.rows.add(testcase);
            datatable_tableversion.columns.adjust().draw(false);
            $('#refresh-data .fas').removeClass('fa-spin');
            showPanelDetail();
        }).catch(function (err) {
            $('#refresh-data .fas').removeClass('fa-spin');
            if (err.status == 404) {
                // first time open jira ticket link , but issue is not existed in DB.

                // get Issue from JIRA
                AP.request('/rest/api/2/issue/' + issueKey, {
                    success: function (res) {
                        var resData = JSON.parse(res);
                        var data = {
                            "JIRAticket_id": resData.id,
                            "JIRAticket_key": resData.key,
                            "JIRAticket_title": resData.fields.summary,
                            "type_id": 1,
                            "project_id": project_id,
                            "created_by": userId
                        };
                        createIssueFromJira(data).done(function (res) {
                            console.log('res create new ', res);
                            issueId = res.id;
                        });
                    }
                });

            }
        });
    }

    function resetTable() {
        var selectedRow = tableVersion.find('tr').eq(currentIndex);
        selectedRow.find('td').remove();
        selectedRow.append(oldTds);
    }

    function resetRow(tr) {
        tr.removeClass('editing');
        tr.find('p').show();
        // var data = datatable_tableversion.row(tr).data();
        tr.find('textarea').remove();
        tr.find('.dropdownMenuButton').show();
        tr.find('#fcBtn').remove();
        tr.find('#fcBtn_delete').remove();
        tr.find('.error').remove();
    }

    function addButtonsFunctional(typeButton) {
        if (typeButton == 'editing') {
            return '<div id="fcBtn"><button id="btnUpdateStep" type="button" class="btn btn-link"><span class="fa fa-check"></span></button>' +
                '<button id="btnCancelStep" type="button" class="btn btn-link"><span class="fa fa-times"></span></button></div>';

        }

        if (typeButton == 'normal') {
            return '<div class="dropdown">' +
                '<button class="btn btn-link dropdown-toggle dropdownMenuButton" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                '<span class="fa fa-cog"></span></button>' +
                '<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">' +
                '<button class="dropdown-item btn-clone-testcase" type="button"><span class="fa fa-clone"></span> Clone</button >' +
                '<button class="dropdown-item btn-delete-testcase" type="button"><span class="fa fa-trash-o"></span> Delete</button >' +
                '</div></div>';
        }

        if (typeButton == 'confirmDelete') {
            return '<div id="fcBtn_delete">' +
                '<button id="btnCancelDelete" type="button" class="btn btn-danger" title="Delete"><span class="fas fa-times"></span></button>&nbsp;&nbsp;' +
                '<button id="btnDeleteYes" type="button" class="btn btn-success" title="Cancel"><span class="fas fa-check"></span></button>' + '</div>';
        }
    }

    $('body').on('keyup', 'form textarea', function () {

        removeAllValid(this);

    });

    $('body').on('keyup', 'tr textarea', function () {

        removeAllValidForTrTable(this);

    });

    $('#btnAddStep').click(function () {
        var parentDiv = $('#addNewStep');

        var that = this;
        var validate = null;
        parentDiv.find('form textarea').each(function () {
            if ($(this).val().trim() != '') {
                validate = true;
            } else {
                validate = false;
            }
            if (validate) return false;
        });

        if (validate) {
            $(that).html('<span class="fa fa-spinner fa-pulse"></span>');
            $(that).attr('disabled', true);
            var paramsData = JSON.stringify({
                issue_id: issueId,
                test_step: parentDiv.find("[data-field-name=step]").val(),
                test_data: parentDiv.find("[data-field-name=data]").val(),
                expected_result: parentDiv.find("[data-field-name=result]").val(),
                created_by: currentUser.emailAddress,
                order_id: datatable_tableversion.data().count() + 1
            });
            ajaxRequest(requestAPI.manualTestCase, 'POST', paramsData).done(function (res) {
                datatable_tableversion.row.add(res);
                datatable_tableversion.columns.adjust().draw(false);
                parentDiv.find("[data-field-name=step]").val('');
                parentDiv.find("[data-field-name=data]").val('');
                parentDiv.find("[data-field-name=result]").val('');
                parentDiv.find('.error').remove();
                $(that).html('<span class="fa fa-plus-circle"></span>');
                $(that).removeAttr('disabled');
                showPanelDetail();
            });
        } else {
            var isValid = false;
            var strValue = $(that).val();
             console.log("strValue", strValue);
            if ($.trim(strValue) === '') {
                     isValid = false;
                     if ($(that).parent().has("label.error").length) {
                      return isValid;
                  }
                  $(that).closest('form').find('textarea').addClass('is-invalid');
                    $(that).addClass('is-invalid');
                    $(that).parent().append('<label class="error invisible"></label>');
             } else {
                    $(that).removeClass('is-invalid');
                    $(that).parent().find('label.error').remove();
                     isValid = true;
             }
    
    return isValid;
            // parentDiv.find('form').valid();

        }
    });

    function importSingleValueManual() {
        var fileImport = $('#fileImport')[0].files[0];
        var sheetName = $('#sheetName').val();
        var testStep = $('#testStep').val();
        var testData = $('#testData').val();
        var exceptedResult = $('#exceptedResult').val();
        var formData = new FormData();
        formData.append('datafile', fileImport);
        formData.append('sheet_name', sheetName);
        formData.append('excel_column_test_step', testStep);
        formData.append('excel_column_test_data', testData);
        formData.append('excel_column_expected_result', exceptedResult);
        formData.append('project_id', jiraProjectId);
        ajaxRequestImport(requestURL + '/import/manual_testcase/' + issueId, 'POST', formData).done(function (res) {
            var listNewTestCase = res.message;
            if (Array.isArray(res.message)) {
                renderGUITestCase(listNewTestCase);
                showPanelDetail();
                showLoadingBar(false);
                showNotifyCustoms('Import success', 'success', 'right', 2000);
            } else {
                showLoadingBar(false);
                $.notify(res.message, "warn");
            }
        }).catch(function (err) {
            showLoadingBar(false);
            showNotifyCustoms(err.responseJSON.error, 'danger', 'right', 2000);
        });
    }

    function showPanelDetail() {
        $('#action-selected').show();
        $('#no-action-selected').hide();
        $('#panelImport').hide();
    }

    function showPanelImport() {
        $('#panelImport').show();
        $('#action-selected').hide();
        $('#no-action-selected').hide();
    }
    $('#importThisTestCase').click(function () {
        $('#testCaseName').closest('.form-group').addClass('d-none');
        $('#jiraTicketKey').closest('.form-group').addClass('d-none');
        $('#submitFormManual').addClass('import-single');
        showPanelImport();
    });
    $('#cancelFormManual').click(function () {
        showPanelDetail();
    });
    $('#submitFormManual').on('click', function () {
        if (requiredInput($(this))) {
            showLoadingBar(true);
            $(this).hasClass('import-single')
            importSingleValueManual();
        }
    });

    function requiredInput(that) {
        that.closest('.validate-form').find('input').each(function () {
            if ($(this).val() === '') {
                $(this).addClass('error');
            } else {
                $(this).removeClass('error');
            }
        });
        if (that.closest('.validate-form').find('input.error').length > 0) {
            return false;
        } else {
            return true;
        }
    }

    function renderGUITestCase(testcase) {
        datatable_tableversion.clear();
        datatable_tableversion.rows.add(testcase);
        datatable_tableversion.columns.adjust().draw(false);
    }

    $('#export-data').on('click', function () {
        var _projectId = jiraProjectId;
        var _jiraTicketKey = issueKey;
        ExportReportSinggle(_projectId, _jiraTicketKey);
    });

    $('#main-tableVersion').on('click', '#btnUpdateStep', function () {
        var _tr = $(this).parents('tr');

        var validate = null;
        _tr.find('textarea').each(function () {
            if ($(this).val().trim() != '') {
                validate = true;
                console.log("validate", validate);
            } else {
                validate = false;
            }
            if (validate) return false;
        });

        if (validate) {
            $(this).html('<span class="fa fa-spinner fa-pulse"></span>');
            var data = datatable_tableversion.row(_tr).data();
            var paramsData = JSON.stringify({
                test_step: _tr.find('[data-field-name=step]').val(),
                test_data: _tr.find('[data-field-name=data]').val(),
                expected_result: _tr.find('[data-field-name=result]').val(),
                modified_by: currentUser.emailAddress
            });
            ajaxRequest(requestAPI.manualTestCase + data.id, 'PUT', paramsData).done(function (res) {
                resetRow(_tr);
                datatable_tableversion.row(_tr).data(res);
                datatable_tableversion.columns.adjust().draw(false);
            });
        } else {
            _tr.find('textarea').each(function () {
                validationInput(this);
            });
        }
    });

    $('#main-tableVersion').on('click', '.btn-clone-testcase', function () {
        var _tr = $(this).parents('tr');
        var data = datatable_tableversion.row(_tr).data();
        var paramsData = JSON.stringify({
            issue_id: issueId,
            test_step: data.test_step,
            test_data: data.test_data,
            expected_result: data.expected_result,
            modified_by: currentUser.emailAddress,
            created_by: currentUser.emailAddress,
            order_id: datatable_tableversion.data().count() + 1
        });

        ajaxRequest(requestAPI.manualTestCase, 'POST', paramsData).done(function (res) {
            datatable_tableversion.row.add(res);
            datatable_tableversion.columns.adjust().draw(false);
        });
    });

    $('#main-tableVersion').on('click', '.btn-delete-testcase', function () {
        var _tr = $(this).parents('tr');
        var data = datatable_tableversion.row(_tr).data();
        _tr.find('td.functional-button').append(addButtonsFunctional('confirmDelete'));
    });

    $('#main-tableVersion').on('click', '#btnDeleteYes', function () {
        $(this).html('<span class="fa fa-spinner fa-pulse"></span>');
        $(this).attr('disabled', true);
        var _tr = $(this).parents('tr');
        var data = datatable_tableversion.row(_tr).data();
        ajaxRequest(requestAPI.manualTestCase + data.id, 'DELETE').done(function (res) {
            datatable_tableversion.row(_tr).remove();
            datatable_tableversion.columns.adjust().draw(false);
        });
    });

    $('#main-tableVersion').on('click', '#refresh-data', function () {
        getIssueDetailTestCase(project_id);
    });


    $('.add-new-content').on('change keyup keydown paste cut', function () {
        $(this).height(0).height(this.scrollHeight);
    }).change();

    // test-step, test-data, expected-result are not limit
    $('input[type="text"], textarea').removeAttr('maxlength');

    var idTableVersion = document.getElementById('tableVersion');
    Sortable.create(idTableVersion, {
        animation: 150,
        onUpdate: function () {
            var arrId = [];
            var arrOrder = [];
            $('#tableVersion').find('>tr').each(function () {
                var _tr = $(this);
                var orderId = $(this).index() + 1;
                var data = datatable_tableversion.row(_tr).data();
                arrId.push(data.id);
                arrOrder.push(orderId);
            });
            var paramsData = JSON.stringify({
                order_ids: arrOrder,
                testcase_ids: arrId
            });
            ajaxRequest(requestAPI.manualTestCase + issueId + '/update_order_id', 'PUT', paramsData).done(function (res) {

            });
        }
    });
});

function removeAllValid(that) {
    if(that.value.trim() !== ''){
        $(that).closest('form').find('textarea').removeClass('is-invalid');
        $(that).parent().find('label.error').remove();
    console.log("that", that.value);
    }else{
        $(that).addClass('is-invalid');
    }
    
}

function removeAllValidForTrTable(that) {
    $(that).closest('tr').find('textarea').removeClass('is-invalid');
    $(that).parent().find('label.error').remove();
}

function validationInput(that) {
    var isValid = false;
    var strValue = $(that).val();
    console.log("strValue", strValue);
    if ($.trim(strValue) === '') {
        isValid = false;
        if ($(that).parent().has("label.error").length) {
            return isValid;
        }
        $(that).addClass('is-invalid');
        $(that).parent().append('<label class="error invisible"></label>');
    } else {
        $(that).removeClass('is-invalid');
        $(that).parent().find('label.error').remove();
        isValid = true;
    }
    return isValid;
}