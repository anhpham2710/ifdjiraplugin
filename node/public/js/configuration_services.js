const urlObjectRepositories = requestURL + '/object_repository';
const urlParameter = requestURL + '/data_global';
const urlTestData = requestURL + '/data_test';

var jiraKey = null;
var p_id = $('#projectId').val();

//#region Object Repositories 
function getObjecRepositories(pageSize) {
    var deffer = $.Deferred();
    ajaxRequest(urlObjectRepositories, "GET", {
        project_id: p_id
    }).done(function (res) {
        deffer.resolve(res);
    }).catch(function (err) {
        deffer.reject(err);
    });
    return deffer.promise();
}

function createNewObjectRepository(postData) {
    var _postData = Object.assign({
        project_id: p_id
    }, postData);
    return ajaxRequest(urlObjectRepositories, "POST", JSON.stringify(_postData));
}

function updateObjectRepository(postData) {
    var _postData = {
        name: postData.name,
        content: postData.content
    };
    return ajaxRequest(urlObjectRepositories + '/' + postData.id, "PUT", JSON.stringify(_postData));
}

function deleteObjectRepository(id) {
    return ajaxRequest(urlObjectRepositories + '/' + id, "DELETE", null);
}
//#endregion

//#region Paramters
function getParameters(pageSize) {
    var deffer = $.Deferred();
    ajaxRequest(urlParameter, "GET", {
        project_id: p_id,
        // amount: pageSize
    }).done(function (res) {
        deffer.resolve(res);
    }).catch(function (err) {
        deffer.reject(err);
    });
    return deffer.promise();
}

function createNewParameter(postData) {
    var _postData = Object.assign({
        project_id: p_id
    }, postData);
    return ajaxRequest(urlParameter, "POST", JSON.stringify(_postData));
}

function updateParameter(postData) {
    var _postData = {
        name: postData.name,
        content: postData.content
    };
    return ajaxRequest(urlParameter + '/' + postData.id, "PUT", JSON.stringify(_postData));
}

function deleteParameter(id) {
    return ajaxRequest(urlParameter + '/' + id, "DELETE", null);
}
//#endregion

//#region Test data
function getTestDatas(pageSize) {
    var deffer = $.Deferred();
    ajaxRequest(urlTestData, "GET", {
        project_id: p_id,
        // amount: pageSize
    }).done(function (res) {
        deffer.resolve(res);
    }).catch(function (err) {
        deffer.reject(err);
    });
    return deffer.promise();
}

function createNewTestData(postData) {
    var _postData = Object.assign({
        project_id: p_id
    }, postData);
    return ajaxRequest(urlTestData, "POST", JSON.stringify(_postData));
}

function updateTestData(postData) {
    var _postData = {
        name: postData.name,
        content: postData.content
    };
    return ajaxRequest(urlTestData + '/' + postData.id, "PUT", JSON.stringify(_postData));
}

function deleteTestData(id) {
    return ajaxRequest(urlTestData + '/' + id, "DELETE", null);
}
//#endregion