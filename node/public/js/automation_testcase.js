var localStepsDefinition = [];
var arrIssueSelected = [];
var currentUser = null;
var datasourceFromLibs = [];
var datasourceOnlyLibrary = [];
var arrTestCaseExecution = [];
var allScenario = null;
var autoTestCaseId = null;
var savingElement = '<div class="saving-data">Saving...</div>';
var localScenarioId = null;
var stepConfigId = null;
var tmplStep = '';
var tmplNameScenario = '';
var tmplSearhResult = '';
var tmplAddNewTag = '';
var tmplStepsParam = '';
var execute_Object = {};
var scenarioBackgroundId = null;
var localNameStep = '';
var projectId;

$.get(urlTemplateRender + 'steps-scenario.html', function (template) {
    tmplStep = template;
});

$.get(urlTemplateRender + 'name-scenario.html', function (template) {
    tmplNameScenario = template;
});

$.get(urlTemplateRender + 'list-result-search.html', function (template) {
    tmplSearhResult = template;
});

$.get(urlTemplateRender + 'add-new-tag.html', function (template) {
    tmplAddNewTag = template;
});

$.get(urlTemplateRender + 'steps-param.html', function (template) {
    tmplStepsParam = template;
});

$(function () {

    init();

    IFDRequest.customRequest('/rest/api/2/project/' + projectKey + '/versions', "GET", "application/json", null).then(function (response) {
        arrJiraVersion = JSON.parse(response.body);
    }).catch(function (er) { });

    rawVersionLogExecution = setupGherkin('txt-show-log', 'nocursor', 'text/javascript');
    rawVersionAll = setupGherkin('rawVersionAll');
    rawVersionDataTable = setupGherkin('rawVersionDataTable');
    rawVersionBackground = setupGherkin('rawVersionBackground');
    rawVersionStepsScenario = setupGherkin('rawVersionStepsScenario');
    rawVersionStepsDefinition = setupGherkin('rawVersionStepsDefinition');
    rawVersionDefinition = setupGherkin('rawVersionDefinition', false, 'gherkin');

    var tblTestCaseExcution = $('#auto_testcase_execution_panel').DataTable({
        paging: false,
        searching: false,
        info: false,
        ordering: false,
        autoWidth: false,
        data: null,
        columns: [{
            data: 'version',
            className: 'td-status',
            render: function (data, type, row, meta) {
                var versionName = lodash.find(arrJiraVersion, function (o) {
                    return o.id == data;
                });
                return versionName ? versionName.name : data;
            }
        },
        {
            data: 'test_cycle'
        },
        {
            data: 'status',
            className: 'td-status',
            render: function (data, type, row, meta) {
                var color = 'badge-secondary';
                switch (data.trim()) {
                    case 'FAIL':
                        color = 'badge-danger'; // FAIL
                        break;
                    case 'BLOCKED':
                        color = 'badge-warning'; // BLOCK
                        break;
                    case 'WIP':
                        color = 'badge-info'; // WIP
                        break;
                    case 'PASS':
                        color = 'badge-success'; // PASS
                        break;
                    default:
                        break;
                }
                return '<span class="btn badge badge-pill ' + color + ' change-status btn-block">' + data + '</span>';
            }
        },

        {
            data: 'defects',
            width: '19%',
            "render": function (data, type, row, meta) {
                var tmpl = '';
                $.each(data, function (i, value) {
                    tmpl += '<a class="btn ' + this.statusColor + ' badge badge-pill badge-secondary mr-1"  data-toggle="tooltip"  title="' + this.sumaryAndName + '" target="_blank" href=' + projectBaseUrl + this.value + '>' + this.value + '</a>';
                });
                return '<div class="inner-td">' + tmpl + '</div>';
            }
        },
        {
            // data: 'executed_by'
            data: function (row, type, val, meta) {
                if (row !== null && row.executed_by !== null) {
                    var _user = lodash.find(usersJira, function (o) {
                        return o.emailAddress == row.executed_by;
                    });
                    if (typeof _user !== 'undefined') {
                        return _user.displayName;
                    }
                }
                return row.executed_by;
            }
        },
        {
            // data: 'executed_on'
            data: function (row, type, val, meta) {
                if (row.executed_on === row.created_at) {
                    return '';
                }
                return row.executed_on ? moment(row.executed_on).format('DD/MM/YYYY') : '';
            }
        },
        {
            data: null,
            orderable: false,
            width: "17%",
            render: function (data, type, row, meta) {
                var param = {
                    execution_type: 'debug',
                    action_tupe: 'status'
                };
                return '<div class="group-border">' +
                    '<div class="row" id="' + data.plan_id + '">' +
                    '<div class="col-md-12">' +
                    '<button type="button" class="btn btn-custom-1 btn-block confirm_exec_debug debug_btn" action_type="execute" title="Execute"><span class="fa fa-bug"></span></button>' +
                    '<button type="button" class="btn btn-custom-1 btn-block get_status_debug debug_btn" action_type="status" title="Get Status"><span class="oi" data-glyph="info"></span></button>' +
                    '<button type="button" class="btn btn-custom-1 btn-block get_log_debug debug_btn" action_type="log" title="Get Log"><span class="oi" data-glyph="monitor"></span></button>' +
                    '<button type="button" class="btn btn-custom-1 btn-block confirm_stop_debug debug_btn" action_type="stop" title="Stop" data-toggle="modal" data-target="#modalConfirmStop"><span class="oi" data-glyph="ban"></span></button>' +
                    '</div></div></div>';
            }
        },
        {
            data: null,
            width: "15%",
            orderable: false,
            render: function (data, type, row, meta) {

                return '<a class="btn btn-link py-1 px-2" href=' + baseUrl + '/plugins/servlet/ac/Infodation-tms/auto-execution?project.key=' + projectKey +
                    '&ac.projectId=' + jiraProjectId +
                    '&ac.issueId=' + data.issue_id +
                    '&ac.cycleId=' + data.cycle_id +
                    '&ac.cycleName=' + escape(data.test_cycle) +
                    '&ac.planId=' + data.plan_id + ' target="_blank"><span class="oi" data-glyph="play-circle"></span></a>';
            }
        }
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).find(".debug_btn").on('click', function () {
                var id = $(nRow).find(".group-border .row").attr('id');
                var url = requestURL + '/execution_automation/' + id;
                var type = $(this).attr('action_type');
                var data = {
                    'execution_type': 'debug',
                    'action_type': type
                };
                var method = 'GET';
                if (type == 'stop') {
                    method = 'POST';
                    url = requestURL + '/execution_automation/' + id + '?execution_type=debug&action_type=' + type;
                    data = '';
                }
                var that = this;
                if (type != 'execute') {
                    if ($(that).attr('disabled') == 'disabled') {
                        return false;
                    } else {
                        $(that).attr({
                            'disabled': 'disabled'
                        });
                    }
                    if (type == 'stop') {
                        $('#modalConfirmStop').modal('show');
                        $('#modalConfirmStop .modal-body').html('<p style="padding-left:5px; margin-bottom:0;">Do you want to stop?</p>');
                        $('#stopSure').attr({
                            'data-id': id
                        });
                        $(that).removeAttr('disabled');
                    } else {
                        getDebugActionResult(url, data, method, that, type);
                    }
                } else {
                    method = 'POST';
                    url = requestURL + '/execution_automation/' + id + '?execution_type=debug&action_type=' + type;
                    data = '';
                    method1 = 'GET';
                    var url1 = requestURL + '/execution_automation/' + id;
                    var data1 = {
                        'execution_type': 'debug',
                        'action_type': 'status'
                    };
                    appendSpinner(that);
                    ajaxRequest(url1, method1, data1)
                        .done(function (res) {
                            removeSpinner(that);
                            if (res.message != 'running') {
                                execute_Object.url = url;
                                execute_Object.method = method;
                                $('#modal-exec-info').modal('show');
                                $('#exec_debug').attr({
                                    'data-id': id
                                });
                            } else {
                                alert('is running!');
                            }
                        })
                        .catch(function (err) {
                            removeSpinner(element);
                            $('#refresh-data .fas').removeClass('fa-spin');
                        });
                }
            });
            return nRow;
        },
    });

    $('#exec_debug').on('click', function () {
        var id = $(this).data('id');
        var browser_mode = $('#modal-exec-info').find('input[name=browser]:checked').val();
        var wait_time = $('#wait_time').val();
        var print_logs = $('#print_logs').val();
        var url = execute_Object.url;
        url = url + '&browser=' + browser_mode + '&wait_time=' + wait_time + '&print_log=' + print_logs;
        var that = $('.row[id="' + id + '"] button[action_type="execute"]');
        getDebugActionResult(url, '', execute_Object.method, that);
        $('#modal-exec-info').modal('hide');
    });

    $('#cancel-exec-debug').on('click', function () {
        $('#modal-exec-info').modal('hide');
    });

    $('#stopSure').on('click', function () {
        var id = $(this).data('id');
        var type = 'stop';
        var url = requestURL + '/execution_automation/' + id + '?execution_type=debug&action_type=' + type;
        var method = 'POST';
        var data = "";
        var that = $('.row[id="' + id + '"] button[action_type="stop"]');
        $('#modalConfirmStop').modal('hide');
        getDebugActionResult(url, data, method, that, type);
    });

    function getDebugActionResult(url, param, method, element, type) {
        appendSpinner(element);
        ajaxRequest(url, method, param).done(function (res) {
            $(element).removeAttr('disabled');
            removeSpinner(element);
            if (type) {
                if (type == 'log' && res.message != 'This case has not been triggered for automation yet!') {
                    // getReport(res.message);
                    showLog(res.message, type);
                } else {
                    showNotify(res.message, "custom-1")
                }
            } else {
                showNotify(res.message, "custom-1")
            }
        })
            .catch(function (err) {
                $(element).removeAttr('disabled');
                removeSpinner(element);
                $('#refresh-data .fas').removeClass('fa-spin');
            });
    }

    function showLog(logContent, type) {
        rawVersionLogExecution.setValue('');
        var title = 'Log';
        if (type == 'status') {
            title = 'Status';
        } else if (type == 'stop') {
            title = 'Stop';
        }
        $('#modal-show-log').find('.modal-title').html(title);
        $('#modal-show-log').modal('show');
        rawVersionLogExecution.setValue(logContent);
        rawVersionLogExecution.save();
        rawVersion.refresh();
    }

    function appendSpinner(that) {
        $(spinner).hide();
        $(that).append(spinner);
        $(that).find('.spinner').fadeIn();
    }

    function removeSpinner(that) {
        $(that).find('.spinner').fadeOut("slow", function () {
            $(that).find('.spinner').remove();
        });
    }

    function init() {
        // console.log('automation test page reload');
        projectId = $('#projectId').val();
        var originValObjTestCase = $('#originValObjTestCase').val();
        var originValObjSteps = $('#originValObjSteps').val();
        localStorageTestCase = originValObjTestCase != '' ? JSON.parse(originValObjTestCase) : '';
        localStepsDefinition = originValObjSteps != '' ? JSON.parse(originValObjSteps) : '';
        ajaxRequest(requestURL + '/automation_library', 'GET', null).done(function (datasource) {
            datasourceOnlyLibrary = datasourceOnlyLibrary.concat(getArrayAterRemoveDuplicate(datasource));
            datasourceFromLibs = datasourceOnlyLibrary.concat(getArrayAterRemoveDuplicate(localStepsDefinition));
            autoCompleteForInput($('#txtStepData'), $('#listSteps'));
            autoCompleteForInput($('#txtStepBackground'), $('#listStepsBackground'));
            autoCompleteForInputDefinition($('#txtDefinition'), $('#listDefinition'));
        });

        showHidePanelSelectTestCase(false, false);
        showHidePanelSelectConfig(false, false);
        if (issueId && issueId != '') {
            ajaxRequest(requestAPI.autoTestCase, 'GET', {
                issue_id: issueId
            }).done(function (res) {
                var responseAutoData = res[0];
                viewDetailAutoTestCase(responseAutoData);
                $('#resultTestCase >li[data-project-id="' + projectId + '"][data-issue-key="' + paramsUrl.issueKey + '"]')
                    .addClass('showing');
                getTestCycleOfTestcase(issueId);
            }).catch(function (err) {
                showHidePanelSelectTestCase(false, true);
                showSpinner(false);
                $('#spinnerScenario').addClass('spinner-content');
            });
        } else {
            showSpinner(false);
            $('#spinnerScenario').addClass('spinner-content');
        }
    }

    $('#refresh-data').on('click', function () {
        getTestCycleOfTestcase(issueId);
    });

    function getTestCycleOfTestcase(issueId) {
        $('#refresh-data .fas').addClass('fa-spin');
        ajaxRequest(requestAPI.getTestExecution, 'GET', {
            issue_id: issueId
        }).done(function (res) {
            var mainGetIssuesDefferAuto = [];
            var mainGetIssuesDeffer = [];
            arrTestCaseExecution = [];
            $.each(res, function () {
                var def = $.Deferred();
                mainGetIssuesDeffer.push(def.promise());
                var plan = this;
                ajaxRequest(requestAPI.getCycleDetail + '/' + plan.cycle_id, 'GET', null).done(function (res) {
                    var testCaseExecution = {
                        version: res.JIRAversion,
                        test_cycle: res.name,
                        status: plan.status,
                        defects: plan.defects,
                        // executed_by: plan.modified_by,
                        executed_by: plan.executed_by,
                        created_at: plan.created_at,
                        executed_on: plan.updated_at,
                        issue_id: plan.issue_id,
                        cycle_id: plan.cycle_id,
                        plan_id: plan.id
                    };
                    arrTestCaseExecution.push(testCaseExecution);
                    def.resolve();
                })
                    .catch(function (err) {
                        def.resolve();
                    });
            });
            $.when.apply($, mainGetIssuesDeffer).done(function () {
                $.each(arrTestCaseExecution, function (i, val) {
                    var mainDeferGetDefectDetail = [];
                    var arrDefect = [];
                    var _defects = val.defects != null ? val.defects.split('|') : [];
                    $.each(_defects, function (i, value) {
                        var def = $.Deferred();
                        mainDeferGetDefectDetail.push(def.promise());
                        mainGetIssuesDefferAuto.push(def.promise());
                        AP.request(baseUrl + '/rest/api/2/search?jql=issue=\'' + value + '\'&fields=summary,status', {
                            success: function (res) {
                                var detailDefect = {
                                    statusColor: "",
                                    sumaryAndName: '',
                                    value: '',
                                };
                                var datasource = JSON.parse(res);
                                var issue = datasource && datasource["issues"].length && datasource["issues"]["0"];
                                var status = issue && issue.fields ? issue.fields.status : undefined;
                                var summaryTextBug = issue.fields.summary;
                                var statusColor;
                                sumaryAndName = 'Summary : ' + summaryTextBug.toString() + "&#013;" + 'Status : ' + status.name.toString();

                                colorStatusBug = status.name;
                                if (colorStatusBug == "To Do") {
                                    statusColor = "statusToDoColor";
                                } else if (colorStatusBug == "Done") {
                                    statusColor = "statusDoneColor";
                                } else {
                                    statusColor = "statusInProgressColor";
                                }

                                detailDefect.statusColor = statusColor;
                                detailDefect.sumaryAndName = sumaryAndName;
                                detailDefect.value = value;
                                arrDefect.push(detailDefect);
                                def.resolve();
                            },
                            error: function () {
                                def.resolve();
                            }
                        });
                    });

                    $.when.apply($, mainDeferGetDefectDetail).done(function () {
                        arrTestCaseExecution[i].defects = arrDefect;
                    });
                });
                $.when.apply($, mainGetIssuesDefferAuto).done(function () {
                    setTimeout(function () {
                        tblTestCaseExcution.clear();
                        tblTestCaseExcution.rows.add(arrTestCaseExecution).draw();
                        $('#refresh-data .fas').removeClass('fa-spin');
                    }, 500);
                });
            });


        }).catch(function (err) {
            $('#refresh-data .fas').removeClass('fa-spin');
            tblTestCaseExcution.clear().draw();

        });
    }

    $('#openDialogTestCycle').click(function () {
        ajaxRequest(requestURL + '/cycle?project_id=' + projectId + '', "GET", null).done(function (testCycleData) {
            if (testCycleData.length > 0) {
                $('#project_cycle option').remove();
                $('#project_cycle').removeAttr('disabled');
                // var cycleName = $('<optgroup label="Select Test Cycle"></optgroup>');
                $.each(testCycleData, function () {
                    var option = '<option value="' + this.id + '">' + this.name + '</option>';
                    // cycleName.append(option);
                    $('#selectCycle').append(option);
                });
                // $('#project_cycle').append(cycleName);
            }
            $('#add-test-cycle').removeAttr('disabled');
        }).catch(function (er) {
            $('#project_cycle').html('<option>No cycles found</option>');
        });

        IFDRequest.customRequest('/rest/api/2/user/assignable/search?project=' + projectKey, 'GET', 'application/json', null).then(function (res) {
            userJira = lodash.filter(JSON.parse(res.body), function (o) {
                return o.emailAddress.indexOf("@connect.atlassian.com") === -1;
            });
            $('#project_user option').remove();
            $('#project_user').removeAttr('disabled');
            $.each(userJira, function () {
                var that = this;
                var optionTag = '<option value=' + that.emailAddress + '>' + that.displayName + '</option>';
                $('#project_user').append(optionTag);
            });
        });
    });

    $('#add-test-cycle').on('click', function () {
        var projectCycleId = $('#project_cycle').val();
        arrIssueSelected.push(issueId);
        if (arrIssueSelected.length > 0) {
            var data = {
                "issue_ids": arrIssueSelected
            }
            $('#add-test-cycle').append(spinner);
            ajaxRequest(requestURL + '/plan/add_issues/' + projectCycleId, "POST", JSON.stringify(data)).done(function (res) {
                $('#add-test-cycle').find('.spinner').remove();
                if ($('#project_user').val() !== '') {
                    updatePlanTestCase(projectCycleId, res[0].issue_id, {
                        assigned_to: $('#project_user').val()
                    }).done(function (res) {
                        $('#modalAddTestCycle').modal('hide');
                        getTestCycleOfTestcase(issueId);
                    })
                        .catch(function (err) {
                            $('#error-add-testcycle').html(err.error);
                        });
                } else {
                    $('#modalAddTestCycle').modal('hide');
                }
            }).catch(function (err) {
                $('#error-add-testcycle').html(err.responseJSON.error);
                $('#add-test-cycle').find('.spinner').remove();
            });
        }
    });

    function getSourceLibsAutoComplete(value) {
        var stepId = value.id;
        var keyword = value.keyword;
        var name = value.name;
        var description = value.description;
        var steps_params = value.steps_params;
        if (!keyword) {
            keyword = 'Given';
        }
        if (!description) {
            description = '';
        }
        var source = JSON.stringify({
            value: keyword + ' ' + name,
            // label: keyword + ' ' + name,
            label: name,//---> Thinh edit on ticket IFDTMS-635
            steps_definition_id: stepId,
            name: name,
            keyword: keyword,
            description: description,
            steps_params: steps_params
        });
        return JSON.parse(source);
    }

    rawVersionDefinition.on('blur', function () {
        if ($('#rawDefinition').hasClass('change-codemirror')) {
            rawVersionDefinition.save();
            updateContentStepsDefinition($('#rawDefinition'), $('#rawVersionDefinition').val());
        }
    });

    rawVersionDefinition.on('change', function () {
        if (!$('#rawDefinition').hasClass('change-codemirror')) {
            $('#rawDefinition').addClass('change-codemirror');
        }
    });

    $('#autoTestCaseTab').click(function () {
        activeMainTabs(true);
    });

    $('#configurationTab').click(function () {
        activeMainTabs(false);
    });

    function activeMainTabs(mode) {
        if (mode) {
            $('#autoTestCaseTab').addClass('active');
            $('#autoTestCase').addClass('active');
            $('#configurationTab').removeClass('active');
            $('#configuration').removeClass('active');
        } else {
            $('#configurationTab').addClass('active');
            $('#configuration').addClass('active');
            $('#autoTestCaseTab').removeClass('active');
            $('#autoTestCase').removeClass('active');
        }
    }

    $('#btnOpenPanelAddSteps').on('click', function () {
        showHidePanelSelectConfig(true, false);
        stepConfigId = null;
        $('#nameStepsDefinition').val('');
        $('#resultSteps .detail-steps').removeClass('showing');
    });

    $('#txtSearchSteps').on('keyup', function () {
        loadAllSteps(fuzzySearch($(this).val(), localStepsDefinition));
    });

    function loadAllSteps(allSteps) {
        if (allSteps.length > 0) {
            var renderListSteps = '';
            $.each(allSteps, function (index, value) {
                renderListSteps += Mustache.render(tmplSearhResult, {
                    id: value.id,
                    name: value.name,
                    keyword: value.keyword
                });
            });
            $('#resultSteps').html(renderListSteps);
            $('#resultSteps').find('li[data-step-id="' + stepConfigId + '"]').addClass('showing');
        } else {
            $('#resultSteps').html('<li class="list-group-item">No data</li>');
            showSpinner(false);
        }
    }

    //VIEW DETAIL FEATURE
    ////
    $('#resultTestCase').on('click', '.list-group-item', function () {
        if (!$(this).hasClass('showing')) {
            $('#resultTestCase').find('.list-group-item').removeClass('showing');
            $(this).addClass('showing');
            jiraTicketId = $(this).data('jira-id');
            jiraTicketKey = $(this).find('.issue-key').html();
            jiraTicketTitle = $(this).find('.issue-title').html();
            showSpinner(true);
            showDetailScenario(false);//---> Thinh add at 2018-10-05
            resetDataFeature();
            showHidePanelSelectTestCase(true, false);
            getAutoTestCaseDetail(projectId, jiraTicketId, jiraTicketKey, jiraTicketTitle);
        }
    });

    //VIEW DETAIL STEP DEFINITION
    $('#resultSteps').on('click', '.detail-steps', function () {
        if (!$(this).hasClass('showing')) {
            $('#resultSteps').find('.detail-steps').removeClass('showing');
            $(this).addClass('showing');
            stepConfigId = $(this).data('step-id');
            showSpinner(true);
            getStepsDefinitionDetail(stepConfigId);
        }
    });

    //REDIRECT TO VIEW DETAIL STEP
    $('body').on('click', '.redirect-step-detail', function () {
        stepConfigId = $(this).closest('li').attr('data-step-id');
        activeMainTabs(false);
        showSpinner(true);
        getStepsDefinitionDetail(stepConfigId);
    });

    //CHANGE NAME OF STEP DEFINITION
    $('#nameStepsDefinition').on('keyup', function () {
        $(this).removeClass('existed-database');
        $('#blockCreateNewStep').find('label').remove();
    });

    $('#nameStepsDefinition').focusout(function () {
        var _this = $(this);
        if (_this.hasClass('changed') && _this.val() != '') {
            _this.removeClass('changed');
            var name = _this.val();
            var newName = name;
            var myRegexpParam = /(\".*?\")/g;
            while (matches = myRegexpParam.exec(newName)) {
                newName = newName.replace(matches[0], 'xxx');
            }
            while (matches = myRegexpParam.exec(localNameStep)) {
                localNameStep = localNameStep.replace(matches[0], 'xxx');
            }
            if (newName != localNameStep) {
                $.each(datasourceFromLibs, function (ind, val) {
                    var _currentValue = val.name;
                    var _newValue = _currentValue;
                    while (_newMatches = myRegexpParam.exec(_newValue)) {
                        _newValue = _newValue.replace(_newMatches[0], 'xxx');
                    }
                    if (_newValue === newName) {
                        _this.addClass('existed-database');
                        return false;
                    }
                });
            }
            if (!_this.hasClass('existed-database')) {
                showSpinner(true);
                if (stepConfigId != null) {
                    var params = JSON.stringify({
                        name: _this.val()
                    });
                    API_UpdateStepsDefinition(stepConfigId, params).done(function (res) {
                        getAllStepsDefinition();
                        renderGuiDetailStep(res);
                        getAllStepsOfScenario(scenarioBackgroundId, $('#listStepsBackground'));
                        getAllStepsOfScenario(localScenarioId, $('#listSteps'));
                        showSpinner(false);
                    }).catch(function (err) {
                        showSpinner(false);
                        showNotifyCustoms(err.responseJSON.error, 'danger', 'right', 0);
                    });
                } else {
                    var _params = JSON.stringify({
                        name: _this.val(),
                        keyword: 'Given',
                        project_id: projectId
                    });
                    ajaxRequest(requestAPI.stepsDefinition, 'POST', _params).done(function (response) {

                        getAllStepsDefinition();
                        renderGuiDetailStep(response);
                        showSpinner(false);
                    }).catch(function (err) {
                        showSpinner(false);
                        showNotifyCustoms(err.responseJSON.error, 'danger', 'right', 0);
                    });
                }
            } else {
                $('#blockCreateNewStep').append('<label class="error">This name is already existed in database</label>');
            }
        }
    });

    $('#allParameters').on('click', '.delete-param', function () {
        var _this = $(this);
        var paramId = _this.parent().attr('data-param-id');
        _this.closest('tr').remove();
        ajaxRequest(requestAPI.stepsParam + paramId, 'DELETE').done(function (res) {
            $('#nameStepsDefinition').val(res.steps_definition.name);
            getAllStepsDefinition();
            getAllStepsOfScenario(scenarioBackgroundId, $('#listStepsBackground'));
            getAllStepsOfScenario(localScenarioId, $('#listSteps'));
        }).catch(function (err) {

        });
    });

    //show tags of all scenario
    $('#showTagsScenario').on('click', 'a', function () {
        if ($(this).hasClass('show-tags')) {
            $(this).removeClass('show-tags');
            $('#listScenarios').find('.tags-scenario').hide();
        } else {
            $(this).addClass('show-tags');
            $('#listScenarios').find('.tags-scenario').show();
        }
    });

    function importAllValueAuto() {
        var uploadFile = $('#uploadFile')[0].files[0];
        var formData = new FormData();
        formData.append('datafile', uploadFile);
        formData.append('issue_id', issueId);

        ajaxRequestImport(requestURL + '/import/auto_testcase', 'POST', formData).done(function (res) {
            var responseAutoData = res.message;
            $('#userName').val('');
            $('#password').val('');
            viewDetailAutoTestCase(responseAutoData);
            getTestCycleOfTestcase(issueId);
            getAllStepsDefinition();
        }).catch(function (err) {
            showSpinner(false);
            showNotifyCustoms(err.responseJSON.error, 'danger', 'right', 0);
        });
    }

    function getAllStepsDefinition() {
        datasourceFromLibs = datasourceOnlyLibrary;
        ajaxRequest(requestAPI.stepsDefinition, 'GET', {
            project_id: projectId
        }).done(function (res) {
            // console.log("step definition");
            loadAllSteps(res);
            localStepsDefinition = res;
            datasourceFromLibs = datasourceFromLibs.concat(getArrayAterRemoveDuplicate(res));
            //reload datasources for input autocomplete
            autoCompleteForInput($('#txtStepData'), $('#listSteps'));
            autoCompleteForInput($('#txtStepBackground'), $('#listStepsBackground'));
        });
    }

    function getArrayAterRemoveDuplicate(response) {
        var _arr = [];
        $.each(response, function (index, value) {
            _arr.push(getSourceLibsAutoComplete(value));
        });
        _arr = removeDuplicateObj(_arr);
        return _arr;
    }

    $('#submitAuto').on('click', function () {
        $.notifyClose();
        showSpinner(true);
        importAllValueAuto();
    });

    //GET AUTO TESTCASE DETAIL
    function getAutoTestCaseDetail(projectId, jiraTicketId, jiraTicketKey, jiraTicketTitle) {
        ajaxRequest(requestAPI.getIssueDetail, 'GET', {
            project_id: projectId,
            JIRAticket_key: jiraTicketKey
        }).done(function (res) {
            var issue = res[0];
            issueId = issue.id;
            var projectBaseUrl = baseUrl + "/browse/";
            $("#JIRAticketKey-auto").html("Back to" + "&nbsp;" + issue.JIRAticket_key).attr("href", projectBaseUrl + issue.JIRAticket_key);
            ajaxRequest(requestAPI.autoTestCase, 'GET', {
                issue_id: issueId
            }).done(function (res) {
                var responseAutoData = res[0];
                getTestCycleOfTestcase(issueId);
                viewDetailAutoTestCase(responseAutoData);
            }).catch(function (err) {
                showHidePanelSelectTestCase(false, true);
                showSpinner(false);
            });
        });
    }

    function viewDetailAutoTestCase(objFeature) {
        autoTestCaseId = objFeature.id;
        var featureName = objFeature.feature_name;
        var gherkinContent = objFeature.gherkin_content;
        var featureTags = objFeature.tags;
        
        //---> Refresh list when switch ticket
        var listExistedTags = $('#tagsFeature .add-new-tag').prevAll();
        if (listExistedTags.length) {
            listExistedTags.remove();
        }

        if (featureTags && featureTags != '') {
            var arrfeatureTags = featureTags.split(',');
            $.each(arrfeatureTags, function (ind, val) {
                var valueTag = val.substr(1);
                var render = Mustache.render(tmplAddNewTag, {
                    textTag: valueTag
                });

                $('#tagsFeature').find('.add-new-tag').before(render);
            });
        }
        if (gherkinContent || gherkinContent != null) {
            rawVersionAll.setValue(gherkinContent);
            rawVersionAll.save();
        }
        $('#featureNameLink').addClass('can-edit').html(featureName);
        var featureDescription = objFeature.description;
        if (featureDescription && featureDescription != '') {
            $('#descriptionFeature').removeClass('no-description').val(featureDescription);
        } else {
            $('#descriptionFeature').addClass('no-description');
        }
        allScenario = objFeature.scenarios;
        if (allScenario && allScenario.length > 0) {
            $.each(allScenario, function (ind, value) {
                var scenarioType = value.scenario_type;
                var scenarioName = value.name;
                var scenarioId = value.id;
                var scenarioDescription = value.description;
                var orderId = value.order_id;
                var tagsScenario = value.tags;
                if (scenarioType == 'Background') {
                    orderId = 1;
                    scenarioBackgroundId = scenarioId;
                    //render background
                    getAllStepsOfScenario(scenarioBackgroundId, $('#listStepsBackground'));
                    $('#collapseDataBackgroud').collapse('show');
                    //end
                }
                //render scenario
                $('#showTagsScenario').show();
                addScenarioWhenLoadFromFeatureOrCreateNew(orderId, scenarioType, scenarioName, scenarioId, scenarioDescription, tagsScenario);
                countAllScenario();
                //end
            });
        }
        showHidePanelSelectTestCase(true, false);
        showSpinner(false);
    }

    //GET STEP DEFINITION DETAIL
    function getStepsDefinitionDetail(id) {
        API_GetStepsDefinitionById(id).done(function (data) {
            renderGuiDetailStep(data);
            showSpinner(false);
        });
    }

    //RENDER GUI DETAIL STEP
    function renderGuiDetailStep(data) {
        localNameStep = data.name;
        $('#nameStepsDefinition').val(data.name);
        $('#resultSteps .detail-steps').removeClass('active');
        $('#blockCreateNewStep').find('label').remove();
        $('#resultSteps .detail-steps[data-step-id="' + data.id + '"]').addClass('showing');
        if (data.content != null && data.content != '') {
            rawVersionDefinition.setValue(data.content);
        } else {
            rawVersionDefinition.setValue('');
        }
        rawVersionDefinition.save();
        rawVersionDefinition.refresh();
        var stepsParam = data.steps_params;
        var rendered = '';
        if (stepsParam != '') {
            for (i = 0; i < stepsParam.length; i++) {
                rendered += Mustache.render(tmplStepsParam, {
                    id: stepsParam[i].id,
                    name: stepsParam[i].name,
                    value: stepsParam[i].value
                });
            }
            $('#allParameters').html(rendered);
            $('#collapse-parameter').collapse('show');
        } else {
            $('#allParameters').html('');
            $('#collapse-parameter').collapse('hide');
        }
        showHidePanelSelectConfig(true, true);
    }

    //add new feature
    $('#inputNewFeatureName').focusout(function () {
        var featureName = $(this).val();
        if (featureName != '') {
            var params = JSON.stringify({
                feature_name: featureName,
                issue_id: issueId
            });
            showSpinner(true);
            ajaxRequest(requestAPI.autoTestCase, 'POST', params).done(function (res) {
                autoTestCaseId = res.id;
                $('#featureNameLink').html(featureName);
                $('#inputNewFeatureName').val('');
                getTestCycleOfTestcase(issueId);
                showHidePanelSelectTestCase(true, false);
                countAllScenario();
                setTimeout(function () {
                    showSpinner(false);
                }, 1000);
            })
        }
    });

    $('#txtStepData').on('keyup', function (e) {
        checkEventToAddNewStepsScenario(e, $(this), $('#listSteps'));
    });

    $('#txtStepBackground').on('keyup', function (e) {
        checkEventToAddNewStepsScenario(e, $(this), $('#listStepsBackground'));
    });

    function checkEventToAddNewStepsScenario(event, element, parent) {
        if (event.keyCode == 13) {
            if (element.val() != '') {
                var textOfStep = element.val();
                var params = JSON.stringify({
                    name: textOfStep,
                    scenario_id: parent.attr('data-id'),
                    keyword: '*',
                    project_id: projectId
                });
                addNewStepOfScenario(params, parent);
                element.val('');
            } else {
                element.addClass('error').focus();
            }
        }
    }

    //click to open input feature name
    $('#featureNameLink').click(function () {
        if ($(this).hasClass('can-edit') && !$(this).hasClass('editing')) {
            var html = $(this).html();
            $(this).html('<input type="text" id="updateFeatureName" value="' + html + '">');
            $(this).addClass('editing');
            $(this).find('input').focus().select();
        } else {
            $(this).addClass('can-edit');
            $('#linkOpenedScenarioDetail').hide();
            showDetailScenario(false);
        }
    });

    //update feature name
    $('#featureNameLink').on('focusout', '#updateFeatureName', function () {
        var that = $(this);
        var featureName = that.val();
        if (that.hasClass('changed')) {
            showSpinner(true);
            var params = JSON.stringify({
                feature_name: featureName
            });
            API_UpdateAutoTestcase(autoTestCaseId, params).done(function (res) {
                that.parent().removeClass('editing').html(featureName);
                showSpinner(false);
            }).catch(function (err) { });
        } else {
            that.parent().removeClass('editing').html(featureName);
        }
    });

    $('#descriptionFeature').focusout(function () {
        var that = $(this);
        var featureDescription = that.val();
        if (that.hasClass('changed')) {
            showSpinner(true);
            var params = JSON.stringify({
                description: featureDescription
            });
            API_UpdateAutoTestcase(autoTestCaseId, params).done(function (res) {
                that.html(featureDescription);
                showSpinner(false);
            }).catch(function (err) { });
        } else {
            that.html(featureDescription);
        }
    });

    $('#outputScenarioName').focusout(function () {
        var that = $(this);
        var scenarioName = that.val();
        if (that.hasClass('changed')) {
            showSpinner(true);
            var params = JSON.stringify({
                name: scenarioName
            });
            API_UpdateScenario(localScenarioId, params).done(function (res) {
                that.html(scenarioName);
                $('#scenarioNameLink').html(scenarioName);
                $('#listScenarios').find('.scenario-name-rendered[data-id="' + localScenarioId + '"] .text-name').html(scenarioName);
                showSpinner(false);
            }).catch(function (err) { });
        }
    });

    $('#outputDescriptionScenario').focusout(function () {
        var that = $(this);
        var scenarioDescription = that.val();
        var scenarioId = $('#screenDetailScenario').data('scenario-id');
        if (that.hasClass('changed')) {
            showSpinner(true);
            var params = JSON.stringify({
                description: scenarioDescription
            });
            API_UpdateScenario(localScenarioId, params).done(function (res) {
                that.html(scenarioDescription);
                $('#listScenarios').find('.scenario-name-rendered[data-id="' + localScenarioId + '"] .descriptionScenario').html(scenarioDescription);
                showSpinner(false);
            }).catch(function (err) { });
        }
    });

    //remove class changed to check input change after focusin
    $('body').on('focusin', 'input, textarea', function () {
        $(this).removeClass('changed');
    });

    //add class changed to check input change
    $('body').on('change', 'input, textarea', function () {
        $(this).addClass('changed');
    });

    $('body').on('keyup', 'input', function (e) {
        if (e.keyCode == 13) {
            $(this).blur();
        }
    })

    //change with for input param in step
    $('#autoTestCase').on('keyup keydown paste cut', 'input.input-parameter', function (e) {
        setWidthForInputParamStep($(this));
    });

    //update value steps param
    $('#listStepsBackground, #listSteps').on('focusout', '.input-parameter', function () {
        var value = $(this).val();
        if ($(this).hasClass('changed')) {
            //---> Call init popover
            initPopover($(this));
            updateValueScenarioStepsParams($(this), value, true);
        }
    });

    //update default value steps param
    $('#allParameters').on('focusout', '.input-parameter', function () {
        var value = $(this).val();
        if ($(this).hasClass('changed')) {
            updateDefaultValueStepsParams($(this), value);
        }
    });

    //update keyword of step definiton
    $('#listStepsBackground, #listSteps').on('change', '.keyword select', function () {
        var scenaioStepId = $(this).closest('li').data('scenario-step-id');
        var value = $(this).val();
        var params = JSON.stringify({
            keyword: value
        });
        var allStepsElement = $(this).closest('.all-steps');
        allStepsElement.append(savingElement);
        API_UpdateScenarioSteps(scenaioStepId, params).done(function () {
            allStepsElement.find('.saving-data').remove();
        }).catch(function (err) { });
    });

    //delete step definition
    $('#listStepsBackground, #listSteps').on('click', '.delete-step', function () {
        var _li = $(this).closest('li');
        var stepId = _li.data('step-id');
        var scenarioId_step = _li.closest('.list-steps').attr('data-id');
        var allStepsElement = $(this).closest('.all-steps');
        _li.remove();
        allStepsElement.append(savingElement);
        var params = JSON.stringify({
            steps_definition_id: stepId
        })
        ajaxRequest(requestAPI.scenario + '/' + scenarioId_step + '/remove_steps', 'DELETE', params).done(function (res) {
            allStepsElement.find('.saving-data').remove();
        })
    });

    //delete feature
    $('#deleteSureTestcase').click(function () {
        $('#modalConfirmDeleteTestcase').modal('hide');
        $('#resultTestCase').find('>li').removeClass('showing');
        showSpinner(true);
        API_DeleteAutoTestcase(autoTestCaseId).done(function (res) {
            showNotify("Delete Auto testcase success", "success");
            getAllStepsDefinition();
            showHidePanelSelectTestCase(false, false);
            showSpinner(false);
        }).catch(function (err) {
            showSpinner(false);
        })
    });

    $('#deleteStepsDefinition').on('click', function () {
        $('#modalConfirmDeleteSteps').find('.steps-name').html($('#resultSteps').find('li[data-step-id=' + stepConfigId + ']').html());
    })

    $('#deleteSureSteps').on('click', function () {
        deleteStepsDefinition();
    });

    function deleteStepsDefinition() {
        $('#modalConfirmDeleteSteps').modal('hide');
        $('#resultSteps').find('li[data-step-id=' + stepConfigId + ']').remove();
        showSpinner(true);
        API_DeleteStepsDefinition(stepConfigId).done(function () {
            getAllStepsOfScenario(scenarioBackgroundId, $('#listStepsBackground'));
            getAllStepsOfScenario(localScenarioId, $('#listSteps'));
            showNotify("Delete step definition success", "success");
            showHidePanelSelectConfig(false, false);
            showSpinner(false);
            stepConfigId = null;
        }).catch(function (err) {
            showSpinner(false);
        });
    }

    //call API
    function API_GetStepsDefinitionById(id) {
        return ajaxRequest(requestAPI.stepsDefinition + id, 'GET', null);
    }

    //update data auto_testcase
    function API_UpdateAutoTestcase(autoTestCaseId, params) {
        return ajaxRequest(requestAPI.autoTestCase + autoTestCaseId, 'PUT', params);
    }

    //update scenario
    function API_UpdateScenario(scenarioId, params) {
        return ajaxRequest(requestAPI.scenario + '/' + scenarioId, 'PUT', params);
    }

    //update step_param
    function API_UpdateStepsParam(paramId, params) {
        return ajaxRequest(requestAPI.stepsParam + paramId, 'PUT', params);
    }

    //update scenario_steps_param
    function API_UpdateScenarioStepsParam(paramId, params) {
        return ajaxRequest(requestAPI.scenarioStepParam + paramId, 'PUT', params);
    }

    //update steps_definition
    function API_UpdateStepsDefinition(stepId, params) {
        return ajaxRequest(requestAPI.stepsDefinition + stepId, 'PUT', params);
    }

    //update scenario_steps
    function API_UpdateScenarioSteps(scenarioStepId, params) {
        return ajaxRequest(requestAPI.scenarioSteps + scenarioStepId, 'PUT', params);
    }

    //delete data scenario
    function API_DeleteScenario(scenarioId) {
        return ajaxRequest(requestAPI.scenario + '/' + scenarioId, 'DELETE');
    }

    //detele steps_definiton
    function API_DeleteStepsDefinition(stepId) {
        return ajaxRequest(requestAPI.stepsDefinition + stepId, 'DELETE');
    }

    //delete testcase
    function API_DeleteAutoTestcase(autoTestCaseId) {
        return ajaxRequest(requestAPI.autoTestCase + autoTestCaseId, 'DELETE');
    }

    function updateValueScenarioStepsParams(element, valueParam, showPopover = false) {
        //---> disable input
        element.attr("readonly", true);

        var paramId = element.attr('data-param-id');
        var allStepsElement = element.closest('.all-steps');
        allStepsElement.append(savingElement);
        var params = JSON.stringify({
            value: valueParam
        });
        API_UpdateScenarioStepsParam(paramId, params).done(function () {
            allStepsElement.find('.saving-data').remove();
            if (showPopover === true) {
                // console.log("true");
                // //---> Call init popover
                // initPopover(element);
            } else {
                // console.log("false");
                // element.popover('dispose');
            }
            //---> Enable input
            element.removeAttr("readonly");
        }).catch(function () {
            //---> Call init popover
            initPopover(element);

            //---> Enable input
            element.removeAttr("readonly");
        });
    }

    //---> Update default value steps params
    function updateDefaultValueStepsParams(element, valueParam) {
        var paramId = element.attr('data-param-id');
        var allStepsElement = element.closest('.all-steps');
        allStepsElement.append(savingElement);
        var params = JSON.stringify({
            value: valueParam
        });
        API_UpdateStepsParam(paramId, params).done(function (res) {
            // console.log('res of update steps params',res);
            allStepsElement.find('.saving-data').remove();
            //---> Update view list steps of scenario
            getAllStepsOfScenario(localScenarioId, $('#listSteps'));
        }).catch(function () { });
    }

    //create new scenario
    $('#createNewScenario').click(function () {
        var text = $('#inputScenarioName').val();
        var orderId = $('#totalScenario').data('total') + 2;
        var type = "Scenario";
        $('#modalCreateNewScenario').modal('hide');
        $('#createShowScenario .all-steps').append(savingElement);
        var params = JSON.stringify({
            name: text,
            scenario_type: type,
            keyword: type,
            auto_testcase_id: autoTestCaseId
        });
        ajaxRequest(requestAPI.scenario, 'POST', params).done(function (res) {
            $('#showTagsScenario').show();
            addScenarioWhenLoadFromFeatureOrCreateNew(orderId, type, text, res.id, null, null);
            countAllScenario();
            $('#inputScenarioName').val('');
            showNotify("Create scenario success", "success");
            $('#createShowScenario .all-steps').find('.saving-data').remove();
        });
    });

    //click scenario to view detail
    $('#listScenarios').on('click', '.scenario-name-rendered', function () {
        showSpinner(true);
        var id = $(this).data('id');
        var type = $(this).data('type');
        var text = $(this).find('.renderName .text-name').text().trim();
        var description = $(this).find('.descriptionScenario').html();
        var tagsScenario = $(this).find('.inputTagsScenario').html();
        $('#linkOpenedScenarioDetail').show();
        $('#scenarioNameLink').html(text);
        $('#featureNameLink').removeClass('can-edit');
        // console.log("======== Scenario rendering ==========");
        viewDetailScenario(id, type, text, description, tagsScenario);
    });

    //click btn delete to delete scenario
    $('#listScenarios').on('click', '.delete-scenario', function () {
        var id = $(this).data('id');
        var _li = $(this).parent();
        _li.remove();
        reIndexScenario();
        API_DeleteScenario(id).done(function () {
            countAllScenario();
            showNotify("Delete scenario success", "success");
        });
    });

    $('#switch-version-all-content').click(function () {
        if (!$(this).hasClass('show-raw-version')) {
            showSpinner(true);
            changeStatusButtonSwitchVersion($(this), true);
            renderAllDataToTextArea();
            setTimeout(function () {
                rawVersionAll.refresh();
            }, 100);
            setTimeout(function () {
                showhideRawVersion($('#screenEditorVersionAll'), $('#screenRawVersionAll'), true);
            }, 500);
        } else {
            changeStatusButtonSwitchVersion($(this), false);
            showhideRawVersion($('#screenEditorVersionAll'), $('#screenRawVersionAll'), false);
        }
    });

    $('#switch-version-data-table').click(function () {
        if (!$(this).hasClass('show-raw-version')) {
            changeStatusButtonSwitchVersion($(this), true);
            renderDataTableToTextArea();
            setTimeout(function () {
                rawVersionDataTable.refresh();
            }, 100);
            showhideRawVersion($('#allDataTable'), $('#rawDataTable'), true);
        } else {
            changeStatusButtonSwitchVersion($(this), false);
            showhideRawVersion($('#allDataTable'), $('#rawDataTable'), false);
        }
    });

    $('#switch-version-background').click(function () {
        if (!$(this).hasClass('show-raw-version')) {
            changeStatusButtonSwitchVersion($(this), true);
            renderBackgroundToTextArea();
            setTimeout(function () {
                rawVersionBackground.refresh();
            }, 100);
            showhideRawVersion($('#allStepsBackground'), $('#rawBackground'), true);
        } else {
            changeStatusButtonSwitchVersion($(this), false);
            showhideRawVersion($('#allStepsBackground'), $('#rawBackground'), false);
        }
    });

    $('#switch-version-step').click(function () {
        if (!$(this).hasClass('show-raw-version')) {
            changeStatusButtonSwitchVersion($(this), true);
            renderStepsToTextArea();
            setTimeout(function () {
                rawVersionStepsScenario.refresh();
            }, 100);
            showhideRawVersion($('#allStepsScenario'), $('#rawStepsScenario'), true);
        } else {
            changeStatusButtonSwitchVersion($(this), false);
            showhideRawVersion($('#allStepsScenario'), $('#rawStepsScenario'), false);
        }
    });

    $('#allDataTable, #listSteps, #listStepsBackground').on('click', '.datatable-cell-content', function () {
        if (!$(this).hasClass('editting')) {
            $(this).addClass('editting');
            var value = $(this).find('span').html();
            $(this).append('<input class="render-text" type="text" value="' + value + '">');
            $(this).find('input').focus().select();
        }
    });

    $('#allDataTable').on('focusout', '.render-text', function () {
        var value = $(this).val();
        $(this).parent().removeClass('editting');
        $(this).parent().find('span').html(value);
        var eleTable = $(this).closest('.all-data-table');
        if ($(this).hasClass('changed')) {
            $(this).remove();
            updateExampleDataTable(eleTable);
        } else {
            $(this).remove();
        }
    });

    $('#listSteps, #listStepsBackground').on('focusout', '.render-text', function () {
        var value = $(this).val();
        $(this).parent().removeClass('editting');
        $(this).parent().find('span').html(value);
        var eleTable = $(this).closest('.all-data-table');
        if ($(this).hasClass('changed')) {
            $(this).remove();
            updateValueScenarioStepsParams(eleTable, convertAndSaveValueDataTable(eleTable));
        } else {
            $(this).remove();
        }
    });

    $('#allDataTable').on('click', '.createDataHeader', function () {
        var eleTable = $(this).closest('.all-data-table');
        addColumnForDataTable(eleTable);
        updateExampleDataTable(eleTable);
        eleTable.find('.new-parameter-name').trigger('click');
    });

    $('#listSteps, #listStepsBackground').on('click', '.createDataHeader', function () {
        var eleTable = $(this).closest('.all-data-table');
        addColumnForDataTable(eleTable);
        updateValueScenarioStepsParams(eleTable, convertAndSaveValueDataTable(eleTable));
        eleTable.find('.new-parameter-name').trigger('click');
    });

    $('#allDataTable').on('click', '.delete-table-row', function () {
        var _tr = $(this).closest('tr');
        var eleTable = _tr.closest('.all-data-table');
        _tr.remove();
        updateExampleDataTable(eleTable);

    });

    $('#listSteps, #listStepsBackground').on('click', '.delete-table-row', function () {
        var _tr = $(this).closest('tr');
        var eleTable = _tr.closest('.all-data-table');
        _tr.remove();
        updateValueScenarioStepsParams(eleTable, convertAndSaveValueDataTable(eleTable));
    });

    $('#allDataTable').on('click', '.delete-table-column', function (e) {
        e.preventDefault();
        var _td = $(this).parent();
        var order = _td.index();
        var eleTable = _td.closest('.all-data-table');
        _td.remove();
        eleTable.find('tbody tr').each(function () {
            $(this).find('td').eq(order).remove();
        });
        updateExampleDataTable(eleTable);
    });

    $('#listSteps, #listStepsBackground').on('click', '.delete-table-column', function (e) {
        e.preventDefault();
        var _td = $(this).parent();
        var order = _td.index();
        var eleTable = _td.closest('.all-data-table');
        _td.remove();
        eleTable.find('tbody tr').each(function () {
            $(this).find('td').eq(order).remove();
        });
        updateValueScenarioStepsParams(eleTable, convertAndSaveValueDataTable(eleTable));
    });

    $('body').on('click', '#addNewDataSetRow, #listSteps .button-create, #listStepsBackground .button-create', function () {
        var eleTable = $(this).closest('.all-data-table');
        var lengthColumn = eleTable.find('th').length;
        var htmlColumn = '';
        eleTable.find('tbody span').removeClass('first-column');
        for (var i = 0; i < lengthColumn; i++) {
            if (i == 0) {
                htmlColumn += '<td class="datatable-cell-content first-column"><span></span></td>';
            } else {
                if (i == lengthColumn - 1) {
                    htmlColumn += '<td class="fixed-last-column">' +
                        '<button class="delete-table-row"><span class="fas fa-trash-alt"></span></button></td>';
                } else {
                    htmlColumn += '<td class="datatable-cell-content"><span></span></td>';
                }
            }
        }
        eleTable.find('tbody').append('<tr>' + htmlColumn + '</tr>');
        eleTable.find('.first-column').trigger('click');
    });

    function addColumnForDataTable(element) {
        element.find('.datatable-cell-content').removeClass('new-parameter-name');
        element.find('thead tr th:last').before('<th class="datatable-cell-content new-parameter-name">' +
            '<span>new_parameter</span>' +
            '<button class="delete-table-column"><i class="fas fa-trash-alt"></i></button></th>');
        element.find('tbody tr').each(function () {
            $(this).find('td:last').before('<td class="datatable-cell-content"><span></span></td>');
        });
        showHideBtnDataTable(true);
    }

    function updateExampleDataTable(element) {
        var params = JSON.stringify({
            example: convertAndSaveValueDataTable(element)
        });
        element.append(savingElement);
        API_UpdateScenario(localScenarioId, params).done(function () {
            element.find('.saving-data').remove();
        }).catch(function () { });
    }

    var listStepBackground = document.getElementById('listStepsBackground');
    Sortable.create(listStepBackground, {
        animation: 150,
        handle: '.order',
        onUpdate: function () {
            reindexlistSteps($('#listStepsBackground'));
        }
    });

    var listSteps = document.getElementById('listSteps');
    Sortable.create(listSteps, {
        animation: 150,
        handle: '.order',
        onUpdate: function () {
            reindexlistSteps($('#listSteps'));
        }
    });

    var listScenarios = document.getElementById('listScenarios');
    Sortable.create(listScenarios, {
        animation: 150,
        onUpdate: function () {
            reIndexScenario();
            countAllScenario();
        }
    });

    function viewDetailScenario(id, type, text, description, tagsScenario) {
        resetDataDetailScenario();
        localScenarioId = id;
        $('#outputScenarioName').val(text);
        if (description != '') {
            $('#outputDescriptionScenario').removeClass('no-description').val(description);
        } else {
            $('#outputDescriptionScenario').addClass('no-description').val('');
        }
        //render tags scenario
        if (tagsScenario && tagsScenario != '') {
            var arrTagsScenario = tagsScenario.split(',');
            $.each(arrTagsScenario, function (ind, val) {
                var valueTag = val.substr(1);
                var render = Mustache.render(tmplAddNewTag, {
                    textTag: valueTag
                });
                $('#listTagsScenario').find('.add-new-tag').before(render);
            });
        }
        //render table
        if (type == "ScenarioOutline") {
            $.each(allScenario, function (ind, val) {
                if (val.id === id) {
                    var examplesData = JSON.parse(val.example);
                    $('#allDataTable .table-background').html(renderTableData(examplesData));
                }
            });
            $('#collapseDataTable').collapse('show');
            showHideBtnDataTable(true);

        }
        getAllStepsOfScenario(id, $('#listSteps'));
        showDetailScenario(true);
        showSpinner(false);
    }

    function getAllStepsOfScenario(id, that) {
        that.attr('data-id', id);
        that.html('');
        ajaxRequest(requestAPI.scenario + '/' + id, 'GET', null).done(function (response) {
            // console.log("response scenario", response);
            var scenario_steps = response.scenario_steps;
            if (scenario_steps && scenario_steps != '') {
                $.each(scenario_steps, function (index, value) {
                    // console.log("Run add detail step scenario");
                    addDetailStepsScenario(that, value);
                });
                setSelectedForOptionKeyword(that);
                hideElement(that.closest('.all-steps').find('.text-no-step'));
            }
        })
    }

    function convertDataTableToGherkin(that) {
        var allValue = '';
        var valueHeader = '|';
        that.find('thead .datatable-cell-content').each(function () {
            valueHeader += ' ' + $(this).find('span').html() + ' |';
        });
        valueHeader += '\n';
        var valueBody = '';
        that.find('tbody tr').each(function () {
            valueBody += '|';
            $(this).find('.datatable-cell-content').each(function () {
                valueBody += ' ' + $(this).find('span').html() + ' |';
            });
            valueBody += '\n';
        });
        allValue = valueHeader + valueBody;
        return allValue;
    }

    function convertStepsToGherkin(that) {
        var allValue = '';
        /************** convert old version *********************************/
        // that.find('li').each(function(){
        //     var _this = $(this);
        //     allValue += _this.find('.keyword select').val();
        //     _this.find('.action-text-step').clone().appendTo(_this).hide().addClass('getTextStep');
        //     _this.find('.getTextStep .input-parameter').each(function(){
        //         var valInput = $(this).val();
        //         $(this).replaceWith('"'+valInput+'"');
        //     });
        //     allValue += ' ' + _this.find('.getTextStep').html() + '\n';
        //     _this.find('.getTextStep').remove();
        //     if (_this.find('.argument-docstring').hasClass('show')) {
        //         var docstring = _this.find('.argument-params').find('textarea').val();
        //         allValue += '"""\n'+ '  ' + docstring + '\n"""'; 
        //     }
        //     if (_this.find('.argument-datatable').hasClass('show')) {
        //         var dataTable = convertDataTableToGherkin(_this.find('.argument-datatable .table-background'));
        //         allValue += dataTable; 
        //     }
        // });
        /**************** convert same hiptest **********************/
        that.find('li').each(function () {
            var _this = $(this);
            allValue += _this.find('.keyword select').val() + ' ';
            allValue += _this.find('.hiddenTextNoParams').val();
            var lengthParam = _this.find('.action-text-step .input-parameter').length;
            if (lengthParam > 0) {
                allValue += ' (';
                _this.find('.action-text-step .input-parameter').each(function (index) {

                    var name = $(this).attr('data-param-name');
                    var value = $(this).val();
                    allValue += name + ' = "' + value + '"';
                    if (index < lengthParam - 1) {
                        allValue += ', ';
                    }
                });
                allValue += ')';
            }
            allValue += '\n';
            if (_this.find('.argument-docstring').hasClass('show')) {
                var docstring = _this.find('.argument-params').find('textarea').val();
                allValue += '"""\n' + '  ' + docstring + '\n"""';
            }
            if (_this.find('.argument-datatable').hasClass('show')) {
                var dataTable = convertDataTableToGherkin(_this.find('.argument-datatable .table-background'));
                allValue += dataTable;
            }
        });
        return allValue;
    }

    function convertDefinitionToGherkin(objDefinition) {
        var allValue = '';
        allValue += objDefinition.value;
        var headerTable = '';
        var bodyTable = '';
        $.each(objDefinition.steps_params, function (ind, val) {
            if (val.params_type === 'DocString') {
                allValue += '\n"""\n' + '  ' + val.value + '\n"""';
            }
            if (val.params_type === 'DataTable') {
                var valueTable = JSON.parse(val.value);
                $.each(valueTable, function (index, value) {
                    if (index === 0) {
                        headerTable += '\n|';
                        $.each(value, function (indx, valx) {
                            headerTable += ' ' + valx + ' |';
                        });
                        headerTable += '\n';
                    } else {
                        bodyTable += '|';
                        $.each(value, function (indx, valx) {
                            bodyTable += ' ' + valx + ' |';
                        });
                        bodyTable += '\n';
                    }
                });
            }
        });
        allValue += headerTable + bodyTable;
        return allValue;
    }

    function renderAllDataToTextArea() {
        var allValue = '';
        ajaxRequest(requestAPI.autoTestCase + autoTestCaseId, 'GET', null).done(function (res) {
            allValue = res.gherkin_content;
            if (allValue || allValue != null) {
                rawVersionAll.setValue(allValue);
                rawVersionAll.save();
            }
            showSpinner(false);
        });
    }

    function renderDataTableToTextArea() {
        rawVersionDataTable.setValue(convertDataTableToGherkin($('#allDataTable')));
        rawVersionDataTable.save();
    }

    function renderBackgroundToTextArea() {
        rawVersionBackground.setValue(convertStepsToGherkin($('#listStepsBackground')));
        rawVersionBackground.save();
    }

    function renderStepsToTextArea() {
        rawVersionStepsScenario.setValue(convertStepsToGherkin($('#listSteps')));
        rawVersionStepsScenario.save();
    }

    function setSelectedForOptionKeyword(that) {
        that.find('li').each(function () {
            var selected = $(this).find('.keyword').data('keyword').trim();
            $(this).find('select').val(selected);
        })
    }

    function resetDataFeature() {
        resetDataDetailScenario();
        $('#linkOpenedScenarioDetail').hide();
        $('#descriptionFeature').val('');
        $('#listScenarios').html('');
        showElement($('#text-no-scenario'));
        $('#listStepsBackground').html('');
        showElement($('#text-no-steps-background'));
        $('#screenRawVersionAll').hide();
        $('#screenEditorVersionAll').show();
        changeStatusButtonSwitchVersion($('#switch-version-all-content'), false);
    }

    function resetDataDetailScenario() {
        $('#outputScenarioName').val('');
        $('#descriptionScenario').val('');
        changeStatusButtonSwitchVersion($('#switch-version-data-table'), false);
        showhideRawVersion($('#allDataTable'), $('#rawDataTable'), false);
        $('#collapseDataTable').collapse('hide');
        showHideBtnDataTable(false);
        changeStatusButtonSwitchVersion($('#switch-version-step'), false);
        showhideRawVersion($('#allStepsScenario'), $('#rawStepsScenario'), false);
        $('#allDataTable').find('thead .datatable-cell-content').remove();
        $('#allDataTable').find('tbody tr').remove();
        $('#listSteps').html('');
        showElement($('#text-no-step'));
        $('#listTagsScenario').find('li').each(function () {
            if (!$(this).hasClass('add-new-tag')) {
                $(this).remove();
            }
        });
    }

    function showhideRawVersion(elementEditor, elementRaw, mode) {
        if (mode) {
            elementRaw.show();
            elementEditor.hide();
        } else {
            elementRaw.hide();
            elementEditor.show();
        }
    }

    function changeStatusButtonSwitchVersion(that, status) {
        if (status) { //switch to raw version
            that.addClass('show-raw-version').html('Editor version');
        } else { //switch to editor version
            that.removeClass('show-raw-version').html('Raw version');
        }
    }

    function showDetailScenario(mode) {
        if (mode) {
            $('#screenDetailScenario').show();
            $('#splitScenario').hide();
        } else {
            $('#screenDetailScenario').hide();
            $('#splitScenario').show();
        }
    }

    function showHidePanelSelectTestCase(mode, showTabsAction) {
        if (mode) {
            $('#action-selected').show();
            $('#no-action-selected').hide();
        } else {
            $('#no-action-selected').show();
            $('#action-selected').hide();
            if (showTabsAction) {
                $('#no-action-selected').find('.placeholder-section__legend').hide();
                $('#no-action-selected').find('.tab-wrapper-feature').show();
            } else {
                $('#no-action-selected').find('.placeholder-section__legend').show();
                $('#no-action-selected').find('.tab-wrapper-feature').hide();
            }
        }
    }

    function showHidePanelSelectConfig(mode, showBlockDetail) {
        if (mode) {
            $('#actionSelectedConfig').show();
            $('#noActionSelectedConfig').hide();
            if (showBlockDetail) {
                $('#blockEditSteps').show();
            } else {
                $('#blockEditSteps').hide();
            }
        } else {
            $('#actionSelectedConfig').hide();
            $('#noActionSelectedConfig').show();
        }
    }

    function sysnListTabsScenario(tagsScenario) {
        var listTags = '';
        if (tagsScenario && tagsScenario != '') {
            var arrTagsScenario = tagsScenario.split(',');
            $.each(arrTagsScenario, function (ind, val) {
                var valueTag = val.substr(1);
                listTags += '<li>' + valueTag + '</li>';
            });
        }
        return [listTags, tagsScenario];
    }

    function addScenarioWhenLoadFromFeatureOrCreateNew(orderId, type, name, id, descriptionScenario, tagsScenario) {
        var typeNoBackground = '';
        var listTags = sysnListTabsScenario(tagsScenario);
        if (type == 'Background') {
            typeNoBackground = 'noScenario';
        } else {
            typeNoBackground = 'scenario';
        }
        var render = Mustache.render(tmplNameScenario, {
            typeNoBackground: typeNoBackground,
            index: orderId,
            type: type,
            name: name,
            listTags: listTags[0],
            id: id,
            descriptionScenario: descriptionScenario,
            tagsScenario: listTags[1]
        });
        $('#listScenarios').append(render);
        hideElement($('#text-no-scenario'));
    }

    //reindex list scenario
    function reIndexScenario() {
        var arrScenarioId = [];
        var arrScenarioOrderId = [];
        $('#listScenarios > li').each(function () {
            $(this).find('.scenario-name-rendered').attr('data-index', $(this).index() + 1);
            var scenarioId = $(this).find('.scenario-name-rendered').data('id');
            var scenarioOrderId = $(this).find('.scenario-name-rendered').data('index');
            arrScenarioId.push(scenarioId);
            arrScenarioOrderId.push(scenarioOrderId);
        });
        var params = JSON.stringify({
            scenario_ids: arrScenarioId,
            order_ids: arrScenarioOrderId
        });
        var allScenarioElement = $('#listScenarios').closest('.all-steps');
        allScenarioElement.append(savingElement);
        ajaxRequest(requestAPI.autoTestCase + autoTestCaseId + '/update_scenario_order_id', 'PUT', params).done(function (res) {
            allScenarioElement.find('.saving-data').remove();
        }).catch(function (err) { });
    }

    //reindex list steps
    function reindexlistSteps(that) {
        var arrStepId = [];
        var arrStepOrderId = [];
        that.find('>li').each(function () {
            $(this).attr('data-order-id', $(this).index() + 1);
            $(this).find('.step-render .order span').html($(this).index() + 1);
            var stepId = $(this).data('step-id');
            var stepOrderId = $(this).data('order-id');
            arrStepId.push(stepId);
            arrStepOrderId.push(stepOrderId);
        });
        var params = JSON.stringify({
            step_ids: arrStepId,
            order_ids: arrStepOrderId
        });
        var allScenarioElement = that.closest('.all-steps');
        allScenarioElement.append(savingElement);
        var scenarioId = that.data('id')
        ajaxRequest(requestAPI.scenario + '/' + scenarioId + '/update_step_order_id', 'PUT', params).done(function (res) {
            allScenarioElement.find('.saving-data').remove();
        }).catch(function (err) { })
    }

    //count scenario
    function countAllScenario() {
        var allCount = $('#listScenarios').find('>li[data-scenario-type="scenario"]').length;
        if (allCount == 0) {
            $('#totalScenario').html('No Scenarios').attr('data-total', allCount);
        }
        if (allCount > 1) {
            $('#totalScenario').html(allCount + ' Scenarios').attr('data-total', allCount);
        } else {
            $('#totalScenario').html(allCount + ' Scenario').attr('data-total', allCount);
        }
    }

    //conver data table
    function convertAndSaveValueDataTable(that) {
        var arrContent = [];
        var arrContentHead = [];
        that.find('thead tr .datatable-cell-content').each(function () {
            var value = $(this).find('span').html();
            arrContentHead.push(value);
        });
        arrContent.push(arrContentHead);
        that.find('tbody tr').each(function () {
            var arrContentBody = [];
            $(this).find('.datatable-cell-content').each(function () {
                var value = $(this).find('span').html();
                arrContentBody.push(value);
            });
            arrContent.push(arrContentBody);
        });
        return JSON.stringify(arrContent);
    }

    //set width for input param in step
    function setWidthForInputParamStep_bk(that) {
        var size = that.val().length;
        if (size === 0) {
            size = 20;
        } else {
            size = (size + 1) * 8;
        }
        that.attr('style', 'width: ' + size + 'px');
    }

    function setWidthForInputParamStep(that) {
        var size = that.val().length;
        var placeholderSize = that.attr("placeholder").length;
        if (size === 0) {
            if (placeholderSize === 0) {
                size = 20;
            } else {
                size = (placeholderSize + 1) * 8;
            }

        } else {
            size = (size + 1) * 8;
        }
        that.attr('style', 'width: ' + size + 'px');
    }

    //add tag
    $('#tagsFeature, #listTagsScenario').on('click', '.tag-info .tag-name', function () {
        var text = $(this).html();
        $("#tagsFeature").next(".alert-warning").remove();

        if (!$(this).hasClass('edit-input')) {
            $(this).addClass('edit-input');
            if ($(this).hasClass('add-new')) {
                $(this).html('<input type="text" data-tag="" value="" class="input-tag">');
            } else {
                $(this).html('<input type="text" data-tag="' + text + '" value="' + text + '" class="input-tag">');
            }
            $(this).find('input').focus();
        }
    });

    $('#tagsFeature').on('focusout', '.input-tag', function () {
        var lengthOfTag = $(this).val().length;
        var regrexTag = /^[A-Za-z\d=_ ]+$/;

        if ($(this).val() !== '') {
            if (lengthOfTag <= 20 && regrexTag.exec($(this).val())) {//---> Only allow add tag less than 20 character
                $("#tagsFeature").next(".alert-warning").remove();
                var arrUpdateTags = addNewTag($(this));
                var totalLengthTag = 0;
                var jsonobj = $.parseJSON(arrUpdateTags[1]);
                totalLengthTag = jsonobj.tags.length;

                if (totalLengthTag <= 255) {//---> //---> Only allow All tags less than 255 character
                    API_UpdateAutoTestcase(autoTestCaseId, arrUpdateTags[1]).done(function (res) {
                        //---> Update Auto testcase success
                    }).catch(function (err) {
                        //---> Update Auto testcase success
                        //console.log('err', err);
                    });
                } else {
                    //---> Error occured when all tags > 255 characters
                    var messageOverFloat = "An error has occured. Sorry for the inconvenience";
                    var errTemplate = parseErrorTemplate(messageOverFloat);
                    $("#tagsFeature").after(errTemplate);
                }
            } else {
                var errorMessage = "Please add tag less than 20 characters or remove special characters.";
                var errTemplate = parseErrorTemplate(errorMessage);
                $("#tagsFeature").after(errTemplate);
            }
        } else {
            addNewTag($(this));
        }

    });

    function parseErrorTemplate(errorMessage) {
        var errTemplate = '<div class="alert alert-warning alert-dismissible fade show mt-10" role="alert">\
                                <strong>Bad format! </strong>' + errorMessage
            + '<button type="button" class="close" data-dismiss="alert" aria-label="Close">\
                                <span aria-hidden="true">&times;</span>\
                                </button>\
                            </div>';
        return errTemplate;
    }

    $('#listTagsScenario').on('focusout', '.input-tag', function () {
        var lengthOfTag = $(this).val().length;
        var regrexTag = /^[A-Za-z\d=_ ]+$/;

        if ($(this).val() !== '') {
            if (lengthOfTag <= 20 && regrexTag.exec($(this).val())) {//---> Only allow add tag less than 20 character
                $("#listTagsScenario").next(".alert-warning").remove();
                var arrUpdateTags = addNewTag($(this));
                var totalLengthTag = 0;
                var jsonobj = $.parseJSON(arrUpdateTags[1]);
                totalLengthTag = jsonobj.tags.length;

                if (totalLengthTag <= 255) {//---> //---> Only allow All tags less than 255 character
                    updateTagsOfScenario(localScenarioId, arrUpdateTags[1]);
                } else {
                    //---> Error occured when all tags > 255 characters
                    var messageOverFloat = "An error has occured. Sorry for the inconvenience";
                    var errTemplate = parseErrorTemplate(messageOverFloat);
                    $("#listTagsScenario").after(errTemplate);
                }
            } else {
                var errorMessage = "Please add tag less than 20 characters or remove special characters.";
                var errTemplate = parseErrorTemplate(errorMessage);
                $("#listTagsScenario").after(errTemplate);
            }
        } else {
            addNewTag($(this));
        }
       
    });


    $('#tagsFeature').on('click', 'i.fa', function () {
        $(this).parent().remove();
        var arrUpdateTags = getParamsListTags($('#tagsFeature'));
        API_UpdateAutoTestcase(autoTestCaseId, arrUpdateTags).done(function (res) { }).catch(function (err) { });
    });

    $('#listTagsScenario').on('click', 'i.fa', function () {
        $(this).parent().remove();
        var arrUpdateTags = getParamsListTags($('#listTagsScenario'));
        updateTagsOfScenario(localScenarioId, arrUpdateTags);
    });

    function updateTagsOfScenario(id, params) {
        API_UpdateScenario(id, params).done(function (res) {
            var listTags = sysnListTabsScenario(res.tags);
            $('#listScenarios').find('.scenario-name-rendered[data-id="' + res.id + '"] .tags-scenario').html(listTags[0]);
            $('#listScenarios').find('.scenario-name-rendered[data-id="' + res.id + '"] .inputTagsScenario').html(listTags[1]);
        }).catch(function (err) { });
    }

    function addNewTag(that) {
        var value = that.val();
        var dataTag = that.data('tag');
        var parent = that.parent();
        parent.removeClass('edit-input');
        if (value != '') {
            var arrValue = value.split(' ');
            var newValue = '';
            $.each(arrValue, function (ind, val) {
                newValue += val;
                if (ind < arrValue.length - 1) {
                    newValue += '_';
                }
            });
            if (parent.hasClass('add-new')) {
                var render = Mustache.render(tmplAddNewTag, {
                    textTag: newValue
                });
                parent.html('Add new tag');
                parent.closest('ul').find('.add-new-tag').before(render);
            } else {
                parent.html(value);
            }
        } else {
            if (parent.hasClass('add-new')) {
                parent.html('Add new tag');
            } else {
                parent.html(dataTag);
            }
        }
        that.remove();
        var params = getParamsListTags(parent.closest('ul'));
        return [localScenarioId, params];
    }

    function getParamsListTags(element) {
        var strTags = '';
        var lengthListTags = element.find('li').length;
        element.find('li').each(function (e) {
            if (!$(this).hasClass('add-new-tag')) {
                strTags = strTags + '@' + $(this).find('.tag-name').html();
                if (e < lengthListTags - 2) {
                    strTags = strTags + ',';
                }
            }
        });
        var params = JSON.stringify({
            tags: strTags
        });
        return params;
    }

    function renderTableData(arrValue) {
        var headerTable = '';
        var bodyTable = '';
        $.each(arrValue, function (index, value) {
            if (index === 0) {
                headerTable = '<tr>';
                $.each(value, function (ind, val) {
                    headerTable += '<th class="datatable-cell-content"><span>' + val + '</span>' +
                        '<button class="delete-table-column"><i class="fas fa-trash-alt"></i></button>' +
                        '</th>';
                });
                headerTable += '<th class="fixed-last-column">' +
                    '<button class="createDataHeader" title="Add new dataset">' +
                    '<span class="fa fa-plus-circle"></span></button></th>';
                headerTable += '</tr>';
            } else {
                bodyTable += '<tr>';
                $.each(value, function (ind, val) {
                    bodyTable += '<td class="datatable-cell-content"><span>' + val + '</span></td>';
                });
                bodyTable += '<td class="fixed-last-column">' +
                    '<button class="delete-table-row"><span class="fas fa-trash-alt"></span></button></td>';
                bodyTable += '</tr>';
            }
        });
        return '<table><thead>' + headerTable + '</thead><tbody>' + bodyTable + '</tbody></table>'
    }

    function showHideBtnDataTable(mode) {
        if (mode) {
            $('#switch-version-data-table').show();
            $('#addNewDataSetRow').show();
        } else {
            $('#switch-version-data-table').hide();
            $('#addNewDataSetRow').hide();
        }
    }

    function autoCompleteForInput(element, appendToElement) {
        // console.log('datasourceFromLibs ', datasourceFromLibs);

        element.autocomplete({
            minLength: 1,
            delay: 0,
            source: datasourceFromLibs,
            select: function (event, ui) {
                var params_name = '';
                var params_value = '';
                var params_type = '';
                $.each(ui.item.steps_params, function (ind, val) {
                    if (val.params_type != 'normal') {
                        params_name = val.name;
                        params_value = val.value;
                        params_type = val.params_type;
                    }
                });
                var _scenarioid = appendToElement.attr('data-id');
                if (!_scenarioid) {
                    // create scenario with type Background
                }
                var params = JSON.stringify({
                    steps_definition_id: ui.item.id,
                    name: ui.item.name,
                    scenario_id: _scenarioid,
                    project_id: projectId,
                    keyword: ui.item.keyword,
                    params_name: params_name,
                    params_value: params_value,
                    params_type: params_type
                });
                addNewStepOfScenario(params, appendToElement);
                $(this).val('');
                return false;
            }
        });
    }

    function autoCompleteForInputDefinition(element, appendToElement) {
        element.autocomplete({
            minLength: 1,
            delay: 0,
            source: datasourceOnlyLibrary,
            select: function (event, ui) {
                var content = $('#rawVersionDefinition').val();
                if (content === '') {
                    content += convertDefinitionToGherkin(ui.item);
                } else {
                    content += '\n' + convertDefinitionToGherkin(ui.item);
                }
                updateContentStepsDefinition($('#rawDefinition'), content);
                $(this).val('');
                return false;
            }
        })
    }

    function updateContentStepsDefinition(appendToElement, content) {
        var params = JSON.stringify({
            content: content
        })
        appendToElement.closest('.all-steps').append(savingElement);
        API_UpdateStepsDefinition(stepConfigId, params).done(function (response) {
            appendToElement.closest('.all-steps').find('.saving-data').remove();
            rawVersionDefinition.setValue(content);
            rawVersionDefinition.save();
            rawVersionDefinition.refresh();
            $('#rawDefinition').removeClass('change-codemirror');
        }).catch(function (err) { });
    }

    function addNewStepOfScenario(params, appendToElement) {
        var allStepsElement = appendToElement.closest('.all-steps');
        // console.log("saving element", savingElement);
        allStepsElement.append(savingElement);
        ajaxRequest(requestAPI.stepsDefinition, 'POST', params).done(function (response) {
            // console.log('response ', response);
            addDetailStepsScenario(appendToElement, response[response.length - 1]);
            allStepsElement.find('.saving-data').remove();
            setSelectedForOptionKeyword(appendToElement);
            getAllStepsDefinition();
        }).catch(function (error) {
            showNotifyCustomsStepsScenario(error.responseJSON.error, 'danger', 'right', 0);
        });
    }

    function addDetailStepsScenario(that, scenario_step) {
        //---> Get steps params
        var steps_params = scenario_step.scenario_steps_params;

        //---> Get Steps definition
        var value = scenario_step.steps_definition;

        //---> Get step param definition
        var definitionStepParams = scenario_step.steps_definition.steps_params;

        //---> Get step name
        var newName = value.name;

        //---> Define refex param
        var myRegexpParam = /(\".*?\")/g;

        var matches, indParam = 0;

        //---> Define flag show datatable, show doc string
        var isShowDataTable = '',
            isShowDocString = '';

        var textArgumentDataTable = '',
            textArgumentDocString = '';
        var argumentIdDataTable = '',
            argumentIdDocString = '';
        var valueArgumentDataTable = '',
            valueArgumentDocString = '';
        while (matches = myRegexpParam.exec(value.name)) {
            var jsonParam = steps_params[indParam];
            var definitionValueDetaultItem = definitionStepParams[indParam];
            if (jsonParam != '' && jsonParam && jsonParam.params_type == 'normal') {
                // console.log('normal');
                // console.log(jsonParam);
                var valueInput = jsonParam.value;
                //---> Format new name
                newName = empedStepParamValueToInput(newName, jsonParam, definitionValueDetaultItem, valueInput, matches);

                indParam++;
            }
            if (jsonParam != '' && jsonParam && jsonParam.params_type == 'argument') {
                // console.log('argument');
                // console.log(jsonParam);
                var valueInput = jsonParam.value;
                var replaceMatches = valueInput.replace(/\"/g, "");
                replaceMatches = replaceMatches.replace(/\</g, "= ");
                replaceMatches = replaceMatches.replace(/\>/g, "");

                // newName = newName.replace(matches[1],
                //     '<input class="input-parameter default-value-popover" placeholder="&laquo; &raquo;"' +
                //     'value="' + replaceMatches + '" data-param-id="' + jsonParam.id + '" data-param-name="' + jsonParam.name + '" data-param-default-value="' + jsonParam.value + '" data-content=\'&lt;a class="custom-popover reset-default-value" &gt;&lt;small&gt;&lt;i class="fas fa-ban"&gt;&lt;/i&gt; &lt;span&gt;Reset to "' + jsonParam.value + '"&lt;/span&gt;&lt;/small&gt;&lt;/a&gt;\' data-toggle="popover">');

                //---> Format new name
                newName = empedStepParamValueToInput(newName, jsonParam, definitionValueDetaultItem, valueInput, matches);
                indParam++;
            }
        }
        $.each(steps_params, function (indParam, valParam) {
            if (valParam.params_type == "DataTable") {
                isShowDataTable = "show";
                textArgumentDataTable = valParam.params_type;
                argumentIdDataTable = valParam.id;
                jsonDataTable = JSON.parse(valParam.value);
                valueArgumentDataTable = renderTableData(jsonDataTable);
            }
            if (valParam.params_type == "DocString") {
                isShowDocString = "show";
                textArgumentDocString = 'Free text';
                argumentIdDocString = valParam.id;
                valueArgumentDocString = valParam.value;
            }
        });
        var order = scenario_step.order_id;
        var scenario_step_id = scenario_step.id;
        var redersteps = Mustache.render(tmplStep, {
            order: order,
            scenarioStepId: scenario_step_id,
            stepId: value.id,
            keyword: scenario_step.keyword,
            hiddenTextNoParams: value.name,
            span: newName,
            isShowDataTable: isShowDataTable,
            textArgumentDataTable: textArgumentDataTable,
            argumentIdDataTable: argumentIdDataTable,
            htmlDataTable: valueArgumentDataTable,
            isShowDocString: isShowDocString,
            textArgumentDocString: textArgumentDocString,
            argumentIdDocString: argumentIdDocString,
            valueArgumentDocString: valueArgumentDocString
        });
        // console.log("============= Render steps =============");
        // console.log(redersteps);
        that.append(redersteps);
        $.each(steps_params, function (indParam, valParam) {
            if (jsonParam != '' && jsonParam && jsonParam.params_type == 'normal') {
                //---> Old code - Thinh added for ticket ITMS-636
                // that.find('input.input-parameter[data-param-id="' + valParam.id + '"]').val(valParam.value);

            }
        });
        that.find('input.input-parameter').each(function () {
            setWidthForInputParamStep($(this));
        });
        hideElement(that.closest('.all-steps').find('.text-no-step'));

        //---> Call init popover
        initPopover($('.default-value-popover'));
    }

    //---> Init popover
    function initPopover(element) {
        element.popover({
            container: 'body',
            placement: 'top',
            html: true,
            animation: true,
            trigger: 'focus'
        });
    }

    $(document).on('click', '.reset-default-value', function () {
        //---> Get param id
        var inputIDStr = "i-input-parameter-" + $(this).data('id');
        var inputID = $("#" + inputIDStr);

        //---> Dispose popover
        inputID.popover("dispose");

        //---> Reset value
        inputID.val('');
        var paramDefaultValue = inputID.data("param-default-value");
        inputID.attr("placeholder", paramDefaultValue);

        //---> Call API update value for Scenario Step Param
        updateValueScenarioStepsParams(inputID, paramDefaultValue);

        //---> Resize input
        setWidthForInputParamStep(inputID);
    });

    //---> Check scenario has step param value is not step param default value
    function isStepParamDefaultValue(scenarioStepParamValue, definitionStepParamValue) {
        if (scenarioStepParamValue === definitionStepParamValue) {
            return true;
        } else {
            return false;
        }
    }

    //---> Emped step param value to input
    function empedStepParamValueToInput(newName = "", jsonParam, definitionValueDetaultItem, valueInput, matches) {
        if (isStepParamDefaultValue(jsonParam.value, definitionValueDetaultItem.value)) {
            //---> Case: Scenario step param value === Definition step param value
            newName = newName.replace(matches[1],
                '<input id="i-input-parameter-' + jsonParam.id + '" class="input-parameter" placeholder="' + valueInput + '"' +
                ' data-param-id="' + jsonParam.id + '" data-param-name="' + jsonParam.name + '" data-param-default-value="' + definitionValueDetaultItem.value + '"'
                + ' data-content=\'&lt;a class="custom-popover reset-default-value" data-id="' + jsonParam.id + '" &gt;&lt;small&gt;&lt;i class="fas fa-ban"&gt;&lt;/i&gt; &lt;span&gt;Reset to "' + definitionValueDetaultItem.value
                + '"&lt;/span&gt;&lt;/small&gt;&lt;/a&gt;\' data-toggle="popover">');
        } else {
            if (valueInput === '') {//---> Case: Scenario step param value !== Definition step param value, but Scenario step param value is empty
                newName = newName.replace(matches[1],
                    '<input id="i-input-parameter-' + jsonParam.id + '" class="input-parameter default-value-popover" placeholder="' + valueInput + '" value="' + valueInput + '"' +
                    ' data-param-id="' + jsonParam.id + '" data-param-name="' + jsonParam.name + '" data-param-default-value="' + definitionValueDetaultItem.value + '"'
                    + ' data-content=\'&lt;a class="custom-popover reset-default-value" data-id="' + jsonParam.id + '" &gt;&lt;small&gt;&lt;i class="fas fa-ban"&gt;&lt;/i&gt; &lt;span&gt;Reset to "' + definitionValueDetaultItem.value
                    + '"&lt;/span&gt;&lt;/small&gt;&lt;/a&gt;\' data-toggle="popover">');
            } else {//---> Case: Scenario step param value !== Definition step param value, but Scenario step param value is not empty
                newName = newName.replace(matches[1],
                    '<input id="i-input-parameter-' + jsonParam.id + '" class="input-parameter default-value-popover" placeholder="' + valueInput + '"' + 'value="' + valueInput + '"' +
                    ' data-param-id="' + jsonParam.id + '" data-param-name="' + jsonParam.name + '" data-param-default-value="' + definitionValueDetaultItem.value + '"'
                    + ' data-content=\'&lt;a class="custom-popover reset-default-value" data-id="' + jsonParam.id + '" &gt;&lt;small&gt;&lt;i class="fas fa-ban"&gt;&lt;/i&gt; &lt;span&gt;Reset to "' + definitionValueDetaultItem.value
                    + '"&lt;/span&gt;&lt;/small&gt;&lt;/a&gt;\' data-toggle="popover">');
            }
        }
        return newName;
    }
});