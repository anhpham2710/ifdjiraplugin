/// <reference path="./common/constants.js" />
var MongoClient = require('mongodb').MongoClient;
var mongoURL = 'mongodb://10.5.1.239:27017/IFDTMS';
module.exports = {
    FindinAddonSettings: function(query) {
        return MongoClient.connect(mongoURL).then(function(db) {
            var collection = db.collection('AddonSettings');
            return collection.find(query).toArray();
            db.close();
        }).then(function(items) {
            return items;
        });
    }
};