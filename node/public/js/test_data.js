/// <reference path="./common/common.js" />
$(function() {
    ajaxRequest(requestURL + '/project?company_name=' + company_name + '&JIRAproject_id=' + jiraProjectId, "GET", null)
        .done(function(project_response) {
            project_id = project_response[0].id;
            ajaxRequest(requestURL + '/data_test?project_id=' + project_id, "GET", null)
                .done(function(data_test) {
                    bindDataToDataTest(data_test).done(function() {});
                }).catch(function(er) {
                    console.log('Could not load test data', er);
                });
        });

    var tableDataTest = $('#data-test-table').DataTable({
        data: null,
        columns: [{
                data: null,
                orderable: false,
                defaultContent: ''
            },
            {
                data: 'name'
            },
            {
                data: 'created_at'
            },
            {
                data: 'updated_at'
            }
        ]
    });


    function bindDataToDataTest(data) {
        var defer = $.Deferred();
        $.each(data, function() {
            tableDataTest.row.add(this).draw();
        });
        return defer.promise();
    }
});