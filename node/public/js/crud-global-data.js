const typeObject = 'ObjectRepositories';
const typeTestData = 'TestData';
const topNumberRow = 10;
var arrCodeMirror = [];
var arrObjects = null,
    arrTestData = null,
    tmpListTestData = '',
    tmpList = '',
    editorTestData = null;
var tmpNodata = '';
var dataInSearchObjRepo = null;
var dataInSearchTestData = null;

$(function () {
    $.get(urlTemplateRender + 'paging.html', function (template) {
        tmpPaging = template;
    });

    $.get(urlTemplateRender + 'no-data.html', function (template) {
        tmpNodata = template;
    });
    
    $.get(urlTemplateRender + 'global-data-child-row-test-data.html', function (template) {
        tmpListTestData = '{{#objects}}' + template + '{{/objects}}';
    });

    $.get(urlTemplateRender + 'global-data-child-row.html', function (template) {
        tmpList = '{{#objects}}' + template + '{{/objects}}';
    });

    $.get(urlTemplateRender + 'global-data-test-data.html', function (template) {
        $('#test-data-content').html(Mustache.render(template, {
            placeholder: 'Search test data',
            formName: 'formAddNewTestData',
            globalType: typeTestData,
            idElementSearchGlobalData: 'txtSearchTestData'
        }));
        editorTestData = setupGherkin('textAreaAddNewTestData', false, 'gherkin', false);
        editorTestData.on('keyup', function () {
            editorTestData.save();
        });

    });

    //get html template for child tab [Parameters,Objec repositories]    
    $.get(urlTemplateRender + 'global-data.html', function (template) {
        $('#object-repositories-content').html(Mustache.render(template, {
            placeholder: 'Search object repositories',
            formName: 'formAddNewObject',
            globalType: typeObject,
            idElementSearchGlobalData: 'txtSearchObjects'
        }));
    });

    $('.txtSearchGlobal').each(function () {
        $(this).on('keypress', function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                $(this).parent().find('button').trigger('click');
            }
        });
    });

    $('.txtInputNewRow').each(function () {
        $(this).on('keypress', function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                $(this).closest('.add-new-row').find('.btn-add-new-row').trigger('click');
            }
        });
    });

    $('body').on('keyup', '#txtSearchObjects', function(){
        searchGlobalData('ObjectRepositories');
    });

    $('body').on('keyup', '#txtSearchTestData', function(){
        searchGlobalData('TestData');
    });

    renderObjRepo();

    $('#submitFormObjectRepo').click(function(){
        showLoadingBar(true);
        var fileImportObjectRepo = $('#fileImportObjectRepo')[0].files[0];
        var formData = new FormData();
        formData.append('datafile', fileImportObjectRepo);
        formData.append('project_id', jiraProjectId);
        $('#fileImportObjectRepo').val('');
        ajaxRequestImport(requestURL + '/import/object_repo', 'POST', formData).done(function(res){
            if(jQuery.type(res.message)==="string"){
                showNotifyCustoms(res.message, 'success', 'center', 3000);
            } else{
                renderObjRepo();
            }
            showLoadingBar(false);
        }).catch(function(err){
            showLoadingBar(false);
            showNotifyCustoms(err.responseJSON.error, 'danger', 'right', 0);
        });
    });

    function renderObjRepo(){
        getObjecRepositories(topNumberRow).done(function (res) {
            var _res = _.forEach(res, function (o) {
                o.globalType = typeObject;
            });
            var objListData = {
                objects: paginate(_res, topNumberRow, 1)
            };
            arrObjects = _res;
            var rendered = Mustache.render(tmpList, objListData);
            $('#object-repositories-content').find('.grid-body').html(rendered);
            calculatePaging('#object-repositories-content', typeObject, arrObjects);
        }).catch(function (err) {
            $('#object-repositories-content').find('.grid-body').html($(tmpNodata));
            arrObjects = [];
        });
    }

    getTestDatas(topNumberRow).done(function (res) {
        var _res = _.forEach(res, function (o) {
            o.globalType = typeTestData;
        });
        var objListData = {
            objects: paginate(_res, topNumberRow, 1)
        };
        arrTestData = _res;
        var rendered = Mustache.render(tmpListTestData, objListData);
        $('#test-data-content').find('.grid-body').html(rendered);
        calculatePaging('#test-data-content', typeTestData, arrTestData);
        renderCodeMirrorTestData();
    }).catch(function (err) {
        $('#test-data-content').find('.grid-body').html($(tmpNodata));
        arrTestData = [];
    });
});

function deleteGlobalData(that, globalType) {
    var form = $(that).closest('form');
    var id = form.find('input[type="hidden"]').val();
    form.find('.deleteRow').hide();
    form.find('.spinner').fadeIn('fast', function () {
        switch (globalType) {
            case typeObject:
                deleteObjectRepository(id).done(function () {
                    getObjecRepositories(topNumberRow).done(function (res) {
                        var _res = _.forEach(res, function (o) {
                            o.globalType = typeObject;
                        });
                        var objListData = {
                            objects: paginate(_res, topNumberRow, 1)
                        };
                        arrObjects = _res;
                        var rendered = Mustache.render(tmpList, objListData);
                        $('#object-repositories-content').find('.grid-body').html(rendered);
                        calculatePaging('#object-repositories-content', typeObject, arrObjects);
                    }).catch(function (err) {
                        $('#object-repositories-content').find('.grid-body').html($(tmpNodata));
                        arrObjects = [];
                    });
                });
                break;
            case typeTestData:
                deleteTestData(id).done(function () {
                    getTestDatas(topNumberRow).done(function (res) {
                        var _res = _.forEach(res, function (o) {
                            o.globalType = typeTestData;
                        });
                        var objListData = {
                            objects: paginate(_res, topNumberRow, 1)
                        };
                        arrTestData = _res;
                        var rendered = Mustache.render(tmpListTestData, objListData);
                        $('#test-data-content').find('.grid-body').html(rendered);
                        renderCodeMirrorTestData();
                        calculatePaging('#test-data-content', typeTestData, arrTestData);
                    }).catch(function (err) {
                        $('#test-data-content').find('.grid-body').html($(tmpNodata));
                        arrTestData = [];
                    });
                });
                break;
        }
    });
}

function searchGlobalData(globalType) {
    'use strict';
    var fuseSearch = null;
    var result = null;
    var rendered = null;
    var objListData = null;

    function showResult(element, result) {
        objListData = {
            objects: paginate(result, topNumberRow, 1)
        };
        if (result.length > 0) {
            switch (globalType) {
                case typeObject:
                    rendered = Mustache.render(tmpList, objListData);
                    $(element).find('.grid-body').html(rendered);
                    break;
                case typeTestData:
                    rendered = Mustache.render(tmpListTestData, objListData);
                    $(element).find('.grid-body').html(rendered);
                    break;
            }
        } else {
            $(element).find('.grid-body').html($(tmpNodata));
        }
    }

    switch (globalType) {
        case typeObject:
            fuseSearch = new Fuse(arrObjects, optionsFuzzySearch);
            if ($('#txtSearchObjects').val().trim() != '') {
                result = fuseSearch.search($('#txtSearchObjects').val());
            } else
                result = arrObjects;
            showResult('#object-repositories-content', result);
            dataInSearchObjRepo = result;
            calculatePaging('#object-repositories-content', typeObject, result);
            break;
        case typeTestData:
            fuseSearch = new Fuse(arrTestData, optionsFuzzySearch);
            if ($('#txtSearchTestData').val().trim() != '') {
                result = fuseSearch.search($('#txtSearchTestData').val());
            } else
                result = arrTestData;
            showResult('#test-data-content', result);
            dataInSearchTestData = result;
            calculatePaging('#test-data-content', typeTestData, result);
            break;
    }
}

function cancelUpdateGlobalData(that) {
    var form = $(that).closest('form');
    form.find('.form-control').each(function (i) {
        $(this).val($(this).attr('value'));
    });
    form.validate().resetForm();
    form.find('.showHideButton, .flat-error, .spinner').fadeOut();
}

//update data on Objecs,Test data, Parameters
function updateGlobalData(that, globalType) {
    "use strict";
    const form = $(that).closest('form');
    var postData = {};
    form.serializeArray().map(function (x) {
        postData[x.name] = $.trim(x.value);
    });
    const elErr = form.find('.flat-error');
    const loading = form.find('.spinner');
    const deleteRow = form.find('.deleteRow');

    function updateSuccess(postData) {
        form.find('[name="name"]').attr('value', postData.name);
        form.find('[name="content"]').attr('value', postData.content);
        loading.fadeOut('fast');
        elErr.fadeOut();
        deleteRow.fadeIn();
    }

    function updateFail(err) {
        loading.fadeOut('fast', function () {
            deleteRow.fadeIn();
            elErr.attr('title', err.responseJSON.error);
            form.find('.showHideButton').fadeIn('fast', function () {
                elErr.fadeIn();
                elErr.tooltip();
            });
        });
    }

    function updateArrGlobalData(postData, arr, typeGlobal) {
        var idx = _.findIndex(arr, function (o) {
            return o.id == postData.id;
        });
        arr[idx] = Object.assign({
            globalType: typeGlobal
        }, postData);
    }

    if (form.valid()) {
        form.find('.showHideButton, .flat-error, .deleteRow').hide();
        loading.fadeIn('slow', function () {
            switch (globalType) {
                case typeObject:
                    updateObjectRepository(postData).done(function () {
                        updateSuccess(postData);
                        updateArrGlobalData(postData, arrObjects, typeObject);
                    }).catch(function (err) {
                        updateFail(err);
                    });
                    break;
                case typeTestData:
                    updateTestData(postData).done(function () {
                        updateSuccess(postData);
                        updateArrGlobalData(postData, arrTestData, typeTestData);
                    }).catch(function (err) {
                        updateFail(err);
                    });
                    break;
                default:
                    break;
            }
        });
    }
}

//Create new data for Objecs,Test data, Parameters
function addNewRow(globalType) {
    "use strict";

    function reRenderGridGlobalData(form, _res, arrData, elementHtml) {
        form.find('input[name="name"]').val('');
        form.find('input[name="content"]').val('');
        form.validate().resetForm();
        var loading = form.find('.spinner');
        var elErr = form.find('.flat-error');
        form.find('.btn-add-new-row').show();
        elErr.fadeOut();
        loading.fadeOut();

        arrData.push(_res);
        var objListData = {
            objects: paginate(arrData, topNumberRow, 1)
        };
        var rendered = '';
        if (globalType == typeTestData) {
            rendered = Mustache.render(tmpListTestData, objListData);
            $(elementHtml).find('.grid-body').html(rendered);
            renderCodeMirrorTestData();
        } else {
            rendered = Mustache.render(tmpList, objListData);
            $(elementHtml).find('.grid-body').html(rendered);
        }
    }

    function saveFail(form, err) {
        var loading = form.find('.spinner');
        var elErr = form.find('.flat-error');
        loading.fadeOut('fast', function () {
            elErr.attr('title', err.responseJSON.error);
            form.find('.btn-add-new-row').fadeIn('fast', function () {
                elErr.fadeIn();
                elErr.tooltip();
            });
        });
    }

    var postData = {};
    var form = null;
    switch (globalType) {
        case typeObject:
            form = $('#formAddNewObject');
            break;
        case typeTestData:
            form = $('#formAddNewTestData');
            break;
    }

    if (form.valid()) {
        form.serializeArray().map(function (x) {
            postData[x.name] = $.trim(x.value);
        });
        form.find('.btn-add-new-row').hide();
        const loading = form.find('.spinner');
        const elErr = form.find('.flat-error');
        elErr.fadeOut('fast');
        loading.fadeIn('fast', function () {
            switch (globalType) {
                case typeObject:
                    createNewObjectRepository(postData).done(function (res) {
                        var _res = Object.assign({
                            globalType: typeObject
                        }, res);
                        reRenderGridGlobalData(form, _res, arrObjects, '#object-repositories-content');
                        calculatePaging('#object-repositories-content', typeObject, arrObjects);
                    }).catch(function (err) {
                        saveFail(form, err);
                    });
                    break;
                case typeTestData:
                    createNewTestData(postData).done(function (res) {
                        var _res = Object.assign({
                            globalType: typeTestData
                        }, res);
                        editorTestData.setValue('');
                        reRenderGridGlobalData(form, _res, arrTestData, '#test-data-content');
                        calculatePaging('#test-data-content', typeTestData, arrTestData);
                    }).catch(function (err) {
                        saveFail(form, err);
                    });
                    break;
            }
        });
    }
}

//displaySaveButton on each rows of global data
function displaySaveButton(that) {
    var form = $(that).closest('form');
    form.find('.flat-error').fadeOut();
    var arr = [null, null];
    form.find('.form-control').each(function (i) {
        if ($(this).attr('value') === $(this).val()) {
            arr[i] = true;
        } else arr[i] = false;
    });
    var isDisplay = arr.find(function (o) {
        return o == false;
    });
    if (typeof isDisplay != 'undefined') {
        form.find('.showHideButton').fadeIn('fast');
    } else
        form.find('.showHideButton').fadeOut('fast');
}

function displayPage(that) {
    'use strict';
    var globalType = $(that).data('type');
    var index = $(that).data('index');
    var data = null;

    function showData(element, dataShow) {
        var rendered = '';
        if (globalType == typeTestData) {
            rendered = Mustache.render(tmpListTestData, dataShow);
            $(element).find('.grid-body').html(rendered);
            renderCodeMirrorTestData(element);
        } else {
            rendered = Mustache.render(tmpList, dataShow);
            $(element).find('.grid-body').html(rendered);
        }
    }
    
    $(that).closest('.pagination').find('.isactive').removeClass('isactive');
    $(that).parent().addClass('isactive');

    switch (globalType) {
        case typeObject:
            if (dataInSearchObjRepo != null) {
                data = paginate(dataInSearchObjRepo, topNumberRow, index);
            } else {
                data = paginate(arrObjects, topNumberRow, index);
            }
            showData('#object-repositories-content', {objects: data});
            break;
        case typeTestData:
            if (dataInSearchTestData != null) {
                data = paginate(dataInSearchTestData, topNumberRow, index);
            } else {
                data = paginate(arrTestData, topNumberRow, index);
            }
            showData('#test-data-content', {objects: data});          
            break;
    }
}

function calculatePaging(ele, globalType, dataArray) {
    'use strict';
    if (dataArray.length == 0) {
        $(ele).find('.paging nav').remove();
        return;
    }
    var paging = '';
    var totalPage = parseInt(dataArray.length / topNumberRow) + (dataArray.length % topNumberRow > 0 ? 1 : 0);
    for (var i = 1; i <= totalPage; i++) {
        var _active = '';
        if (i == 1) {
            _active = 'isactive';
        }
        paging += '<li class="page-item ' + _active + '"><a onclick="displayPage(this)" data-index="' + i + '" data-type="' + globalType + '" class="page-link" href="javascript:;">' + i + '</a></li>';
    }

    var rendered = Mustache.to_html(tmpPaging, {
        pagings: paging,
        globalType: globalType
    });
    $(ele).find('.paging').html(rendered);
}

function nextPage(that) {
    var ul = $(that).closest('.pagination');
    var indexActive = ul.find('.isactive').find('.page-link').data('index') + 1;
    var nextEleActive = ul.find('.page-link[data-index="' + indexActive + '"]');
    displayPage(nextEleActive);
}

function previousPage(that) {
    var ul = $(that).closest('.pagination');
    var indexActive = ul.find('.isactive').find('.page-link').data('index') - 1;
    var nextEleActive = ul.find('.page-link[data-index="' + indexActive + '"]');
    displayPage(nextEleActive);
}

function viewTestDataContent(that) {
    var form = $(that).closest('form');
    $(that).toggleClass('active');
    form.find('.row-test-data-content').slideToggle();
}

function renderCodeMirrorTestData() {
    var textareas = document.getElementsByClassName('textarea-test-data');
    arrCodeMirror = [];
    for (var i = 0; i < textareas.length; i++) {
        var mirror = CodeMirror.fromTextArea(textareas[i], {
            mode: 'gherkin',
            theme: "eclipse",
            lineNumbers: false,
            autoRefresh: true
        });
        arrCodeMirror.push(mirror);
    }
    arrCodeMirror.forEach(function (editor) {
        editor.on('keyup', function () {
            editor.save();
            $(editor.getTextArea()).trigger('keyup');
        });
    });
}