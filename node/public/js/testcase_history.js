/// <reference path="./common/common.js" />
var issueId = null;
$(function () {
    "use strict";
    var testcaseHistoryTable = $('#testcase-history-table').DataTable({
        data: null,
        autoWidth: false,
        order: [
            [3, 'desc']
        ],
        rowGroup: {
            dataSrc: 'updated_at'
        },
        columns: [{
                data: 'field_name'
            },
            {
                data: 'original_value'
            },
            {
                data: 'new_value'
            },
            {
                data: 'updated_at',
                visible: false
            }
        ],
    });
    console.log('getProjectDetail');
    getProjectDetail().done(function (res) {
        var project = res[0];
        var project_id = project.id;
        ajaxRequest(requestAPI.getIssueDetail, 'GET', {
            project_id: project_id,
            JIRAticket_key: issueKey
        }).done(function (res) {
            console.log('getIssueDetail', res);
            issueId = res[0].id;
            ajaxRequest(requestAPI.manualTestCase, "GET", {
                issue_id: issueId
            }).done(function (res) {
                console.log('test execution res', res);
                $.each(res, function () {
                    var tc = this;
                    var testcase_id = tc.id;
                    ajaxRequest(requestAPI.manualTestCase + '/' + testcase_id + "/history", "GET", null).done(function (res) {
                        var d = [];
                        $.each(res, function () {
                            var _d = $.Deferred();
                            d.push(_d.promise());
                            var tce = this;
                            var objTestDetailsHistory = {
                                field_name: tce.field_name,
                                original_value: tce.old_value,
                                new_value: tce.new_value,
                                updated_at: tce.updated_at
                            };

                            testcaseHistoryTable.row.add(objTestDetailsHistory);
                            _d.resolve();
                        });

                        $.when.apply($, d).done(function () {
                            testcaseHistoryTable.draw();
                        });
                    });
                });
            });
        });

    });
});