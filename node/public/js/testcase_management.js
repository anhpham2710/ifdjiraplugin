var issueId = paramsUrl.issueId;
var userId = paramsUrl.user_id;
var userKey = paramsUrl.user_key;
var localStorageTestCase = [];
var tmplListAutoTestCase = '';
var jiraTicketId = null;
var jiraTicketKey = null;
var jiraTicketTitle = null;
var usersJira = null;
var arrJiraVersion = null;
var lodash = _;

$.get(urlTemplateRender + 'issue-auto-testcase.html', function (template) {
    tmplListAutoTestCase = template;
});

AP.request('/rest/api/2/user/assignable/search?project=' + jiraProjectId, {
    success: function (res) {
        usersJira = lodash.filter(JSON.parse(res), function (o) {
            return o.emailAddress.indexOf("@connect.atlassian.com") === -1;
        });
    }
});

function searchTestCase(that, issueType){
    loadAllTestCase(fuzzySearch($(that).val(), localStorageTestCase), issueType);
}

function initLoadAllFeature(JIRAprojectKey, issueType){
    console.log('initLoadAllFeature')
    localStorageTestCase = [];
    $('#reloadFeature span').addClass('fa-spin');
    $('#resultTestCase').html('');
    AP.request(baseUrl+'/rest/api/2/search?jql=project=\'' + JIRAprojectKey + '\'AND issuetype IN ('+issueType+')&maxResults=100', {
        success: function (res) {
            var alljson = JSON.parse(res);
            $.each(alljson.issues, function(index, value){
                // if(value.fields.issuetype.name == issueType){
                    var params = JSON.stringify({
                        projectId: projectId,
                        jiraTicketId: value.id,
                        key: value.key,
                        summary: value.fields.summary
                    });
                    localStorageTestCase.push(JSON.parse(params));
                // }
            });
            loadAllTestCase(localStorageTestCase, issueType);
        }
    });
}

function loadAllTestCase(allIssue, issueType) {
    if(allIssue.length > 0) {
        var renderListAutoTestCase = '';
        if(issueType == "MANUAL-TEST"){
            var urlIcon = '../../images/manual.png';
        }
        if(issueType == "AUTO-TEST"){
            var urlIcon = '../../images/auto.png';
        }
        
        $.each(allIssue, function(index, value){
            renderListAutoTestCase += Mustache.render(tmplListAutoTestCase, {
                projectId: value.projectId,
                jiraTicketId: value.jiraTicketId,
                key: value.key,
                summary: value.summary,
                urlIcon: urlIcon
            });
        });
        $('#resultTestCase').html(renderListAutoTestCase);
        $('#reloadFeature span').removeClass('fa-spin');
        showSpinner(false);
    } else {
        $('#resultTestCase').html('<li class="list-group-item">No data found</li>');
        showSpinner(false);
    }
}

function showSpinner(show){
    if(show){
        $('#spinnerScenario').fadeIn();
    } else{
        $('#spinnerScenario').fadeOut();
    }
}

function togglePanel(that){
    $(that).toggleClass('move-left')
    $('.left-content').toggleClass('move-left');
    $('.right-content').toggleClass('full-width');
    $('#btnOpenPanelAddSteps').toggleClass('move-left');
    $('#importManualTestcase').toggleClass('move-left');
}

function showLoadingBar(mode){
    if(mode){
        $('#body-loading-bar').show();
    } else{
        $('#body-loading-bar').hide();
    }
}