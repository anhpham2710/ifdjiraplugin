$(function () {
    var urlExecution = requestURL + '/execution_automation/' + planId;

    $('#cancel-execution').on('click', function () {
        $(this).parents('.confirmation-execution').fadeOut("fast");
    });

    $('#confirm_exec_debug').on('click', function () {
        $(this).parent().find('.confirmation-execution').fadeIn("slow");
    });
    $('#cancel-exec-debug').on('click', function () {
        $(this).parents('.confirmation-execution').fadeOut("fast");
    });
    $("#exec_debug").click(function () {
        var that = $('#confirm_exec_debug');
        $(this).parents('.confirmation-execution').fadeOut("fast");
        showSpinner(that);
        ajaxRequest(urlExecution + '?execution_type=debug&action_type=execute', "POST", null).then(function (response) {
            var strMessage = JSON.parse(response).message;
            showNotify(strMessage, "custom-1");
            hideSpinner(that);
        });
    });

    $('#cancel-stop').on('click', function () {
        $(this).parents('.confirmation-execution').fadeOut("fast");
    });


    $('#confirm_stop_debug').on('click', function () {
        $(this).parent().find('.confirmation-execution').fadeIn("slow");
    });

    $('#cancel-stop-debug').on('click', function () {
        $(this).parents('.confirmation-execution').fadeOut("fast");
    });

    $("#stop_debug").click(function () {
        var that = $('#confirm_stop_debug');
        $(this).parents('.confirmation-execution').fadeOut("fast");
        showSpinner(that);
        ajaxRequest(urlExecution + '?execution_type=debug&action_type=stop', "POST", null).then(function (response) {
            hideSpinner(that);
            var strMessage = JSON.parse(response).message;
            showNotify(strMessage, "custom-1");
        });
    });


    $("#get_log_debug").click(function () {
        var that = this;
        showSpinner(that);
        ajaxRequest(urlExecution + '?execution_type=debug&action_type=log', "GET", null).then(function (response) {
            if (String(response).toLocaleUpperCase() !== 'NULL') {
                showLog(JSON.parse(response).message);
            } else
                showNotify(response, "custom-1");
            hideSpinner(that);
        });
    });

    $("#get_status_debug").click(function () {
        var that = this;
        showSpinner(that);
        ajaxRequest(urlExecution + '?execution_type=debug&action_type=status', "GET", null).then(function (response) {
            hideSpinner(that);
            var strMessage = JSON.parse(response).message;
            showNotify(strMessage, "custom-1");
        });
    });

    initData();

    function showSpinner(that) {
        $(spinner).hide();
        $(that).append(spinner);
        $(that).find('.spinner').fadeIn();
    }

    function hideSpinner(that) {
        $(that).find('.spinner').fadeOut("slow", function () {
            $(that).find('.spinner').remove();
        });
    }

    $.when.apply($, deferredInit1).done(function () {
        setTimeout(function () {
            // automationHistoryTable.draw();
            automationHistoryTable.columns.adjust().draw();
        }, 1000);
    });

    $.when.apply($, deferredInit2).done(function () {
        setTimeout(function () {
            // automationHistoryTable.draw();
            automationExecutionHistoryTable.columns.adjust().draw();
        }, 1000);
    });

    function showLog(logContent) {
        $('#modal-show-log').modal('show');
        $('#txt-show-log').val(logContent);
        setTimeout(function () {
            log.setValue(logContent);
            log.hasFocus();
        }, 500);
    }

    function initData() {
        log = CodeMirror.fromTextArea(document.getElementById("txt-show-log"), {
            lineNumbers: false,
            mode: "application/x-powershell",
            readOnly: "nocursor",
            theme: "base16-light",
            autofocus: true
        });

        var _params = {
            cycle_id: cycleId,
            issue_id: issueId
        };

        var deferredInit = [];

        var defferGetTestExcution = $.Deferred();
        deferredInit.push(defferGetTestExcution.promise());
        ajaxRequest(requestAPI.getTestExecution, "GET", _params).done(function (res) {
            testExecution = res[0];
            defferGetTestExcution.resolve();
        });

    }
});