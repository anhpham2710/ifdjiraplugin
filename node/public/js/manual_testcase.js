var lodash = _;
var projectId;
var arrDefect = [];
var arrIssueSelected = [];
$(function () {
    var arrTestCaseExecution = [];
    var datatable_tableversion_manual_testcase = $('#main-tableVersion-manual-testcase').DataTable({
        data: null,
        paging: false,
        searching: false,
        info: false,
        ordering: false,
        autoWidth: false,
        rowReorder: true,
        columns: [{
                data: null,
                class: 'order_id',
                defaultContent: ''
            },
            {
                className: 'test_step',
                data: 'test_step',
                render: function (data, type, row, meta) {
                    if (data != null) {
                        var rplData = data.replace(/(?:\r\n|\r|\n)/g, '<br />');
                        return '<p class="m-0 p-0">' + rplData + '</p>';
                    } else {
                        return '<p class="m-0 p-0">' + data + '</p>';
                    }
                }
            },
            {
                className: 'test_data',
                data: 'test_data',
                render: function (data, type, row, meta) {
                    if (data != null) {
                        var rplData = data.replace(/(?:\r\n|\r|\n)/g, '<br />');
                        return '<p class="m-0 p-0">' + rplData + '</p>';
                    } else {
                        return '<p class="m-0 p-0">' + data + '</p>';
                    }
                }
            },
            {
                className: 'expected_result',
                data: 'expected_result',
                render: function (data, type, row, meta) {
                    if (data != null) {
                        var rplData = data.replace(/(?:\r\n|\r|\n)/g, '<br />');
                        return '<p class="m-0 p-0">' + rplData + '</p>';
                    } else {
                        return '<p class="m-0 p-0">' + data + '</p>';
                    }
                }
            },
            {
                data: null,
                width: '96px',
                className: 'text-right functional-button',
                defaultContent: addButtonsFunctional('normal')
            }
        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).on('click', 'td:not(:first-child):not(:last-child)', function () {
                var tr = $(this).parent('tr');
                if (tr.hasClass('editing')) {
                    return;
                }
                tr.addClass('editing');
                var heightStep = tr.find('.test_step').height();
                var heightData = tr.find('.test_data').height();
                var heightResult = tr.find('.expected_result').height();
                tr.find('p').hide();
                var data = datatable_tableversion_manual_testcase.row(tr).data();
                tr.find('.test_step').append('<textarea style="resize: auto; min-height: ' + heightStep + 'px;  max-width: 250px; min-width:190px"; position: fixed; name="step" data-field-name="step" class="form-control">' + data.test_step + '</textarea>');
                tr.find('.test_data').append('<textarea style="resize: auto; min-height: ' + heightData + 'px;  max-width: 250px; min-width:190px"; position: fixed; name="data" data-field-name="data" class="form-control">' + data.test_data + '</textarea>');
                tr.find('.expected_result').append('<textarea style="resize: auto; min-height: ' + heightResult + 'px;  max-width: 250px; min-width:190px"; position: fixed; name="result" data-field-name="result" class="form-control">' + data.expected_result + '</textarea>');
                tr.find('.dropdownMenuButton').hide();
                tr.find('.functional-button').append(addButtonsFunctional('editing'));
            });
            $(nRow).on('click', '#btnCancelStep', function () {
                var tr = $(this).parents('tr');
                resetRow(tr);
            });
            $(nRow).on('click', '#btnCancelDelete', function () {
                var tr = $(this).parents('tr');
                resetRow(tr);
            });
            var index = iDisplayIndex + 1;
            $('td:eq(0)', nRow).html(index);
            return nRow;
        }
    });
    init();

    IFDRequest.customRequest('/rest/api/2/project/' + projectKey + '/versions', "GET", "application/json", null).then(function (response) {
        arrJiraVersion = JSON.parse(response.body);
    }).catch(function (er) {});

    var tblTestCaseExcution = $('#manual_testcase_execution_panel').DataTable({
        paging: false,
        searching: false,
        info: false,
        ordering: false,
        autoWidth: false,
        data: null,
        columns: [{
                data: 'version',
                className: 'td-status',
                render: function (data, type, row, meta) {
                    var versionName = lodash.find(arrJiraVersion, function (o) {
                        return o.id == data;
                    });
                    return versionName ? versionName.name : data;
                }
            },
            {
                data: 'test_cycle'
            },
            {
                data: 'status',
                className: 'td-status',
                render: function (data, type, row, meta) {
                    var color = 'badge-secondary';
                    switch (data.trim()) {
                        case 'FAIL':
                            color = 'badge-danger'; // FAIL
                            break;
                        case 'BLOCKED':
                            color = 'badge-warning'; // BLOCK
                            break;
                        case 'WIP':
                            color = 'badge-info'; // WIP
                            break;
                        case 'PASS':
                            color = 'badge-success'; // PASS
                            break;
                        default:
                            break;
                    }
                    return '<span class="btn badge badge-pill ' + color + ' change-status btn-block">' + data + '</span>';
                }
            },

           
            {
                data: 'defects',
                width: '195px',
                "render": function (data, type, row, meta) {
                    var tmpl = '';
                    $.each(data, function () {
                        tmpl += '<a class="btn ' + this.statusColor + ' badge badge-pill badge-secondary mr-1"  data-toggle="tooltip"  title="' + this.sumaryAndName + '" target="_blank" href=' + projectBaseUrl + this.value + '>' + this.value + '</a>';
                    });
                    return '<div class="inner-td">' + tmpl + '</div>';
                }
            },
            
            // {
            //     data: 'defects',
            //     width: '195px',
            //     "render": function (data, type, row, meta) {
            //         var tmpl = '';  
            //         var defects = data != null ? data.split('|') : [];       
            //         $.each(defects,function(){
            //             tmpl += '<a class="btn badge badge-pill badge-secondary mr-1"  data-toggle="tooltip"  target="_blank" href=' + projectBaseUrl + this + '>' + this + '</a>';
            //         });
                   
            //         return '<div class="inner-td">' + tmpl + '</div>';
            //     }
            // },
            {
                data: function (row, type, val, meta) {
                    if (row !== null && row.executed_by !== null) {
                        var _user = lodash.find(usersJira, function (o) {
                            return o.emailAddress == row.executed_by;
                        });
                        if (typeof _user !== 'undefined') {
                            return _user.displayName;
                        }
                    }
                    return row.executed_by;
                }
            },
            {
                data: function (row, type, val, meta) {
                    if (row.executed_on === row.created_at) {
                        return '';
                    }
                    return row.executed_on ? moment(row.executed_on).format('DD/MM/YYYY') : '';
                }
            },
            {
                data: null,
                width: 100,
                orderable: false,
                render: function (data, type, row, meta) {
                    return '<a class="btn btn-link py-1 px-2" href=' + baseUrl + '/plugins/servlet/ac/Infodation-tms/manual-execution?project.key=' + projectKey +
                        '&ac.projectId=' + jiraProjectId +
                        '&ac.issueId=' + data.issue_id +
                        '&ac.cycleId=' + data.cycle_id +
                        '&ac.cycleName=' + escape(data.test_cycle) +
                        '&ac.planId=' + data.plan_id + ' target="_blank"><span class="oi" data-glyph="play-circle"></span></a>';
                }
            }
        ]
    });

    function resetRow(tr) {
        tr.removeClass('editing');
        tr.find('p').show();
        tr.find('textarea').remove();
        tr.find('.dropdownMenuButton').show();
        tr.find('#fcBtn').remove();
        tr.find('#fcBtn_delete').remove();
        tr.find('.error').remove();
    }

    function addButtonsFunctional(typeButton) {
        if (typeButton == 'editing') {
            return '<div id="fcBtn"><button id="btnUpdateStep" type="button" class="btn btn-link"><span class="fa fa-check"></span></button>' +
                '<button id="btnCancelStep" type="button" class="btn btn-link"><span class="fa fa-times"></span></button></div>';
        }

        if (typeButton == 'normal') {
            return '<div class="dropdown">' +
                '<button class="btn btn-link dropdown-toggle dropdownMenuButton" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                '<span class="fa fa-cog"></span></button>' +
                '<div class="dropdown-menu dropdown-menu-right p-0" aria-labelledby="dropdownMenuButton">' +
                '<button class="btn btn-link btn-action-testcase btn-clone-testcase" type="button"><span class="fa fa-clone"></span> Clone</button >' +
                '<button class="btn btn-link btn-action-testcase btn-delete-testcase" type="button"><span class="fa fa-trash-o"></span> Delete</button >' +
                '</div></div>';
        }

        if (typeButton == 'confirmDelete') {
            return '<div id="fcBtn_delete">' +
                '<button id="btnCancelDelete" type="button" class="btn btn-danger" title="Delete"><span class="fas fa-times"></span></button>&nbsp;&nbsp;' +
                '<button id="btnDeleteYes" type="button" class="btn btn-success" title="Cancel"><span class="fas fa-check"></span></button>' + '</div>';
        }
    }

    $('#resultTestCase').on('click', '.list-group-item', function () {
        if (!$(this).hasClass('showing')) {
            $('#resultTestCase').addClass('showing-details').find('.list-group-item').removeClass('showing');
            $('#importManualTestcase').addClass('showing-detail');
            $(this).addClass('showing');
            jiraTicketId = $(this).data('jira-id');
            jiraTicketKey = $(this).find('.issue-key').html();
            jiraTicketTitle = $(this).find('.issue-title').html();
            showSpinner(true);
            getManualTestCaseDetail(projectId, jiraTicketId, jiraTicketKey, jiraTicketTitle);
            // getManualTestCaseExecution(projectId, jiraTicketId, jiraTicketKey, jiraTicketTitle);
        }
    });

    $('#refresh-data').on('click', function () {
        getManualTestCaseDetail(projectId, jiraTicketId, jiraTicketKey, jiraTicketTitle);
    });

    //change height of text area to fit height content
    $('.add-new-content').on('change keyup keydown paste cut', function () {
        $(this).height(0).height(this.scrollHeight);
    }).change();

    //drag an drog table, update order_id of manual testcase
    var idTableVersion = document.getElementById('tableVersion');
    Sortable.create(idTableVersion, {
        animation: 150,
        onUpdate: function () {
            var arrOrder = [];
            var arrId = [];
            $('#tableVersion').find('>tr').each(function () {
                var orderId = $(this).index() + 1;
                var _tr = $(this);
                $(this).find('.order_id').html(orderId);
                arrOrder.push(orderId);
                var data = datatable_tableversion_manual_testcase.row(_tr).data();
                arrId.push(data.id);
            });
            var paramsData = JSON.stringify({
                order_ids: arrOrder,
                testcase_ids: arrId
            });
            ajaxRequest(requestAPI.manualTestCase + issueId + '/update_order_id', 'PUT', paramsData).done(function (res) {});
        }
    });

    //get details of testcase
    function getManualTestCaseDetail(projectId, jiraTicketId, jiraTicketKey, jiraTicketTitle) {
        $('#refresh-data .fas').addClass('fa-spin');
        ajaxRequest(requestAPI.getIssueDetail, 'GET', {
            project_id: projectId,
            JIRAticket_key: jiraTicketKey
        }).done(function (res) {
            issueId = res[0].id;
            var testcase = res[0].cases;
            renderGUITestCase(testcase);
            var projectBaseUrl = baseUrl + "/browse/";
            $("#JIRAticketKey").html("Back to" + "&nbsp;" + res[0].JIRAticket_key).attr("href", projectBaseUrl + res[0].JIRAticket_key);
            getManualTestCaseExecution(projectId, jiraTicketId, jiraTicketKey, jiraTicketTitle)
            showPanelDetail();
            showSpinner(false);
            $('#refresh-data .fas').removeClass('fa-spin');
            $('#refresh-data-execution .fas').removeClass('fa-spin');
            
        }).catch(function (err) {
            // showHidePanelSelectTestCase(false, true);
            showSpinner(false);
        });
    }

    function renderGUITestCase(testcase) {
        datatable_tableversion_manual_testcase.clear();
        datatable_tableversion_manual_testcase.rows.add(testcase);
        datatable_tableversion_manual_testcase.columns.adjust().draw(false);
    }

    $('body').on('keyup', 'form textarea', function () {
        removeAllValid(this);
    });

    $('body').on('keyup', 'tr textarea', function () {
        removeAllValidForTrTable(this);
    });

    $('#btnAddStep').click(function () {
        var parentDiv = $('#addNewStep');
        var that = this;
        var validate = null;
        parentDiv.find('form textarea').each(function () {
            if ($(this).val().trim() != '') {
                validate = true;
            } else {
                validate = false;
            }
            if (validate) return false;
        });

        if (validate) {
            $(that).html('<span class="fa fa-spinner fa-pulse"></span>');
            $(that).attr('disabled', true);
            var paramsData = JSON.stringify({
                issue_id: issueId,
                test_step: parentDiv.find("[data-field-name=step]").val(),
                test_data: parentDiv.find("[data-field-name=data]").val(),
                expected_result: parentDiv.find("[data-field-name=result]").val(),
                // created_by: currentUser.emailAddress,
                order_id: datatable_tableversion_manual_testcase.data().count() + 1
            });
            // console.log('manual ', paramsData);
            ajaxRequest(requestAPI.manualTestCase, 'POST', paramsData).done(function (res) {
                datatable_tableversion_manual_testcase.row.add(res);
                datatable_tableversion_manual_testcase.columns.adjust().draw(false);
                parentDiv.find("[data-field-name=step]").val('');
                parentDiv.find("[data-field-name=data]").val('');
                parentDiv.find("[data-field-name=result]").val('');
                parentDiv.find('.error').remove();
                $(that).html('<span class="fa fa-plus-circle"></span>');
                $(that).removeAttr('disabled');
            });
        } else {
            // parentDiv.find('form').valid();
            var isValid = false;
            var strValue = $(that).val();
            //  console.log("strValue", strValue);
            if ($.trim(strValue) === '') {
                     isValid = false;
                     if ($(that).parent().has("label.error").length) {
                      return isValid;
                  }
                  $(that).closest('form').find('textarea').addClass('is-invalid');
                    $(that).addClass('is-invalid');
                    $(that).parent().append('<label class="error invisible"></label>');
             } else {
                    $(that).removeClass('is-invalid');
                    $(that).parent().find('label.error').remove();
                     isValid = true;
             }
    
                return isValid;
        }
    });

    $('#main-tableVersion-manual-testcase').on('click', '#btnUpdateStep', function () {
        var _tr = $(this).parents('tr');
        var validate = null;
        _tr.find('textarea').each(function () {
            if ($(this).val().trim() != '') {
                validate = true;
            } else {
                validate = false;
            }
            if (validate) return false;
        });

        if (validate) {
            $(this).html('<span class="fa fa-spinner fa-pulse"></span>');
            var data = datatable_tableversion_manual_testcase.row(_tr).data();
            var paramsData = JSON.stringify({
                test_step: _tr.find('[data-field-name=step]').val(),
                test_data: _tr.find('[data-field-name=data]').val(),
                expected_result: _tr.find('[data-field-name=result]').val(),
                modified_by: currentUser.emailAddress
            });
            ajaxRequest(requestAPI.manualTestCase + data.id, 'PUT', paramsData).done(function (res) {
                resetRow(_tr);
                datatable_tableversion_manual_testcase.row(_tr).data(res);
                datatable_tableversion_manual_testcase.columns.adjust().draw(false);
            });
        } else {
            _tr.find('textarea').each(function () {
                validationInput(this);
            });
        }
    });

    $('#main-tableVersion-manual-testcase').on('click', '.btn-clone-testcase', function () {
        var _tr = $(this).parents('tr');
        var data = datatable_tableversion_manual_testcase.row(_tr).data();
        var paramsData = JSON.stringify({
            issue_id: issueId,
            test_step: data.test_step,
            test_data: data.test_data,
            expected_result: data.expected_result,
            modified_by: currentUser.emailAddress,
            created_by: currentUser.emailAddress,
            order_id: datatable_tableversion_manual_testcase.data().count() + 1
        });
        ajaxRequest(requestAPI.manualTestCase, 'POST', paramsData).done(function (res) {
            datatable_tableversion_manual_testcase.row.add(res);
            datatable_tableversion_manual_testcase.columns.adjust().draw(false);
        });
    });

    $('#main-tableVersion-manual-testcase').on('click', '.btn-delete-testcase', function () {
        var _tr = $(this).parents('tr');
        var data = datatable_tableversion_manual_testcase.row(_tr).data();
        _tr.find('td.functional-button').append(addButtonsFunctional('confirmDelete'));
    });

    $('#main-tableVersion-manual-testcase').on('click', '#btnDeleteYes', function () {
        $(this).html('<span class="fa fa-spinner fa-pulse"></span>');
        $(this).attr('disabled', true);
        var _tr = $(this).parents('tr');
        var data = datatable_tableversion_manual_testcase.row(_tr).data();
        ajaxRequest(requestAPI.manualTestCase + data.id, 'DELETE').done(function (res) {
            datatable_tableversion_manual_testcase.row(_tr).remove();
            datatable_tableversion_manual_testcase.columns.adjust().draw(false);
        });
    });

    function init() {
        var originValObjTestCase = $('#originValObjTestCase').val();
        projectId = $('#projectId').val();
        if (originValObjTestCase && originValObjTestCase != '') {
            localStorageTestCase = JSON.parse(originValObjTestCase);
        } else {
            localStorageTestCase = '';
        }
        showPanelNoAction();
        showSpinner(false);
        $('#spinnerScenario').addClass('spinner-content');
    }

    function showPanelDetail() {
        $('#action-selected').show();
        $('#no-action-selected').hide();
        $('#panelImport').hide();
    }

    function showPanelNoAction() {
        $('#action-selected').hide();
        $('#panelImport').hide();
        $('#no-action-selected').show();
    }

    function showPanelImport() {
        $('#panelImport').show();
        $('#action-selected').hide();
        $('#no-action-selected').hide();
    }

    function removeAllValid(that) {
        // $(that).closest('form').find('textarea').removeClass('is-invalid');
        // $(that).parent().find('label.error').remove();
        if(that.value.trim() !== ''){
            $(that).closest('form').find('textarea').removeClass('is-invalid');
            $(that).parent().find('label.error').remove();
        // console.log("that", that.value);
        }else{
            $(that).addClass('is-invalid');
        } 
    }

    function removeAllValidForTrTable(that) {
        $(that).closest('tr').find('textarea').removeClass('is-invalid');
        $(that).parent().find('label.error').remove();
    }

    function validationInput(that) {
        var isValid = false;
        var strValue = $(that).val();
        if ($.trim(strValue) === '') {
            isValid = false;
            if ($(that).parent().has("label.error").length) {
                return isValid;
            }
            $(that).addClass('is-invalid');
            $(that).parent().append('<label class="error invisible"></label>');
        } else {
            $(that).removeClass('is-invalid');
            $(that).parent().find('label.error').remove();
            isValid = true;
        }
        return isValid;
    }

    $('#refresh-data-execution').on('click', function () {
        getManualTestCaseExecution(projectId, jiraTicketId, jiraTicketKey, jiraTicketTitle);
    });
    function getManualTestCaseExecution(projectId, jiraTicketId, jiraTicketKey, jiraTicketTitle) {
        
        $('#refresh-data-execution .fas').addClass('fa-spin');
        ajaxRequest(requestAPI.getIssueDetail, 'GET', {
            project_id: projectId,
            JIRAticket_key: jiraTicketKey
        }).done(function (res) {
            var mainGetIssuesDeffer = [];
            arrTestCaseExecution = [];
            var testcycles = res[0].cycles;
            var testplans = res[0].plans;
            var testplans_arr = [];
                $.each(testplans, function (index, plan) {
                    var testplan = {
                        status: plan.status,
                        defects: plan.defects,
                        // executed_by: plan.modified_by,
                        executed_by: plan.executed_by,
                        created_at: plan.created_at,
                        executed_on: plan.updated_at,
                        issue_id: plan.issue_id,
                        cycle_id: plan.cycle_id,
                        plan_id: plan.id
                    };
                    testplans_arr.push(testplan);
                });
                var testcycle_arry = [];
                $.each(testcycles, function (index, cycle) {
                    var testcycle = {
                        version: cycle.JIRAversion,
                        test_cycle: cycle.name,
                    };
                    testcycle_arry.push(testcycle);
                });
                $.each(testplans_arr, function (i, item) {
                    var TestCaseExecution = $.extend(true, testplans_arr[i], testcycle_arry[i]);
                    arrTestCaseExecution.push(TestCaseExecution);
                });
                $.each(arrTestCaseExecution, function (i, val) {
                    var mainDeferGetDefectDetail = [];
                    var arrDefect = [];
                    var _defects = val.defects != null ? val.defects.split('|') : [];
                    $.each(_defects, function (i, value) {
                        var def = $.Deferred();
                        mainDeferGetDefectDetail.push(def.promise());
                        mainGetIssuesDeffer.push(def.promise());
                        AP.request(baseUrl + '/rest/api/2/search?jql=issue=\'' + value + '\'&fields=summary,status', {
                            success: function (res) {
                                var detailDefect = {
                                    statusColor: "",
                                    sumaryAndName: '',
                                    value: '',
                                };
                                var datasource = JSON.parse(res);
                                var issue = datasource && datasource["issues"].length && datasource["issues"]["0"];
                                var status = issue && issue.fields ? issue.fields.status : undefined;
                                var summaryTextBug = issue.fields.summary;
                                var statusColor;
                                sumaryAndName = 'Summary : ' + summaryTextBug.toString() + "&#013;" + 'Status : ' + status.name.toString();

                                colorStatusBug = status.name;
                                if (colorStatusBug == "To Do") {
                                    statusColor = "statusToDoColor";
                                } else if (colorStatusBug == "Done") {
                                    statusColor = "statusDoneColor";
                                } else {
                                    statusColor = "statusInProgressColor";
                                }

                                detailDefect.statusColor = statusColor;
                                detailDefect.sumaryAndName = sumaryAndName;
                                detailDefect.value = value;
                                arrDefect.push(detailDefect);
                                def.resolve();
                            },
                            error: function () {
                                def.resolve();
                            }
                        });
                    });

                    $.when.apply($, mainDeferGetDefectDetail).done(function () {
                        arrTestCaseExecution[i].defects = arrDefect;
                    });
                });
                $.when.apply($, mainGetIssuesDeffer).done(function () {
                    setTimeout(function () {
                        tblTestCaseExcution.clear();
                        tblTestCaseExcution.rows.add(arrTestCaseExecution).draw();
                        $('#refresh-data-execution .fas').removeClass('fa-spin');
                    }, 500);
                });

        }).catch(function (err) {
            $('#refresh-data-execution .fas').removeClass('fa-spin');
        });

    }
   

    $('#openDialogTestCycle').click(function () {
        ajaxRequest(requestURL + '/cycle?project_id=' + projectId + '', "GET", null).done(function (testCycleData) {
            if (testCycleData.length > 0) {
                $('#project_cycle option').remove();
                $('#project_cycle').removeAttr('disabled');
                // var cycleName = $('<optgroup label="Select Test Cycle"></optgroup>');
                $.each(testCycleData, function () {
                    var option = '<option value="' + this.id + '">' + this.name + '</option>';
                    $('#selectCycle').append(option);
                });
                // $('#project_cycle').append(cycleName);
            }
            $('#add-test-cycle').removeAttr('disabled');
        }).catch(function (er) {
            $('#project_cycle').html('<option>No cycles found</option>');
        });

        IFDRequest.customRequest('/rest/api/2/user/assignable/search?project=' + projectKey, 'GET', 'application/json', null).then(function (res) {
            userJira = lodash.filter(JSON.parse(res.body), function (o) {
                return o.emailAddress.indexOf("@connect.atlassian.com") === -1;
            });
            $('#project_user option').remove();
            $('#project_user').removeAttr('disabled');
            $.each(userJira, function () {
                var that = this;
                var optionTag = '<option value=' + that.emailAddress + '>' + that.displayName + '</option>';
                $('#project_user').append(optionTag);
            });
        });
    });
    $('#add-test-cycle').on('click', function () {

        var projectCycleId = $('#project_cycle').val();
        arrIssueSelected.push(issueId);
        if (arrIssueSelected.length > 0) {
            var data = {
                "issue_ids": arrIssueSelected
            }
            $('#add-test-cycle').append(spinner);
            ajaxRequest(requestURL + '/plan/add_issues/' + projectCycleId, "POST", JSON.stringify(data)).done(function (res) {
                $('#add-test-cycle').find('.spinner').remove();
                if ($('#project_user').val() !== '') {
                    updatePlanTestCase(projectCycleId, res[0].issue_id, {
                            assigned_to: $('#project_user').val()
                        }).done(function (res) {
                            $('#modalAddTestCycle').modal('hide');
                            getManualTestCaseExecution(projectId, jiraTicketId, jiraTicketKey, jiraTicketTitle)
                        })
                        .catch(function (err) {
                            $('#error-add-testcycle').html(err.error);
                        });
                } else {
                    $('#modalAddTestCycle').modal('hide');
                }
            }).catch(function (err) {
                $('#error-add-testcycle').html(err.responseJSON.error);
                $('#add-test-cycle').find('.spinner').remove();
            });
        }
    });
    $('#importManualTestcase').click(function () {
        $('#testCaseName').closest('.form-group').removeClass('d-none');
        $('#jiraTicketKey').closest('.form-group').removeClass('d-none');
        $('#submitFormManual').removeClass('import-single');
        showPanelImport();
    });

    $('#importThisTestCase').click(function () {
        $('#testCaseName').closest('.form-group').addClass('d-none');
        $('#jiraTicketKey').closest('.form-group').addClass('d-none');
        $('#submitFormManual').addClass('import-single');
        showPanelImport();
    });

    $('#cancelFormManual').click(function () {
        if ($('#resultTestCase').hasClass('showing-details')) {
            showPanelDetail();
        } else {
            showPanelNoAction();
        }
    });

    $('#submitFormManual').on('click', function () {
        if (requiredInput($(this))) {
            showLoadingBar(true);
            if ($(this).hasClass('import-single')) {
                importSingleValueManual();
            } else {
                importMultiValueManual();
            }
        }
    });

    function requiredInput(that) {
        that.closest('.validate-form').find('input').each(function () {
            if ($(this).val() === '') {
                $(this).addClass('error');
            } else {
                $(this).removeClass('error');
            }
        });
        if (that.closest('.validate-form').find('input.error').length > 0) {
            return false;
        } else {
            return true;
        }
    }

    function importSingleValueManual() {
        var fileImport = $('#fileImport')[0].files[0];
        var sheetName = $('#sheetName').val();
        var testStep = $('#testStep').val();
        var testData = $('#testData').val();
        var exceptedResult = $('#exceptedResult').val();

        var formData = new FormData();
        formData.append('datafile', fileImport);

        formData.append('sheet_name', sheetName);
        formData.append('excel_column_test_step', testStep);
        formData.append('excel_column_test_data', testData);
        formData.append('excel_column_expected_result', exceptedResult);
        formData.append('project_id', jiraProjectId);

        ajaxRequestImport(requestURL + '/import/manual_testcase/' + issueId, 'POST', formData).done(function (res) {
            var listNewTestCase = res.message;
            if (Array.isArray(res.message)) {
                renderGUITestCase(listNewTestCase);
                showPanelDetail();
                showLoadingBar(false);
                showNotifyCustoms('Import success', 'success', 'right', 2000);
            } else {
                showLoadingBar(false);
                $.notify(res.message, "warn");
            }
        }).catch(function (err) {
            showLoadingBar(false);
            showNotifyCustoms(err.responseJSON.error, 'danger', 'right', 2000);
        });
    }

    function importMultiValueManual() {
        var fileImport = $('#fileImport')[0].files[0];
        var sheetName = $('#sheetName').val();
        var testCaseName = $('#testCaseName').val();
        var jiraTicketKey = $('#jiraTicketKey').val();
        var testStep = $('#testStep').val();
        var testData = $('#testData').val();
        var exceptedResult = $('#exceptedResult').val();

        var formData = new FormData();
        formData.append('datafile', fileImport);

        formData.append('sheet_name', sheetName);
        formData.append('excel_column_ticket_key', jiraTicketKey);
        formData.append('excel_column_testcase_name', testCaseName);
        formData.append('excel_column_test_step', testStep);
        formData.append('excel_column_test_data', testData);
        formData.append('excel_column_expected_result', exceptedResult);
        formData.append('project_id', projectId);

        ajaxRequestImport(requestURL + '/import/manual_testcase/', 'POST', formData).done(function (res) {
            showNotifyCustoms('Import success, click any testcase in left side to view detail', 'success', 'right', 0);
            $('#resultTestCase').find('.list-group-item').removeClass('showing');
            showPanelNoAction();
            showLoadingBar(false);
        }).catch(function (err) {
            showLoadingBar(false);
            showNotifyCustoms(err.responseJSON.error, 'danger', 'right', 0);
        });
    }

});

var tab_text = '';
var data_type = 'data:application/vnd.ms-excel';

function exportData(body) {
    var arrHead = ['Test Case Name', 'Jira Ticket Key', 'Test Step', 'Test Data', 'Expected Result'];
    var columnHead = '';
    $.each(arrHead, function (index, value) {
        columnHead += '<td><b>' + value + '</b></td>';
    });
    var TableMarkUp = '';
    TableMarkUp = '<table id="myModifiedTable"';
    TableMarkUp += '<thead><tr>' + columnHead + '</tr></thead>';
    TableMarkUp += '<tbody>' + body + '</tbody>';
    TableMarkUp += '</table>';

    $('#MessageHolder').append(TableMarkUp);
    tab_text = '';
    tab_text = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
    tab_text += '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';
    tab_text += '<x:Name>Manual</x:Name>';
    tab_text += '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
    tab_text += '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';
    tab_text += '<table>';
    tab_text += $('#myModifiedTable').html();
    tab_text += '</table></body></html>';

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        if (window.navigator.msSaveBlob) {
            var blob = new Blob([tab_text], {
                type: "application/csv;charset=utf-8;"
            });
            navigator.msSaveBlob(blob, 'iTMS_Manual_Exported_Testcases.xls');
        }
    } else {
        $('#testAnchor').removeAttr('href download');
        $('#testAnchor')[0].click();
    }
    $('#MessageHolder').html("");
}

function createBodyTable(ListOfMessages, jiraTicketKey, jiraTicketTitle) {
    var columnBody = '';
    if (ListOfMessages.length > 0) {
        $.each(ListOfMessages, function (index, value) {
            columnBody += '<tr>';
            if (index === 0) {
                columnBody += '<td>' + jiraTicketTitle + '</td>';
                columnBody += '<td>' + jiraTicketKey + '</td>';
            } else {
                columnBody += '<td></td>';
                columnBody += '<td></td>';
            }
            columnBody += '<td>' + value.test_step + '</td>';
            columnBody += '<td>' + value.test_data + '</td>';
            columnBody += '<td>' + value.expected_result + '</td>';
            columnBody += '</tr>';
        });
    } else {
        columnBody += '<tr>';
        columnBody += '<td>' + jiraTicketTitle + '</td>';
        columnBody += '<td>' + jiraTicketKey + '</td>';
        columnBody += '<td></td>';
        columnBody += '<td></td>';
        columnBody += '<td></td>';
        columnBody += '</tr>';
    }
    return columnBody;
}

function fnExcelReportSingle() {
    ExportReportSinggle(projectId, jiraTicketKey);
}

function ExportReportSinggle(_projectId, _jiraTicketKey) {
    var ListOfMessages = null;
    var allBody = '';
    ajaxRequest(requestAPI.getIssueDetail, 'GET', {
        project_id: projectId,
        JIRAticket_key: jiraTicketKey
    }).done(function (res) {
        ListOfMessages = res[0].cases;
        allBody = createBodyTable(ListOfMessages, res[0].JIRAticket_key, res[0].JIRAticket_title);
        exportData(allBody);
    });
}

function fnExcelReportAll() {
    var listDetailTestCase = [];
    // AP.request(baseUrl + '/rest/api/2/search?jql=project=\'' + projectKey + '\'&fields=summary,issuetype', {
    AP.request(baseUrl + '/rest/api/2/search?jql=project=\'' + projectKey + '\'AND issuetype IN (MANUAL-TEST)&maxResults=100', {
        success: function (res) {
            var alljson = JSON.parse(res);
            $.each(alljson.issues, function (index, value) {
                // if (value.fields.issuetype.name == 'MANUAL-TEST') {
                    listDetailTestCase.push(value.key);
                // }
            });
            var promises = [];
            var rows = [
                ['Test Case Name', 'Jira Ticket Key', 'Test Step', 'Test Data', 'Expected Result']
            ];
            $.each(listDetailTestCase, function (ind, val) {
                promises.push(function () {
                    return $.Deferred(function (dfd) {
                        ajaxRequest(requestAPI.getIssueDetail, 'GET', {
                            project_id: projectId,
                            JIRAticket_key: val
                        }).done(function (res) {
                            var ListOfMessages = res[0].cases;
                            if (ListOfMessages.length > 0) {
                                for (var index = 0; index < ListOfMessages.length; index++) {
                                    var row = ['', '',
                                        ListOfMessages[index].test_step,
                                        ListOfMessages[index].test_data,
                                        ListOfMessages[index].expected_result
                                    ];
                                    if (index === 0) {
                                        row[0] = res[0].JIRAticket_title;
                                        row[1] = res[0].JIRAticket_key;
                                    }
                                    // console.info('row', row);
                                    rows.push(row);
                                }
                            }
                            // console.info('rows', rows);
                            // var allBody = createBodyTable(ListOfMessages, res[0].JIRAticket_key, res[0].JIRAticket_title);

                            dfd.resolve(rows);
                        });
                    }).promise();
                });
            });
            $.when(all(promises)).then(function () {
                // console.log('results ', results);
                // var htmlBody = '';
                // $.each(results, function (ind, val) {
                //     htmlBody += val;
                // });
                // exportData(htmlBody);
                var filename = "iTMS_ALL_Manual_Testcases.xlsx";
                var ws_name = "ALL-Manual-testcase";
                // console.info('rows', rows);
                var wb = XLSX.utils.book_new(),
                    ws = XLSX.utils.aoa_to_sheet(rows);

                /* add worksheet to workbook */
                XLSX.utils.book_append_sheet(wb, ws, ws_name);
                XLSX.writeFile(wb, filename);
            });
        }
    });
    var all = function (array) {
        var deferred = $.Deferred();
        var fulfilled = 0,
            length = array.length;
        var results = [];

        if (length === 0) {
            deferred.resolve(results);
        } else {
            array.forEach(function (promise, i) {
                $.when(promise()).then(function (value) {
                    results[i] = value;
                    fulfilled++;
                    if (fulfilled === length) {
                        deferred.resolve(results);
                    }
                });
            });
        }
        return deferred.promise();
    };
}

$($("#testAnchor")[0]).click(function () {
    $('#testAnchor').attr('href', data_type + ', ' + encodeURIComponent(tab_text));
    $('#testAnchor').attr('download', 'iTMS_Manual_Exported_Testcases.xls');
});
