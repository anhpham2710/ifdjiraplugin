/// <reference path="configuration_services.js" />
var project_id = null;
var datasourceFromLibs = null;
var localStorageStepsDefinition = [];
var tmplSearhResult = '';
var tmplStepsParam = '';


CodeMirror.defineExtension("autoFormatRange", function (from, to) {
    var cm = this;
    var outer = cm.getMode(), text = cm.getRange(from, to).split("\n");
    var state = CodeMirror.copyState(outer, cm.getTokenAt(from).state);
    var tabSize = cm.getOption("tabSize");
    var out = "", lines = 0, atSol = from.ch == 0;

    function newline() {
        out += "\n";
        atSol = true;
        ++lines;
    }

    for (var i = 0; i < text.length; ++i) {
        var stream = new CodeMirror.StringStream(text[i], tabSize);
        while (!stream.eol()) {
            var inner = CodeMirror.innerMode(outer, state);
            var style = outer.token(stream, state), cur = stream.current();
            stream.start = stream.pos;
            if (!atSol || /\S/.test(cur)) {
                out += cur;
                atSol = false;
            }
            if (!atSol && inner.mode.newlineAfterToken &&
                inner.mode.newlineAfterToken(style, cur, stream.string.slice(stream.pos) || text[i + 1] || "", inner.state))
                newline();
        }
        if (!stream.pos && outer.blankLine) outer.blankLine(state);
        if (!atSol) newline();
    }

    cm.operation(function () {
        cm.replaceRange(out, from, to);
        for (var cur = from.line + 1, end = from.line + lines; cur <= end; ++cur)
            cm.indentLine(cur, "smart");
    });
});

$(function () {
    CodeMirror.commands.autocomplete = function (cm) {
        cm.showHint({
            hint: autocompleteForCodeMirror,
            completeSingle: false
        });
    };

    // rawVersion.on('keyup', function (cm, event) {
    //     event.preventDefault();
    //     if (!cm.state.completionActive && event.keyCode != 13 && event.keyCode != 27) {
    //         CodeMirror.commands.autocomplete(cm);
    //     }
    //     rawVersion.save();
    //     updateStepsDefinition();
    // });

    $('#nameStepsDefinition').focusout(function(){
        if($(this).hasClass('changed')){
            $(this).removeClass('changed');
            loadStepsParamWhenPostStepsDefinition();
        }
    });

    $('#deleteSure').on('click', function(){
        deleteStepsDefinition();
    });

    var tooltip = '';
    tooltip+='<div>When cursor is in the editor:<div>'
    tooltip+='<div>Press F11  to toggle full screen editing</div>';
    tooltip+='<div>Press Esc to exit full screen editing</div>';
    tooltip+='<div>Press Ctrl-F / Cmd-F to start searching</div>';
    tooltip+='<div>Press Ctrl-G / Cmd-G to find next</div>';
    tooltip+='<div>Press Shift-Ctrl-G / Shift-Cmd-G to find previous</div>';
    tooltip+='<div>Press Shift-Ctrl-F / Cmd-Option-F to replace</div>';
    tooltip+='<div>Press Shift-Ctrl-R / Shift-Cmd-Option-F to replace all</div>';
    tooltip+='<div>Press Alt-F to start searching</div>';
    $('#tooltip-guide').attr('title', tooltip);

    $('#tooltip-guide').tooltip();

    $('#newStepsDefinition').on('click', function () {
        showHidePanelSelectConfig(true);
        showHidePanelParameterAndStep(false);
        $('#idStepsDefinition').val('');
        $('#nameStepsDefinition').val('');
        rawVersion.setValue('');
        $('#allParameters').html('');
        $('#resultSteps .list-unstyled-item').removeClass('show-item');
    });
});

function autocompleteForCodeMirror(cm, option) {
    return new Promise(function (accept) {
        setTimeout(function () {
            var suggestPhrase = [];
            var cursor = cm.getCursor(),
                line = cm.getLine(cursor.line);
            var start = cursor.ch;
            var end = cursor.ch;
            for (var i = 0; i < datasourceFromLibs.length; i++) {
                if (~datasourceFromLibs[i].name.toLowerCase().indexOf(line.toLowerCase())) {
                    suggestPhrase.push(datasourceFromLibs[i].name);
                }
            }
            if (suggestPhrase.length > 0) {
                return accept({
                    list: suggestPhrase, //datasourceFromLibs[i].name,
                    from: CodeMirror.Pos(cursor.line, 0),
                    to: CodeMirror.Pos(cursor.line, end)
                });
            }
            return accept(null);
        }, 100);
    });
}

function postStepsDefinition(name) {
    var tempData = {
        name: name,
        project_id: project_id
    }
    return ajaxRequest(requestURL + '/steps_definition', 'POST', JSON.stringify(tempData));
}

function putNameAndParamsDefinition(id, name){
    var tempData = {
        name: name
    };
    if (name == null) {
        delete tempData.name;
    }
    return ajaxRequest(requestURL + '/steps_definition/' + id, 'PUT', JSON.stringify(tempData));
}

function putStepsDefinition(id, name, content){
    var tempData = {
        name: name,
        content: content
    };
    if (name == null) {
        delete tempData.name;
    }
    if (content == null) {
        delete tempData.content;
    }
    return ajaxRequest(requestURL + '/steps_definition/' + id, 'PUT', JSON.stringify(tempData));
}

function loadStepsParamWhenPostStepsDefinition(){
    setTimeout(function(){
        rawVersion.refresh();
    }, 100);
    $('#body-spiner').stop().fadeIn();
    if($('#idStepsDefinition').val()==''){
        showHidePanelParameterAndStep(true);
        var name =  $('#nameStepsDefinition').val();
        postStepsDefinition(name).done(function(res){
            $('#body-spiner').stop().fadeOut();
            showNotify('Create success','success');
            $('#idStepsDefinition').val(res.id);
            loadAllStepsParam(res);
            $('#resultSteps .list-unstyled-item').removeClass('show-item');
            $('#resultSteps .list-unstyled').append('<li class="list-unstyled-item show-item">'+
            '<a href="javascript:viewDetailStepsDefinition('+res.id+');" title="'+name+'">'+name+'</a></li>');
        }).catch(function(err){
            $('#body-spiner').stop().fadeOut();
            showNotify(err.responseJSON.error,'custom-1');
            $('#nameStepsDefinition').val('');
        });
    } else{
        updateNameAndStepsParamDefinition();
    }
}

function loadAllStepsParam(res){
    var stepsParam = res.steps_params;
    var rendered = '';
    if(stepsParam!=''){
        for (i=0; i<stepsParam.length; i++){
            rendered += Mustache.render(tmplStepsParam, {
                id : stepsParam[i].id,
                name : stepsParam[i].name,
                value : stepsParam[i].value
            });
        }
        $('#allParameters').html(rendered);
        $('#collapse-parameter').collapse('show');
    } else{
        $('#collapse-parameter').collapse('hide');
    }
}

function updateNameAndStepsParamDefinition(){
    var id = $('#idStepsDefinition').val();
    var name = $('#nameStepsDefinition').val();
    putNameAndParamsDefinition(id, name).done(function(res){
        showNotify('Save success','success');
        loadAllStepsParam(res);
        $('#body-spiner').stop().fadeOut();
        $('#resultSteps').find('.show-item a').attr('title', name).html(name);
    });
}

function updateStepsDefinition(){
    var id = $('#idStepsDefinition').val();
    var name = $('#nameStepsDefinition').val();
    var content = $('#rawVersionStepsDefinition').val();
    var allStepsDefinition = $('#rawVersionStepsDefinition').val();
    putStepsDefinition(id, name, content).done(function(res){
        
    });
}


function getStepsDefinitionByProject() {
    return ajaxRequest(requestURL + '/steps_definition', 'GET', {
        project_id: project_id
    });
}

function validateInput(ele){
    // var alphanumers = /^[a-zA-Z0-9" ]+$/;
    var alphanumers = /^[ ]+$/;
    if(!alphanumers.test(ele.val())){
        ele.addClass('error').focus();
        return false;
    } else{
        ele.removeClass('error');
        return true;
    }
}

function showHidePanelParameterAndStep(mode){
    if(mode){
        $('#panel-steps-param').show();
        $('#row-version').show();
    } else{
        $('#panel-steps-param').hide();
        $('#row-version').hide();
    }
}