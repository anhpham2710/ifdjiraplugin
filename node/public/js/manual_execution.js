/// <reference path="./common/constants.js" />
/// <reference path="./common/request.js" />
/// <reference path="./common/common.js" />

var lodash = _;
var userJira = [];

var issueId = paramsUrl.issueId;
var cycleId = paramsUrl.cycleId;
var cycleName = paramsUrl.cycleName;
var planId = paramsUrl.planId;
var testExecution = null;
var isUpdateDefectsTestDetail = false;
var JIRAticket_key = null;
var statusBug = [];
var statusSumaryBug = [];
var JiraTitle = null;
var JIRAType = null;
var arrObjTestCaseDetail = [];

$(function () {
    "use strict";
    var tcDetailTable = null;
    $('body').append('<div id="body-spiner">' + spinner + '<div>');
    var arrStatus = [];
    var flagUpdatePlanComment = false;
    var oldContentPlanComment = '';

    ajaxRequest(requestURL + '/issue/' + issueId, 'GET', null)
        .done(function (response) {
            console.log("response response response", response);
            JIRAticket_key = response.JIRAticket_key;
            JiraTitle = response.JIRAticket_title;
            JIRAType = response.type;
            $("#issueKey").html('<a target="_blank" href="' + projectBaseUrl + JIRAticket_key + '">' + JIRAticket_key + '</a>');
            tcDetailTable = $('#testcasedetail-table').DataTable({
                language: {
                    "emptyTable": 'No Testcase data found, <a target="_blank" href="' + projectBaseUrl + JIRAticket_key + '">click here to add<a/>'
                },
                data: null,
                autoWidth: false,
                ordering: true,
                // order: [
                //     [0, 'asc']
                // ],
                // "sDom": "Rlfrtip",
                columns: [
                    {
                        
                        data: 'order_id',
                        visible: false
                    },
                    {
                        data: 'test_step',
                        width: '25%',
                        render: function (data, type, row, meta) {
                            
                            return '<pre style="white-space:pre-line">' + data + '</pre>';
                        }
                    },
                    {
                        width: '25%',
                        data: 'test_data',
                        render: function (data, type, row, meta) {
                            return '<pre style="white-space:pre-line">' + data + '</pre>';
                        }
                    },
                    {
                        data: 'expected_result',
                        width: '25%',
                        render: function (data, type, row, meta) {
                            return '<pre style="white-space:pre-line">' + data + '</pre>';
                        }
                    },
                    {

                        data: 'status',
                        width: '10%',
                        className: 'td-status',
                        render: function (data, type, row, meta) {
                            var color = 'badge-secondary';
                            if (data == null) {
                                
                                return '<button class="btn badge-secondary badge badge-pill change-status btn-block">UNEXECUTED</button>';
                            } else {
                                switch (data.trim()) {
                                    case 'FAIL':
                                        color = 'badge-danger'; // FAIL
                                        break;
                                    case 'BLOCKED':
                                        color = 'badge-warning'; // BLOCK
                                        break;
                                    case 'WIP':
                                        color = 'badge-info'; // WIP
                                        break;
                                    case 'PASS':
                                        color = 'badge-success'; // PASS
                                        break;
                                    default:
                                        break;
                                }
                                return '<button class="btn badge badge-pill ' + color + ' change-status btn-block">' + data + '</button>';
                            }

                        }
                    },
                    {
                        data: 'attachment',
                        visible: false
                    },
                    {
                        data: 'defect',
                        className: 'defectsColumn',
                        orderable: false,
                        "searchable": false,
                        "render": function (data, type, row, meta) {
                            var tmpl = '<input type="text" placeholder="Issue search..." class="issueSearch form-control">';
                            if (data !== null) {
                                // var _issueKey = data != null ? data.split('|') : '';
                                $.each(data, function () {
                                    if (this != '') {
                                        tmpl += '<span class="defects mr-1 mb-1">' +
                                            '<a class="ticket ' + this.statusColor + '" data-toggle="tooltip"  title="' + this.sumaryAndName + '" target="_blank" href=' + projectBaseUrl + this.value + '>' + this.value + '</a>' +
                                            '<a class="update-defect ' + this.statusColor + ' remove-ticket" data-issue="' + this.value + '" href="javascript:;">' +
                                            'x</a></span>';
                                    }
                                });
                                return '<div class="inner-td">' + tmpl + '</div>';
                            }
                            return '';
                        }
                    }

                ],
                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).find(".displayComment").on('click', function () {
                        $(this).hide();
                        $(nRow).find('.container-update-comment').show();
                        var textarea = $(nRow).find('textarea');
                        updateHighOfTxtComment(textarea);
                    });
                    $(nRow).find('.issueSearch').autoComplete({
                        minChars: 3,
                        cache: false,
                        source: function (term, suggest) {
                            if ($(nRow).find('.add-defects .spinner').length == 0) {
                                $(nRow).find('.add-defects').append(spinner);
                            }
                            AP.request({
                                url: '/rest/api/2/issue/picker',
                                type: 'GET',
                                data: {
                                    query: term,
                                    currentProjectId: jiraProjectId,
                                    showSubTasks: true,
                                    showSubTaskParent: true
                                },
                                contentType: 'application/json',
                                success: function (res) {
                                    $(nRow).find('.add-defects .spinner').remove();
                                    var suggestions = [];
                                    
                                    var _arrOldDefect = [];
                                    $.each(aData.defect,function(){
                                        var _issue = this.value;
                                        _arrOldDefect.push(_issue);
                                    });
                                    var datasource = JSON.parse(res).sections[0].issues;
                                    for (var i = 0; i < datasource.length; i++) {
                                        var key = datasource[i].key.toUpperCase();
                                        if ((~datasource[i].key.toLowerCase().indexOf(term) || ~datasource[i].summaryText.toLowerCase().indexOf(term)) && !_arrOldDefect.includes(key)) {
                                            suggestions.push(datasource[i]);
                                        }
                                    }
                                    if (suggestions.length == 0) {
                                        suggest([{
                                            id: -1,
                                            message: "No issues found. Create new?"
                                        }]);
                                    } else
                                        suggest(suggestions);
                                },
                                error: function (xhr, statusText, errorThrown) {
                                    suggest([{
                                        id: -1,
                                        message: "No issues found. Create new?"
                                    }]);
                                }
                            });
                        },
                        renderItem: function (item, search) {
                            search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                            var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
                            if (item.id == -1) {
                                return '<div title="' + item.message + '" class="autocomplete-suggestion" data-values="-1">' + item.message + '</div>';
                            }
                            return '<div class="autocomplete-suggestion" data-values="' + item.key + '">' +
                                '<span>' + item.key.replace(re, "<b>$1</b>") + '</span> - ' +
                                unescape(escape(item.summaryText)).replace(re, "<b>$1</b>") + '</div>';
                        },
                        onSelect: function (e, term, item) {
                            if (item.data("values") == -1) {
                                createIssueInTestDetail(nRow);
                                return;
                            }
                            if ($(nRow).find('.add-defects .spinner').length == 0) {
                                $(nRow).find('.add-defects').append(spinner);
                            }
                            var row = tcDetailTable.row(nRow);
                            
                            var currentDefects = row.data().defect;
                            AP.request(baseUrl + '/rest/api/2/search?jql=issue=\'' + item.data("values") + '\'&fields=summary,status', {

                                success: function (res) {

                                    var newDefect = {
                                        value: item.data("values"),
                                        statusColor: '',
                                        sumaryAndName: '',
                                    };
                                    var datasource = JSON.parse(res);
                                    var issue = datasource && datasource["issues"].length && datasource["issues"]["0"];
                                    var status = issue && issue.fields ? issue.fields.status : undefined;

                                    newDefect.sumaryAndName = 'Summary : ' + issue.fields.summary.toString() + "&#013;" + 'Status : ' + status.name.toString();
                                    if (status.name == "To Do") {
                                        newDefect.statusColor = "statusToDoColor";
                                    } else if (status.name == "Done") {
                                        newDefect.statusColor = "statusDoneColor";
                                    } else {
                                        newDefect.statusColor = "statusInProgressColor";
                                    }

                                    currentDefects.push(newDefect);
                                    // var issueUpdate = lodash.join(currentDefects, '|');
                                    // row.data().defect = issueUpdate;
                                    row.data().defect = currentDefects;
                                  
                                    
                                    //update row
                                    tcDetailTable.row(nRow).data(row.data());
                                    tcDetailTable.draw(false);
                                    var _issue = [];
                                    $.each(currentDefects,function(){
                                        var issue = this.value;
                                        _issue.push(issue);
                                    });
                                    var issueUpdate = lodash.join(_issue, '|');
                                    updateDefectsInTestDetail(issueUpdate, nRow, row.data());
                                    $(nRow).find('.issueSearch').val('').trigger('blur.autoComplete');
                                },
                                error: function () {

                                }
                            });

                        }
                    });
                    return nRow;
                },
            });
            initData();
        });

    var tcExecHistoryTable = $('#exec-history-table').DataTable({
        data: null,
        order: [
            [3, 'desc']
        ],
        // rowGroup: {
        //     dataSrc: 'updated_at'
        // },
        columns: [{
                data: 'field_name'
            },
            {
                // data: 'original_value',
                render: function (data, type, row, meta) {
                    var original_value = row.original_value != null ? row.original_value : '';
                    if (row.field_name === 'status_id' && row.original_value != null) {
                        original_value = lodash.filter(arrStatus, function (o) {
                            return o.id == row.original_value;
                        })[0].name;
                    }
                    if (row.field_name === 'defects') {
                        original_value = original_value.replace('|', '');
                    }
                    return original_value;
                }
            },
            {
                // data: 'new_value',
                render: function (data, type, row, meta) {
                    var new_value = row.new_value != null ? row.new_value : '';
                    if (row.field_name === 'status_id' && row.new_value != null) {
                        var obj = lodash.filter(arrStatus, function (o) {
                            return o.id == row.new_value;
                        });
                        new_value = obj[0].name;
                    }
                    if (row.field_name === 'defects') {
                        new_value = new_value.replace('|', '');
                    }
                    return new_value;
                }
            },
            {
                // data: 'updated_at'
                "searchable": false,
                "bSort": false,
                orderable: false,
                data: function (row, type, val, meta) {
                    return row.updated_at ? moment(row.updated_at).format('DD/MM/YYYY hh:mm:ss') : '';
                }
            }
        ]
    });

    // create issue in test case detail
    function createIssueInTestDetail(tr) {
        
        var data = tcDetailTable.row(tr).data();
        var _currentIssues = data.defect;
        var currentIssues = [];
        $.each(_currentIssues,function(){
            var _value = this.value;
            currentIssues.push(_value);
        });
        AP.jira.openCreateIssueDialog(function (issues) {
            //user click cancel
            if (issues.length == 0) {
                $(tr).find('.spinner').remove();
            } else {
                        
                currentIssues.push(issues[0].key);
              
                var strIssues = lodash.join(currentIssues, '|');
                updateTestCaseDetail(data.id, {
                    defects: strIssues
                }).done(function (res) {
                            // if (data.defect != '') {
                            //     var arrDefects = strIssues != '' ? strIssues.split('|'): [];
                            //     $.each(arrDefects, function (index, value) {
                            //         if (value !== '') {
                            //             data.defect.push(value);
                            //         }
                            //     });
                            // }
                            data.defect = strIssues != '' ? strIssues.split('|'): [];
                    var deff = [];
                    var arrDefect = [];
                    $.each(data.defect, function (i, value) {
                        
                        var _deff = $.Deferred();
                        deff.push(_deff.promise());
                        AP.request(baseUrl + '/rest/api/2/search?jql=issue=\'' + value + '\'&fields=summary,status', {
                            success: function (res) {
                                var detailDefect = {
                                    statusColor: "",
                                    sumaryAndName: '',
                                    value: value,
                                };
                                var datasource = JSON.parse(res);
                                var issue = datasource && datasource["issues"].length && datasource["issues"]["0"];
                                var status = issue && issue.fields ? issue.fields.status : undefined;
                                var summaryTextBug = issue.fields.summary;
                                var statusColor;
                                var sumaryAndName = 'Summary : ' + summaryTextBug.toString() + "&#013;" + 'Status : ' + status.name.toString();

                                if (status.name == "To Do") {
                                    statusColor = "statusToDoColor";
                                } else if (status.name == "Done") {
                                    statusColor = "statusDoneColor";
                                } else {
                                    statusColor = "statusInProgressColor";
                                }

                                detailDefect.statusColor = statusColor;
                                detailDefect.sumaryAndName = sumaryAndName;
                                arrDefect.push(detailDefect);
                                _deff.resolve();
                            },
                            error: function () {
                                _deff.resolve();
                            }
                        });
                        $.when.apply($,deff).done(function(){
                            data.defect = arrDefect;
                        });
                    });
                    //update row
                    $.when.apply($,deff).done(function(){
                        tcDetailTable.row(tr).data(data);
                        tcDetailTable.draw();
                        $(tr).find('.spinner').remove();
                        var btn = $(tr).find('.add-defects .btn.dropdown-toggle');
                        btn.removeClass('active');
                        getTestCaseExcutionHistory(res, tcExecHistoryTable);
                    });
                  
                });
            }
        }, {
            pid: jiraProjectId
        });

    }

    //delete issue in test case execution
    $('#testcasedetail-table tbody').on('click', 'a.remove-ticket', function () {
        var tr = $(this).parents('tr');
        $(this).closest('span.defects').remove();
        var data = tcDetailTable.row(tr).data();
        // var currentIssues = data.defect != null ? data.defect.split('|') : [];
        var currentIssues = data.defect ;
        var _currentIssues = [];
        var arrDefect = [];
        $.each(currentIssues ,function(){
            var _value = this.value;
            _currentIssues.push(_value);
        });
        var issueRemove = $(this).data("issue").toString();
        lodash.remove(_currentIssues, function (o) {
            return o == issueRemove;
        });
        var deff =[];
        if(_currentIssues === undefined || _currentIssues.length ==0){
                data.defect = {
                    statusColor: "",
                        sumaryAndName: '',
                        value: '',
                };
                tcDetailTable.row(tr).data(data);
                tcDetailTable.draw(false);
                updateDefectsInTestDetail(lodash.join(_currentIssues, '|'), tr, data); 
        }else{
            $.each(_currentIssues, function (i, value) {         
                var _deff = $.Deferred();
                deff.push(_deff.promise());
                    AP.request(baseUrl + '/rest/api/2/search?jql=issue=\'' + value + '\'&fields=summary,status', {
                        success: function (res) {
                            var detailDefect = {
                                statusColor: "",
                                sumaryAndName: '',
                                value: value,
                            };
                            var datasource = JSON.parse(res);
                            var issue = datasource && datasource["issues"].length && datasource["issues"]["0"];
                            var status = issue && issue.fields ? issue.fields.status : undefined;
                            var summaryTextBug = issue.fields.summary;
                            var statusColor;
                            var sumaryAndName = 'Summary : ' + summaryTextBug.toString() + "&#013;" + 'Status : ' + status.name.toString();
        
                            if (status.name == "To Do") {
                                statusColor = "statusToDoColor";
                            } else if (status.name == "Done") {
                                statusColor = "statusDoneColor";
                            } else {
                                statusColor = "statusInProgressColor";
                            }
        
                            detailDefect.statusColor = statusColor;
                            detailDefect.sumaryAndName = sumaryAndName;
                            arrDefect.push(detailDefect);
                            _deff.resolve();
                        },
                        error: function () {
                            _deff.resolve();
                        }
                    });
                    $.when.apply($,deff).done(function(){
                        data.defect = arrDefect;
                    });
               
                
            });
    
            $.when.apply($,deff).done(function(){
                tcDetailTable.row(tr).data(data);
                tcDetailTable.draw(false);
                updateDefectsInTestDetail(lodash.join(_currentIssues, '|'), tr, data);
            });
        }
       
       
    });

    // cancel update status
    $('#testcasedetail-table tbody').on('click', 'button.cancel-update-status', function () {
        var tr = $(this).parents('tr');
        var td = $(this).parents('tr');
        td.find('.ct-status').remove();
        td.find('.change-status').show();
    });
    // change status in test case detail
    $('#testcasedetail-table tbody').on('click', 'button.change-status', function () {
        var that = this;
        var td = $(that).parents('td');
        var tr = $(that).parents('tr');
        var data = tcDetailTable.row(tr).data();
        var drpStatus = $('<select class="drpStatus form-control form-control-sm"></select>');
        var cancelButton = $('<button class="btn btn-link cancel-update-status">&times;</button>');
        
        console.log("arrStatus",arrStatus);
        $.each(arrStatus, function () {
            var obj = this;
            var option = $('<option value="' + obj.id + '">' + obj.name + '</option>');
            drpStatus.append(option);
        });
        var containerButton = $('<div class="ct-status"></div>').append(drpStatus).append(cancelButton);
        $(that).hide();
        td.append(containerButton);
        if(data.status_id == null){
            data.status_id = 5; //5 is  UNEXECUTED value
            
        }
        td.find('.drpStatus').val(data.status_id);
        // if(data.status_id == null){
        //     td.find('.drpStatus').val(5); //5 is  UNEXECUTED value
            
        // }else{
        //     td.find('.drpStatus').val(data.status_id);
        // }
        drpStatus.on('change', function () {
            td.append(spinner);
            td.find('.cancel-update-status').css('visibility', 'hidden');
            updateTestCaseDetail(data.id, {
                status_id: drpStatus.val(),
                executed_by: currentUser.displayName
            }).done(function (res) {
                console.log("res", res);
                containerButton.remove();
                var objTestCaseDetail = {
                    id: res.id,
                    manual_testcase_id: res.manual_testcase_id,
                    test_step: data.test_step,
                    test_data: data.test_data,
                    expected_result: data.expected_result,
                    status: res.status,
                    status_id: res.status_id,
                    comment: res.comment,
                    attachment: res.attachment,
                    defect: res.defects === null ? [] : res.defects,
                    order_id: res.order_id
                };
                    var _defects = [];
        
                    if (res.defects != '') {
                      
                        var arrDefects = res.defects != null ? res.defects.split('|') : [];
                        $.each(arrDefects, function (index, value) {
                            if (value !== '') {
                                _defects.push(value);
                            }
                        });
                    }else{
                      
                    }
                    var arrDefect = [];
                    var mainDeferGetDefectDetail = [];
                    $.each(_defects, function (i, value) {
                        var def = $.Deferred();
                        mainDeferGetDefectDetail.push(def.promise());
                        AP.request(baseUrl + '/rest/api/2/search?jql=issue=\'' + value + '\'&fields=summary,status', {
                            success: function (res) {
                                var detailDefect = {
                                    statusColor: "",
                                    sumaryAndName: '',
                                    value: '',
                                };
                                var datasource = JSON.parse(res);
                                var issue = datasource && datasource["issues"].length && datasource["issues"]["0"];
                                var status = issue && issue.fields ? issue.fields.status : undefined;
                                var summaryTextBug = issue.fields.summary;
                                var statusColor;
                                var sumaryAndName = 'Summary : ' + summaryTextBug.toString() + "&#013;" + 'Status : ' + status.name.toString();

                                var colorStatusBug = status.name;
                                if (colorStatusBug == "To Do") {
                                    statusColor = "statusToDoColor";
                                } else if (colorStatusBug == "Done") {
                                    statusColor = "statusDoneColor";
                                } else {
                                    statusColor = "statusInProgressColor";
                                }

                                detailDefect.statusColor = statusColor;
                                detailDefect.sumaryAndName = sumaryAndName;
                                detailDefect.value = value;
                                arrDefect.push(detailDefect);
                                def.resolve();
                            },
                            error: function () {
                                def.resolve();
                            }
                        });
                        $.when.apply($, mainDeferGetDefectDetail).done(function () {
                            objTestCaseDetail.defect = arrDefect;
                        });
                    });
                   
               $.when.apply($,mainDeferGetDefectDetail).done(function(){
                   console.log("objTestCaseDetail",objTestCaseDetail);
                tcDetailTable.row(tr).data(objTestCaseDetail).draw(false);
                getTestCaseExcutionHistory(res, tcExecHistoryTable);
               });
               
            });
        });
    });

    $("#projectKey").html(projectKey).attr("href", projectBaseUrl + projectKey);
    $("#cycleName").html(cycleName);
    $("#issueName").html(issueId);

    $('#drpStatus').on('change', function () {
        $(this).parents('.form-group').find('label').append(spinner);
        var that = this;
        $(that).attr('disabled', true);
        updatePlanTestCase(cycleId, issueId, {
            status_id: $(this).val()
        }).done(function () {
            $(that).parents('.form-group').find('.spinner').remove();
            $(that).removeAttr('disabled');
        });
    });

    $('#dropdownAssignee').on('change', function () {
        updateAssigneeOfPlan(cycleId, issueId, $('#dropdownAssignee').val());
    });

    $('#plan-comment').on('keypress', function () {
        flagUpdatePlanComment = true;
    });

    $('#plan-comment').on('blur', function () {
        if (flagUpdatePlanComment) {
            flagUpdatePlanComment = false;
        } else {
            return;
        }
        if ($.trim($(this).val()) !== $.trim(oldContentPlanComment)) {
            $(this).parents('.form-group').find('label').append(spinner);
            var that = this;
            $(that).attr('disabled', true);
            updatePlanTestCase(cycleId, issueId, {
                comment: $(this).val()
            }).done(function (res) {
                $(this).parents('.form-group').find('.spinner').remove();
                oldContentPlanComment = res.comment;
                flagUpdatePlanComment = false;
                $(that).parents('.form-group').find('.spinner').remove();
                $(that).removeAttr('disabled');
            });
        }
    });

    function initData() {
        var params = {
            cycle_id: cycleId,
            issue_id: issueId
        };
        var allDefer = [];
        var deferTestExcution = $.Deferred();
        var deferTestCaseEXHistory = $.Deferred();

        allDefer.push(deferTestExcution.promise());
        allDefer.push(deferTestCaseEXHistory.promise());

        var deferredInit = [];
        var defferGetStatus = $.Deferred();
        deferredInit.push(defferGetStatus.promise());

        var defferGetUserJira = $.Deferred();
        deferredInit.push(defferGetUserJira.promise());
        ajaxRequest(requestAPI.updateLatestTestcase + planId + '/update_latest_testcase?issue_id=' + issueId, 'POST', null)
            .done(function () {
                loadDataForTable();
            })
            .catch(function () {
                console.info('Can not load test case detail');

            });
        // get user from JIRA
        IFDRequest.customRequest('/rest/api/2/user/assignable/search?project=' + projectKey, 'GET', 'application/json', null).then(function (res) {
            userJira = lodash.filter(JSON.parse(res.body), function (o) {
                return o.emailAddress.indexOf("@connect.atlassian.com") === -1;
            });
            $.each(userJira, function () {
                var that = this;
                var aTag = '<option value="' + that.emailAddress + '">' + that.displayName + '</option>';
                $('#dropdownAssignee').append(aTag);
            });
            defferGetUserJira.resolve();
            setTimeout(function () {
                $('#dropdownAssignee').selectpicker('refresh');
            }, 500);
        });

        getStatus().done(function (response) {
            if (response) {
                arrStatus = response;
                $.each(response, function () {
                    var obj = this;
                    var option = $('<option value="' + obj.id + '">' + obj.name + '</option>');
                    $('#drpStatus').append(option);
                });
            }
            defferGetStatus.resolve();
            $('#drpStatus').selectpicker();
        });

        var defferGetTestExcution = $.Deferred();
        deferredInit.push(defferGetTestExcution.promise());
        ajaxRequest(requestAPI.getTestExecution, "GET", params).done(function (res) {

            testExecution = res[0];
            defferGetTestExcution.resolve();
        }, function (err) {
            // console.log('Can not get Execution');
            defferGetTestExcution.resolve();
        });

        $.when.apply($, deferredInit).done(function () {
        
            $('#drpStatus').selectpicker('val', testExecution.status_id);
            var defects = [];
            if (testExecution.defects != '') {
                var arrDefects = testExecution.defects != null ? testExecution.defects.split('|') : [];
                $.each(arrDefects, function (index, value) {
                    if (value !== '') {
                        defects.push(value);
                    }
                });
            }
            // var defects = testExecution.defects != null ? testExecution.defects.split('|') : '';
            var tmpl = '';
            var colorStatusBug;
            var arrDef = []; //Arry[waiting1, waiting2,wating3] ==> Array[done1,done2,done3]==> execute next code
            $.each(defects, function () {
                var _defDefect = $.Deferred();
                _defDefect.promise();
                arrDef.push(_defDefect);
                var _defect = this;
                var sumaryAndName = "";
                if (_defect !== '' && typeof _defect != 'undefined') {
                    // console.log('_defect', _defect);
                    AP.request(baseUrl + '/rest/api/2/search?jql=issue=\'' + _defect + '\'&fields=summary,status', {
                        success: function (res) {
                            var datasource = JSON.parse(res);
                            var issue = datasource && datasource["issues"].length && datasource["issues"]["0"];
                            var status = issue && issue.fields ? issue.fields.status : undefined;
                            var summaryTextBug = issue.fields.summary;
                            var statusColor;
                            sumaryAndName = 'Summary : ' + summaryTextBug.toString() + "&#013;" + 'Status : ' + status.name.toString();
                          
                            colorStatusBug = status.name;
                            if (colorStatusBug == "To Do") {
                                statusColor = "statusToDoColor";
                            } else if (colorStatusBug == "Done") {
                                statusColor = "statusDoneColor";
                            } else {
                                statusColor = "statusInProgressColor";
                            }
                            tmpl += '<li class="list-inline-item"><span  class="defects" data-toggle="tooltip"  title="' + sumaryAndName + '"><a class="ticket ' + statusColor + '" target="_blank" href=' + projectBaseUrl + _defect +
                                '>' + _defect + '</a><a class="update-defect remove-ticket ' + statusColor + '" data-issue="' + _defect +
                                '" onclick="removeDefectInMainExecuton(this)" href="javascript:;">x</a></span></li>';
                            _defDefect.resolve();
                        },
                        error: function () {
                            _defDefect.reject();
                        }
                    });

                }

            });

            // arrDef[done1,done2,done3]==> execute next code
            $.when.apply($, arrDef).done(function () {
                ulMainDefect.prepend(tmpl);
            });

            onKeyUpOfSearchMainDefect();
            var user = lodash.filter(userJira, function (o) {
                return o.emailAddress === testExecution.assigned_to;
            });
            $('#dropdownAssignee').selectpicker('val', user.length > 0 ? user[0].emailAddress : '');
            $('#plan-comment').val(testExecution.comment);
            oldContentPlanComment = testExecution.comment;

            //finish get test execution
            deferTestExcution.resolve();
        });

        $('#refresh-data-test-details').click(function () {
            $(this).find('span').addClass('fa-spin');
            $('#exec-history-table tbody').html('');
            tcExecHistoryTable.clear('');
            tcExecHistoryTable.draw('');
            ajaxRequest(requestAPI.updateLatestTestcase + planId + '/update_latest_testcase?issue_id=' + issueId, 'POST', null)
                .done(function () {
                    loadDataForTable();
                })
                .catch(function () {
                    console.info('Can not load test case detail');
                });

        });

        function loadDataForTable() {
            ajaxRequest(requestAPI.manualTestCaseExecution, "GET", {
                    cycle_issue_execution_id: planId
                })
                .done(function (res) {

                    $('#testcasedetail-table tbody').html('');
                    tcDetailTable.clear();
                    // tcDetailTable.draw();
                    tcDetailTable.order([ 0, 'asc' ]).draw();
                    arrObjTestCaseDetail = [];
                    var d = [];
                    $.each(res, function () {
                        var _d = $.Deferred();
                        d.push(_d.promise());
                        var tce = this;
                        var objTestCaseDetail = {
                            test_step: tce.manual_testcase.test_step,
                            test_data: tce.manual_testcase.test_data,
                            order_id: tce.order_id,
                            expected_result: tce.manual_testcase.expected_result,
                            id: tce.id,
                            // manual_testcase_id: tce.manual_testcase_id,
                            status: tce.status,
                            status_id: tce.status_id,
                            comment: tce.comment,
                            attachment: tce.attachment,
                            defect: tce.defects,
                        };
                        arrObjTestCaseDetail.push(objTestCaseDetail);
                        _d.resolve();
                    });
                    var mainGetIssuesDefferAuto = [];
                    $.when.apply($, d).done(function () {
                        $.each(res, function (i, val) {
                            
                            var _defects = [];
                            if (testExecution.defects != '') {
                                var arrDefects = val.defects != null ? val.defects.split('|') : [];
                                $.each(arrDefects, function (index, value) {
                                    if (value !== '') {
                                        _defects.push(value);
                                    }
                                });
                            }

                            var mainDeferGetDefectDetail = [];
                            var arrDefect = [];
                            $.each(_defects, function (i, value) {
                                var def = $.Deferred();
                                mainGetIssuesDefferAuto.push(def.promise());
                                mainDeferGetDefectDetail.push(def.promise());
                                AP.request(baseUrl + '/rest/api/2/search?jql=issue=\'' + value + '\'&fields=summary,status', {
                                    success: function (res) {
                                        var detailDefect = {
                                            statusColor: "",
                                            sumaryAndName: '',
                                            value: '',
                                        };
                                        var datasource = JSON.parse(res);
                                        var issue = datasource && datasource["issues"].length && datasource["issues"]["0"];
                                        var status = issue && issue.fields ? issue.fields.status : undefined;
                                        var summaryTextBug = issue.fields.summary;
                                        var statusColor;
                                        var sumaryAndName = 'Summary : ' + summaryTextBug.toString() + "&#013;" + 'Status : ' + status.name.toString();

                                        var colorStatusBug = status.name;
                                        if (colorStatusBug == "To Do") {
                                            statusColor = "statusToDoColor";
                                        } else if (colorStatusBug == "Done") {
                                            statusColor = "statusDoneColor";
                                        } else {
                                            statusColor = "statusInProgressColor";
                                        }

                                        detailDefect.statusColor = statusColor;
                                        detailDefect.sumaryAndName = sumaryAndName;
                                        detailDefect.value = value;
                                        arrDefect.push(detailDefect);
                                        def.resolve();
                                    },
                                    error: function () {
                                        def.resolve();
                                    }
                                });
                            });


                            $.when.apply($, mainDeferGetDefectDetail).done(function () {

                                arrObjTestCaseDetail[i].defect = arrDefect;
                            });
                        });
                    });
                    $.when.apply($, mainGetIssuesDefferAuto).done(function () {
                        setTimeout(function () {
                            console.log("arrObjTestCaseDetail arrObjTestCaseDetail", arrObjTestCaseDetail);
                            tcDetailTable.rows.add(arrObjTestCaseDetail);
                            tcDetailTable.draw(true);
                            $('#refresh-data-test-details').find('span').removeClass('fa-spin');
                        }, 500);
                    });
                    // test case execution histories
                    $.each(res, function () {
                        ajaxRequest(requestAPI.manualTestCaseExecution + this.id + "/execution_history", "GET", null).done(function (res) {
                            var d = [];
                            $.each(res, function () {
                                var _d = $.Deferred();
                                d.push(_d.promise());
                                var tce = this;
                                var objTestExecHistory = {
                                    field_name: tce.field_name,
                                    original_value: tce.old_value,
                                    new_value: tce.new_value,
                                    updated_at: tce.updated_at
                                };

                                tcExecHistoryTable.row.add(objTestExecHistory);
                                _d.resolve();
                                $('#refresh-data-execution-history').find('span').removeClass('fa-spin');
                            });

                            $.when.apply($, d).done(function () {
                                tcExecHistoryTable.draw(true);
                                deferTestCaseEXHistory.resolve();
                            });
                        });
                    });
                })
                .catch(function () {
                    $('#refresh-data-test-details').find('span').removeClass('fa-spin');
                    deferTestCaseEXHistory.resolve();
                });
            // API get name Defects
            var err404 = $('<td valign="top" colspan="5" class="dataTables_empty">No Testcase data found, <a target="_blank" href="' + projectBaseUrl + JIRAticket_key + '">click here to add<a/></td>');
            ajaxRequest(requestAPI.manualTestCase, "GET", {
                issue_id: issueId
            }).done(function (res) {
                $('#testcasedetail-table .td').remove();
            }).catch(function () {        
                $('#testcasedetail-table tbody tr td').remove();
                $('#testcasedetail-table tbody').append(err404);          
                $('#refresh-data-test-details').find('span').removeClass('fa-spin');
                deferTestCaseEXHistory.resolve();
            });
        }

        $.when.apply($, allDefer).done(function () {
            $('#body-spiner').remove();
        });
    }

    $('#searchMainDefect').autoComplete({
        minChars: 1,
        cache: false,
        source: function (term, suggest) {
            AP.request({
                url: '/rest/api/2/issue/picker',
                type: 'GET',
                data: {
                    query: term,
                    currentProjectId: jiraProjectId,
                    showSubTasks: true,
                    showSubTaskParent: true
                },
                contentType: 'application/json',
                success: function (res) {
                    $('#add_defect .spinner').remove();
                    term = term.toLowerCase();
                    var suggestions = [];
                    var arrOldDefect = testExecution.defects != null ? testExecution.defects.split("|") : [];
                    var datasource = JSON.parse(res).sections[0].issues;
                    for (var i = 0; i < datasource.length; i++) {
                        var key = datasource[i].key.toUpperCase();
                        if ((~datasource[i].key.toLowerCase().indexOf(term) || ~datasource[i].summaryText.toLowerCase().indexOf(term)) && !arrOldDefect.includes(key)) {
                            suggestions.push(datasource[i]);
                        }
                    }
                    if (suggestions.length == 0) {
                        suggest([{
                            id: -1,
                            message: "No issues found. Create new?"
                        }]);
                    } else
                        suggest(suggestions);
                },
                error: function (xhr, statusText, errorThrown) {
                    suggest([{
                        id: -1,
                        message: "No issues found. Create new?"
                    }]);
                }
            });
        },
        renderItem: function (item, search) {
            search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
            if (item.id == -1) {
                return '<div title="' + item.message + '" class="autocomplete-suggestion" data-values="-1">' + item.message + '</div>';
            }
            return '<div class="autocomplete-suggestion" data-values="' + item.key + '">' +
                '<span>' + item.key.replace(re, "<b>$1</b>") + '</span> - ' +
                unescape(escape(item.summaryText)).replace(re, "<b>$1</b>") + '</div>';
        },
        onSelect: function (e, term, item) {
            if ($('#add_defect .spinner').length == 0) {
                $('#add_defect').find('label').append(spinner);
            }
            if (item.data("values") == -1) {
                openPanelCreateIssueDialog();
            } else {
                
                var tmpMainDefect = testExecution.defects != null ? testExecution.defects.split('|') : [];
                tmpMainDefect.push(item.data("values"));
                testExecution.defects = lodash.join(tmpMainDefect, '|');
                inputSearchMainDefect.val('');
                inputSearchMainDefect.focus();
                updateDefectMainTestCase(cycleId, issueId, tmpMainDefect).done(function () {
                    updateHTMLMainDefects(tmpMainDefect);
                });
            }
        }
    });

    function updateDefectsInTestDetail(issueUpdate, row, rowData) {
        updateTestCaseDetail(rowData.id, {
            defects: issueUpdate
        }).done(function (res) {
            $(row).find('.spinner').remove();
            getTestCaseExcutionHistory(res, tcExecHistoryTable);
        });
    }

    $('#_searchMainDefect').on('blur', function () {
        $('#searchMainDefect').trigger('blur.autocomplete');
    });

    $('#addNewIssue').click(function () {
        openPanelCreateIssueDialog();
    });

    $('.w_add_defect_main').on('click', function () {
        $('#_searchMainDefect').focus();
    });
    // resizeColumnTable();

});

function openPanelCreateIssueDialog() {

    $('#add_defect .spinner').remove();
    AP.jira.openCreateIssueDialog(function callback(issues) {
        console.info('issues', issues);
        linkedToIssue(JIRAticket_key, issues[0], JiraTitle, JIRAType);
        if (issues.length > 0) {
            var tmpMainDefect = testExecution.defects != null ? testExecution.defects.split('|') : [];
            tmpMainDefect.push(issues[0].key);

            testExecution.defects = lodash.join(tmpMainDefect, '|');
            updateDefectMainTestCase(cycleId, issueId, tmpMainDefect).done(function () {
                updateHTMLMainDefects(tmpMainDefect);
                inputSearchMainDefect.val('').blur();
            });
            statusSumaryBug = issues[0].fields.summary + "&#013;" + issues[0].fields.status.name;
        }
    }, {
        pid: jiraProjectId
    });
    return;
}

function resizeColumnTable() {
    var pressed = false;
    var start;
    var startX, startWidth;

    $("table th").mousedown(function (e) {
        start = $(this);
        pressed = true;
        startX = e.pageX;
        startWidth = $(this).width();
        $(start).addClass("resizing");
    });

    $(document).mousemove(function (e) {
        if (pressed) {
            $(start).width(startWidth + (e.pageX - startX));
        }
    });

    $(document).mouseup(function () {
        if (pressed) {
            $(start).removeClass("resizing");
            pressed = false;
        }
    });
}

function getTestCaseExcutionHistory(res, tcExecHistoryTable) {
    ajaxRequest(requestAPI.testCaseExecution + res.id + "/execution_history", "GET", null).done(function (ress) {
        var lastObj = lodash.last(ress);
        var objTestExecHistory = {
            field_name: lastObj.field_name,
            original_value: lastObj.old_value,
            new_value: lastObj.new_value,
            updated_at: lastObj.updated_at
        };
        tcExecHistoryTable.row.add(objTestExecHistory);
        tcExecHistoryTable.draw(false);
    });
}
/**
 *
 * @param {*} cycleId
 * @param {*} issueId
 * @param {array} issueKey: array of Issues/Defects
 */
function updateDefectMainTestCase(cycleId, issueId, issueKey) {
    
    var strIssue = lodash.join(issueKey, '|');
    if ($('#add_defect .spinner').length == 0) {
        $('#add_defect').find('label').append(spinner);
    }
    return updatePlanTestCase(cycleId, issueId, {
        defects: strIssue
    });
}

// function updateHTMLMainDefects(_tmpMainDefect) {
//     var tmpl = '';
//     var bugName;
//     $.each(_tmpMainDefect, function () {
//         bugName = this;
//         if (this != '') {
//             tmpl += '<li class="list-inline-item"><span class="defects" data-toggle="tooltip"  ><a class="ticket" target="_blank" href=' + projectBaseUrl + this + '>' + this +
//                 '</a><a onclick="removeDefectInMainExecuton(this)" class="update-defect remove-ticket" data-issue="' + this +
//                 '" href="javascript:;">x</a></span></li>';
//         }
//     });
//     // ulMainDefect.find('>li').not('.search-issue').remove();
//     // ulMainDefect.prepend(tmpl);

//     ulMainDefect.not('>li').not('.search-issue').prepend('<li class="list-inline-item"><span class="defects" data-toggle="tooltip"  title="' + statusSumaryBug + '"><a class="ticket" target="_blank" href=' + projectBaseUrl + bugName + '>' + bugName +
//         '</a><a onclick="removeDefectInMainExecuton(this)" class="update-defect remove-ticket" data-issue="' + bugName +
//         '" href="javascript:;">x</a></span></li>');
//     $('#add_defect .spinner').remove();
//     // onKeyUpOfSearchMainDefect();
// }

function updateHTMLMainDefects(_tmpMainDefect) {
var arrTmpMainDefect =[];
    $.each(_tmpMainDefect,function(i,value){
        if(this != ''){
            arrTmpMainDefect.push(value);
        }
    });
    var tmpl = '';
    var arrDef = []; //Arry[waiting1, waiting2,wating3] ==> Array[done1,done2,done3]==> execute next code
    $.each(arrTmpMainDefect, function () {
        var _defDefect = $.Deferred();
        _defDefect.promise();
        arrDef.push(_defDefect);
        var _defect = this;
        if (_defect != '' && typeof _defect != 'undefined') {
            // console.log('_defect', _defect);
            AP.request(baseUrl + '/rest/api/2/search?jql=issue=\'' + _defect + '\'&fields=summary,status', {
                success: function (res) {
                    var datasource = JSON.parse(res);
                    var issue = datasource && datasource["issues"].length && datasource["issues"]["0"];
                    var status = issue && issue.fields ? issue.fields.status : undefined;
                    var summaryTextBug = issue.fields.summary;
                    var statusColor;
                    var sumaryAndName = 'Summary : ' + summaryTextBug.toString() + "&#013;" + 'Status : ' + status.name.toString();
                    console.log("sumaryAndName", sumaryAndName);
                    var colorStatusBug = status.name;
                    if (colorStatusBug == "To Do") {
                        statusColor = "statusToDoColor";
                    } else if (colorStatusBug == "Done") {
                        statusColor = "statusDoneColor";
                    } else {
                        statusColor = "statusInProgressColor";
                    }
                    tmpl += '<li class="list-inline-item"><span  class="defects" data-toggle="tooltip"  title="' + sumaryAndName + '"><a class="ticket ' + statusColor + '" target="_blank" href=' + projectBaseUrl + _defect +
                        '>' + _defect + '</a><a class="update-defect remove-ticket ' + statusColor + '" data-issue="' + _defect +
                        '" onclick="removeDefectInMainExecuton(this)" href="javascript:;">x</a></span></li>';
                    _defDefect.resolve();
                },
                error: function () {
                    _defDefect.reject();
                }
            });

        }

    });

    // arrDef[done1,done2,done3]==> execute next code
    $.when.apply($, arrDef).done(function () {
        ulMainDefect.find('>li').not('.search-issue').remove();
        ulMainDefect.not('>li').prepend(tmpl);
        $('#add_defect .spinner').remove();
    });
}


function updateTestCaseDetail(id, data) {
    

    var _data = Object.assign({
        modified_by: currentUser.emailAddress,
    }, data);
    return ajaxRequest(requestAPI.updateManualTestCaseExecution + id, "PUT", JSON.stringify(_data));
}

function updateHighOfTxtComment(that) {
    var content = $(that).val();
    var line = content.substr(0, content.length).split('\n').length;
    $(that).attr('rows', line);
}

function updateAssigneeOfPlan(cycleId, issueId, email) {
    // console.log("Assignee", email);
    $('#add_assign').find('label').append(spinner);
    var that = this;
    updatePlanTestCase(cycleId, issueId, {
        assigned_to: email
    }).done(function (res) {
        $('#add_assign').find('.spinner').remove();
        var user = lodash.filter(userJira, function (o) {
            return o.emailAddress == email;
        });
        $('#dropdownAssignee').selectpicker('val', user[0].emailAddress);
    });
}

var addMainDefect = $('#add_defect .w_add_defect_main');
var ulMainDefect = $('#add_defect .w_add_defect_main ul.list-inline');
var liSearchMainDefect = $('#add_defect .search-issue');
var inputSearchMainDefect = $('#_searchMainDefect');
var canvas = document.getElementById("tempCanvas");
var ctx = canvas.getContext("2d");
ctx.font = "14px Arial";

function onKeyUpOfSearchMainDefect() {
    $('#searchMainDefect').val(inputSearchMainDefect.val());
    if ($('#add_defect .spinner').length == 0) {
        $('#add_defect').find('label').append(spinner);
    }
    if (inputSearchMainDefect.val().length < 3) {
        $('#add_defect .spinner').remove();
    }
    $('#searchMainDefect').trigger('keyup.autocomplete');
    var widthText = ctx.measureText(inputSearchMainDefect.val()).width;
    var w = addMainDefect.width();
    if (widthText < w) {
        if (widthText < 88)
            liSearchMainDefect.css('width', 88);
        else liSearchMainDefect.css('width', widthText + 10);
    } else liSearchMainDefect.css('width', w);
}

function removeDefectInMainExecuton(that) {

    var currentDefects = testExecution.defects != null ? testExecution.defects.split('|') : [];
    // console.log("currentDefects", currentDefects);
    lodash.remove(currentDefects, function (o) {
        return o == $(that).data('issue');
    });
    testExecution.defects = lodash.join(currentDefects, '|');
    $(that).closest('.list-inline-item').remove();
    updateDefectMainTestCase(cycleId, issueId, currentDefects).done(function () {
        // updateHTMLMainDefects(currentDefects);
        $('#add_defect .spinner').remove();
    });
}