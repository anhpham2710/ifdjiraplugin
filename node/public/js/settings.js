/// <reference path="./common/common.js" />
/// <reference path="./common/constants.js" />
/*jshint esversion: 6 */
const urlProjectConfigs = requestURL + '/project_config';
$(function () {
    var demoData = {
        settings: [{
                name: 'default browser',
                value: 'Chrome'
            },
            {
                name: 'Wait time',
                value: '30'
            },
            {
                name: 'Test page address',
                value: 'infodationtest.vureflect.com'
            },
            {
                name: 'Selenium grid',
                value: ''
            },
            {
                name: 'Data source username',
                value: 'infodationvn'
            },
            {
                name: 'Data source password',
                value: 'infodation123'
            },
            {
                name: 'Data source URL',
                value: 'project@bitbucket.git'
            },
            {
                name: 'db type',
                value: 'MySQL'
            },
            {
                name: 'ws url',
                value: ''
            }
        ]
    };
    $.get(urlTemplateRender + 'child-row-settings-item.html', function (template) {
        var rendered = Mustache.render(template, demoData);
        $('#settingContent').html(rendered);
    });
});

function getProjectConfigs() {
    
}

function updateProjectConfig() {
   
}