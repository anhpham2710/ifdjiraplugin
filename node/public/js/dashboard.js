/// <reference path="./common/initIFDRequest.js" />
var arrIssueSelected = [];
var lodash = _;
var tmpCycleID = null;
var tmpCycle_name = null;
var nRowTestCycleSelected = null;
// var clientKey = getClientKeyFromJWT(jwt_token);
// console.log("jwt_token:", jwt_token);
var usersJira = null;

var tableTestCycle = null;
$(function () {

    // var tooltipOpt = {
    //     container: '#tabs-test-cycle'
    // };
    // $('[data-toggle="tooltip"]').tooltip(tooltipOpt);

    var arrJiraVersion = null;

    var project_id = null;
    var JIRAproject_id = null;
    var JIRAproject_key = null;
    var versions = [];
    getProjectDetail().done(function (res) {
        var _jiraProjectId = res[0].JIRAproject_id;
        // get user from JIRA
        AP.request('/rest/api/2/user/assignable/search?project=' + _jiraProjectId, {
            success: function (res) {
                usersJira = lodash.filter(JSON.parse(res), function (o) {
                    return o.emailAddress.indexOf("@connect.atlassian.com") === -1;
                });
            }
        });
        // get Jira version     
        IFDRequest.customRequest('/rest/api/2/project/' + _jiraProjectId + '/versions', "GET", "application/json", null)
            .then(function (response) {
                var optgroupUnRelease = $('<optgroup label="Unreleased version"></optgroup>');
                optgroupUnRelease.append('<option value="UNSCHEDULE">Unschedule</option>');
                arrJiraVersion = JSON.parse(response.body);
                if (arrJiraVersion.length > 0) {
                    // released
                    var arrRelease = lodash.filter(arrJiraVersion, function (obj) {
                        return obj.released == true;
                    });
                    if (arrRelease.length > 0) {
                        var optgroupRelease = $('<optgroup label="Released version"></optgroup>');
                        $.each(arrRelease, function () {
                            var option = '<option value="' + this.id + '">' + this.name + '</option>';
                            optgroupRelease.append(option);
                        });
                        $('#project_version').append(optgroupRelease);
                    }
                    // unreleased
                    var arrUnRelease = lodash.filter(arrJiraVersion, function (obj) {
                        return obj.released == false;
                    });

                    if (arrUnRelease.length > 0) {
                        $.each(arrUnRelease, function () {
                            var option = '<option value="' + this.id + '">' + this.name + '</option>';
                            optgroupUnRelease.append(option);
                        });
                    }
                }

                $('#project_version').append(optgroupUnRelease);
                setTimeout(function () {
                    $('#project_version').selectpicker('refresh');
                }, 500);
            })
            .catch(function (er) {
                // console.log('Could not load Jira version', er);
            });
    });

    tableTestCycle = $("#sprint-table").DataTable({
        data: null,
        columns: [{
                className: 'details-control',
                orderable: false,
                data: null,
                defaultContent: ''
            },
            {
                data: 'name'
            },
            {
                data: 'description'
            },
            {
                data: function (row, type, val, meta) {
                    return row.start_date ? moment(row.start_date).format('DD/MM/YYYY') : '';
                }
            },
            {
                data: function (row, type, val, meta) {
                    return row.end_date ? moment(row.end_date).format('DD/MM/YYYY') : '';
                }
            },
            {
                data: 'environment'
            },
            {
                data: function (row, type, val, meta) {
                    var versionName = lodash.find(arrJiraVersion, function (o) {
                        return o.id == row.JIRAversion;
                    });
                    return versionName ? versionName.name : row.JIRAversion;
                }
            },
            {
                data: 'build'
            },
            {
                // data: 'created_by',
                visible: false,
                data: function (row, type, val, meta) {

                    if (row !== null && row.created_by !== null) {
                        var _user = lodash.find(usersJira, function (o) {
                            return o.emailAddress == row.created_by;
                        });
                        if (typeof _user !== 'undefined') {
                            return _user.displayName;
                        }
                    }
                    return row.created_by;
                }
            },
            {
                // data: 'modified_by'
                visible: false,
                data: function (row, type, val, meta) {
                    if (row.modified_by !== null) {
                        var _user = lodash.find(usersJira, function (o) {
                            return o.emailAddress == row.modified_by;
                        });

                        if (typeof _user !== 'undefined') {
                            return _user.displayName;
                        }
                    }
                    return row.modified_by;
                }
            },
            {
                data: 'created_at',
                render: function (data, type, row, meta) {
                    return moment(data).format('DD/MM/YYYY');
                }
            },
            {
                data: 'issue_report',
                orderable: false,
                width: '150px',
                render: function (data, type, row, meta) {
                    // var rpManual = lodash.filter(data, function (d) {
                    //     return d.type === "manual";
                    // });
                    if (data.length > 0) {
                        var countPass = lodash.filter(data, function (d) {
                            return d.status === "PASS";
                        }).length;
                        var countFail = lodash.filter(data, function (d) {
                            return d.status === "FAIL";
                        }).length;
                        var perExcuted = (countPass + countFail) * 100 / data.length;
                        var div = '<div>Progress: ' + Math.round(perExcuted) + '%</div>';
                        // console.log('data ', data);
                        return div + generateCycleTestReport(data);
                    }
                    return '';
                }
            },
            {
                className: 'cycle-actions',
                orderable: false,
                data: null,
                defaultContent: '<div class="dropdown">' +
                    '<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                    '<span class="oi" data-glyph="cog"></span>' +
                    '</button>' +
                    '<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">' +
                    '<button class="dropdown-item btn-open-add-dialog" type="button">Add issue</button >' +
                    '<button class="dropdown-item btn-open-edit-dialog" type="button">Edit cycle</button >' +
                    '<button class="dropdown-item btn-open-delete-dialog" type="button">Delete cycle</button >' +
                    '<button class="dropdown-item btn-open-clone-dialog" type="button">Clone cycle</button >' +
                    '</div></div>',
                "width": "42px"
            }
        ],
        fnRowCallback: function () {
            $('[data-toggle="tooltip"]').tooltip();
        }
    });

    $('#sprint-table tbody').on('click', ".btn-open-edit-dialog", function () {
        var tr = $(this).closest('tr');
        nRowTestCycleSelected = tr;
    });

    $('#sprint-table tbody').on('click', '.btn-open-delete-dialog', function () {
        var tr = $(this).closest('tr');
        var row = tableTestCycle.row(tr);
        var aData = row.data();
        $('#lblCycleNameToDelete').html(aData.name);
        $('#delete-cycle-dialog').modal("show");
        tmpCycleID = aData.id;
        nRowTestCycleSelected = tr;
    });

    $('#sprint-table tbody').on('click', '.btn-open-add-dialog', function () {
        var tr = $(this).closest('tr');
        nRowTestCycleSelected = tr;
        var row = tableTestCycle.row(tr);
        var aData = row.data();
        $('#frmAdd-issue-dialog_cycle-name').html(aData.name);
        $('#add-issue-dialog').modal("show");
        tmpCycleID = aData.id;
    });

    $('#sprint-table tbody').on('click', ".btn-open-edit-dialog", function () {
        var tr = $(this).closest('tr');
        var row = tableTestCycle.row(tr);
        tmpCycleID = row.data().id;
        $('#lblCreateCycle').html('Edit Cycle: ' + row.data().name);
        getCycleDetailByCycleId(row.data().id).done(function (res) {
            $('#cycleId').val(res.id);
            // $('#project_version').val(res.JIRAversion);
            $('#project_version').selectpicker('val', res.JIRAversion);
            $('#cycle-name').val(res.name);
            $('#cycle-description').val(res.description);
            $('#build').val(res.build);
            $('#environment').val(res.environment);
            var m_startDate = moment(res.start_date);
            $('#start-date').val(res.start_date ? m_startDate.format('DD/MM/YYYY') : '');
            var m_endDate = moment(res.end_date);
            $('#end-date').val(res.end_date ? m_endDate.format('DD/MM/YYYY') : '');
            // Show popup
            $('#newcycle-dialog').modal("show");
        });
    });

    $('#sprint-table tbody').on('click', ".btn-open-clone-dialog", function () {
        var tr = $(this).closest('tr');
        var row = tableTestCycle.row(tr);
        $('#lblCreateCycle').html('Clone Cycle: ' + row.data().name);
        tmpCycleID = row.data().id;
        getCycleDetailByCycleId(row.data().id).done(function (res) {
            // $('#project_version').val(res.JIRAversion);
            setTimeout(function () {
                $('#project_version').selectpicker('val', res.JIRAversion);
                $('#cycle-name').val("CLONE " + res.name);
                $('#cycle-description').val(res.description);
                $('#build').val(res.build);
                $('#environment').val(res.environment);
                var m_startDate = moment(res.start_date);
                $('#start-date').val(res.start_date ? m_startDate.format('DD/MM/YYYY') : '');
                var m_endDate = moment(res.end_date);
                $('#end-date').val(res.end_date ? m_endDate.format('DD/MM/YYYY') : '');
            }, 500);

            //get issues of cycle
            ajaxRequest(requestURL + '/plan?cycle_id=' + row.data().id, "GET", null).then(function (dataResponse) {
                $.each(dataResponse, function () {
                    arrIssueSelected.push(this.issue_id);
                });
            }).catch(function (er) {
                // console.log('No Issue for clone');
            });
            // Show popup
            $('#newcycle-dialog').modal("show");
        });

    });

    $('#sprint-table tbody').on('click', 'td.details-control', function () {

        var tr = $(this).closest('tr');
        // console.info('click');
        nRowTestCycleSelected = tr;

        var that = this;
        if ($(that).hasClass('loading')) {
            return;
        }
        var row = tableTestCycle.row(tr);
        var cycle_id = row.data().id;
        tmpCycleID = cycle_id;
        tmpCycle_name = row.data().name;
        var tableID = "#issue-table-sprint-" + cycle_id + " tbody";
        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            var dataNested = [];
            if ($(tableID).children().length == 0) {
                $(that).append(spinner);
                $(that).addClass('loading');
                ajaxRequest(requestURL + '/plan?cycle_id=' + cycle_id, "GET", null).then(function (dataResponse) {
                    var deferredList = [];
                    $.each(dataResponse, function (index, value) {
                        var dff = $.Deferred();
                        deferredList.push(dff.promise());
                        ajaxRequest(requestURL + '/issue/' + value.issue_id, "GET", null)
                            .done(function (issue_response) {
                                var typeId = issue_response.type_id;
                                var urlExecution = '/plugins/servlet/ac/Infodation-tms/{DETERMINE_EXECUTION}?project.key='; // manual-execution | auto-execution

                                if (issue_response.type === "manual") {
                                    urlExecution = urlExecution.replace('{DETERMINE_EXECUTION}', 'manual-execution');
                                } else if (issue_response.type === "automation") {
                                    urlExecution = urlExecution.replace('{DETERMINE_EXECUTION}', 'auto-execution');
                                } else {
                                    urlExecution = urlExecution.replace('{DETERMINE_EXECUTION}', 'manual-execution');
                                }

                                ajaxRequest(requestURL + '/plan?cycle_id=' + cycle_id + '&issue_id=' + value.issue_id, "GET", null)
                                    .done(function (plan_id) {
                                        var defects = [];
                                        var formatDefects = [];
                                        //---> Check if value.defect is not null
                                        if (value.defects !== '') {
                                            defects = value.defects != null ? value.defects.split('|') : [];
                                            //---> Remove empty value on defect array
                                            $.each(defects, function (index, value) {
                                                if (value !== '') {
                                                    formatDefects.push(value);
                                                }
                                            });
                                        } else {
                                            // console.log("value.defects is null");
                                        }
                                        var arrDefect = [];
                                        var deferGetDefectDetail = [];

                                        $.each(formatDefects, function (i, value) {
                                            var _defDefectDetail = $.Deferred();
                                            deferGetDefectDetail.push(_defDefectDetail.promise());
                                            AP.request(baseUrl + '/rest/api/2/search?jql=issue=\'' + value + '\'&fields=summary,status', {
                                                success: function (res) {
                                                    var detailDefect = {
                                                        statusColor: "",
                                                        sumaryAndName: '',
                                                        value: '',
                                                    };
                                                    var datasource = JSON.parse(res);
                                                    var issue = datasource && datasource["issues"].length && datasource["issues"]["0"];
                                                    var status = issue && issue.fields ? issue.fields.status : undefined;
                                                    var summaryTextBug = issue.fields.summary;
                                                    var statusColor;
                                                    sumaryAndName = 'Summary : ' + summaryTextBug.toString() + "&#013;" + 'Status : ' + status.name.toString();

                                                    colorStatusBug = status.name;
                                                    if (colorStatusBug == "To Do") {
                                                        statusColor = "statusToDoColor";
                                                    } else if (colorStatusBug == "Done") {
                                                        statusColor = "statusDoneColor";
                                                    } else {
                                                        statusColor = "statusInProgressColor";
                                                    }

                                                    detailDefect.statusColor = statusColor;
                                                    detailDefect.sumaryAndName = sumaryAndName;
                                                    detailDefect.value = value;

                                                    arrDefect.push(detailDefect);

                                                    _defDefectDetail.resolve();
                                                },
                                                error: function () {
                                                    _defDefectDetail.reject();
                                                }
                                            });
                                        });
                                        $.when.apply($, deferGetDefectDetail).done(function () {
                                            var defect = {
                                                JIRAticket_key: issue_response.JIRAticket_key,
                                                JIRAticket_title: issue_response.JIRAticket_title,
                                                issue_id: value.issue_id,
                                                cycle_id: cycle_id,
                                                cycle_name: tmpCycle_name,
                                                planId: plan_id[0].id,
                                                created_at: value.created_at,
                                                created_by: value.created_by,
                                                modified_by: value.modified_by,
                                                defects: arrDefect,
                                                // executed_by: value.modified_by,
                                                executed_by: value.executed_by,
                                                executed_on: value.updated_at,
                                                urlExecution: urlExecution,
                                                type: issue_response.type,
                                                status: plan_id[0].status
                                            };
                                            dataNested.push(defect);
                                            dff.resolve();
                                        });
                                    });
                            });
                    });

                    $.when.apply($, deferredList).done(function () {
                        tr.addClass('shown');
                        row.child(formatTableNestedTestCycles(row.data())).show();
                        // console.log('dataNested', dataNested);

                        nestedTable($("#issue-table-sprint-" + cycle_id), dataNested);
                        $(that).find('.spinner').remove();
                        $(that).removeClass('loading');
                    });
                }).catch(function (error) {
                    // console.log('error')
                    tr.addClass('shown');
                    row.child(formatTableNestedTestCycles(row.data())).show();
                    nestedTable($("#issue-table-sprint-" + cycle_id), dataNested);
                    $(that).find('.spinner').remove();
                    $(that).removeClass('loading');
                });
            }
        }
    });

    $('#delete-cycle-dialog').on('hidden.bs.modal', function (e) {
        // tmpCycleID = null;
        $('#dialog-delete-button').find('.spinner').remove();
        $('#dialog-delete-button').removeAttr('disabled');
        $('#dialog-no-button').fadeIn('fast');
    });

    $('body').append('<div id="body-spiner">' + spinner + '<div>');

    // bind data to table CYCLES
    function bindDataToTestCycles(data) {
        var defer = $.Deferred();
        defer.promise();
        $.each(data, function () {
            tableTestCycle.row.add(this).draw();
        });
        return defer.promise();
    }

    function formatTableNestedTestCycles(d) {
        var tableID = "issue-table-sprint-" + d.id;
        return '<div>' +
            '<table class="table-nested table table-bordered m-0" id=' + tableID + ' cellpadding="5" width="100%" cellspacing="0" border="0" style="padding-left:50px;">' +
            '<thead>' +
            '<tr class="table-custom-green">' +
            '<th></th><th>ID</th>' +
            '<th>Title</th>' +
            '<th>Status</th>' +
            // '<th>Created By</th>' +
            // '<th>Modified By</th>' +
            '<th>Defect</th>' +
            '<th>Executed By</th>' +
            '<th>Executed On</th>' +
            '<th>Type</th>' +
            '<th></th>' +
            '</tr>' +
            '</thead>' +
            '<tbody></tbody>' +
            '</table>' +
            '</div>';
    }

    getProjectDetail()
        .done(function (project_response) {
            project_id = project_response[0].id;
            JIRAproject_key = project_response[0].JIRAproject_key;
            JIRAproject_id = project_response[0].JIRAproject_id;

            ajaxRequest(requestURL + '/cycle?project_id=' + project_id + '', "GET", null)
                .done(function (testCycleData) {
                    // console.log('testCycleData ', testCycleData);
                    bindDataToTestCycles(testCycleData);
                    $('#body-spiner').remove();
                    $.fn.dataTable.Api(tableTestCycle).column(10).order('desc').draw();
                })
                .catch(function (er) {
                    // console.log('Could not load cycles data', er);
                    $('#body-spiner').remove();
                });
        });

    // [Add|Edit] new cycle
    $('#dialog-submit-button').click(function () {
        if ($("form").valid() == false) {
            return false;
        }
        var urlRequest = requestURL + '/cycle';
        var method = "POST";

        var cycleId = $('#cycleId').val();
        var name = $('#cycle-name').val();
        var description = $('#cycle-description').val();
        var build = $('#build').val();
        var env = $('#environment').val();
        var start_date = $('#start-date').val();
        var end_date = $('#end-date').val();
        var _data = {
            name: name,
            description: description,
            build: build,
            environment: env,
            project_id: project_id,
            start_date: start_date,
            end_date: end_date,
            JIRAversion: $('#project_version').val(),
            created_by: currentUser.emailAddress
        };

        if (cycleId !== '') {
            method = "PUT";
            urlRequest += '/' + cycleId;
            delete _data.project_id;
            delete _data.created_by;
            $.extend(_data, {
                modified_by: currentUser.emailAddress
            });
        }
        $('#dialog-submit-button-cancel').fadeOut();
        $(this).attr('disabled', true);
        $('#dialog-submit-button').append(spinner);
        ajaxRequest(urlRequest, method, JSON.stringify(_data))
            .done(function (response) {
                if (method == 'PUT') {
                    tableTestCycle.row(nRowTestCycleSelected).data(response);
                    $('#add-issue-dialog').modal('hide');
                    $("#newcycle-dialog").modal('hide');
                    resetFormCycle();
                } else {
                    //case: Clone cycle:
                    if (arrIssueSelected.length > 0) {
                        var dataIssuesId = {
                            "issue_ids": arrIssueSelected
                        };
                        var cycleId = response.id;
                        var tmpData = response;
                        ajaxRequest(requestURL + '/plan/add_issues/' + cycleId, "POST", JSON.stringify(dataIssuesId)).done(function () {
                            getCycleDetail(cycleId).done(function (res) {
                                tmpData.issue_report = res.issue_report;
                                tableTestCycle.row.add(tmpData);
                                new $.fn.dataTable.Api(tableTestCycle).column(10).order('desc').draw();
                                $('#add-issue-dialog').modal('hide');
                                $("#newcycle-dialog").modal('hide');
                                $('[data-toggle="tooltip"]').tooltip();
                                resetFormCycle();
                            });
                        });
                        arrIssueSelected = [];

                    } else {
                        tableTestCycle.row.add(response);
                        new $.fn.dataTable.Api(tableTestCycle).column(10).order('desc').draw();
                        $("#newcycle-dialog").modal('hide');
                        resetFormCycle();
                    }
                }

            })
            .catch(function (er) {
                var error = JSON.parse(er.responseText);
                $('#lblErrorCreated').html('<label class="error">' + error.error + '</label');
                $('#lblErrorCreated').addClass('shake');
                $('#dialog-submit-button-cancel').fadeIn();
                $('#dialog-submit-button').find('.spinner').remove();
                $('#dialog-submit-button').removeAttr('disabled');
                setTimeout(function () {
                    $('#lblErrorCreated').removeClass('shake');
                }, 1000);
            });
    });

    $('#add-issue-dialog').on('show.bs.modal', function (e) {
        searchIssueByCycleName(project_id).done(function (res) {
            autoCompleteForIssueName(res);
        });
    });

    $('#add-issue-dialog').on('hidden.bs.modal', function (e) {
        var strValue = $('#issue-name').val();
        if ($.trim(strValue) === '') {
            $('#issue-name').keypress(function () {
                $('#issue-name').addClass('is-invalid');
            });
        }
        $('#dialog-issue-button-cancel').fadeIn();
        $('#issue-name').val('');
        // tmpCycleID = null;
        arrIssueSelected = [];
        $('#hddSelectedIssueId').val(arrIssueSelected);
        $('#selected-issues').html('');
        $('#errorAddIssues').html('');
        $('.spinner').remove();
        $('#dialog-issue-button').removeAttr('disabled');
        $('#issue-name').removeClass('is-invalid');
        $('#issue-name').autoComplete('destroy');
    });
    $('#issue-name').each(function () {
        $(this).on('keypress', function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                var _that = this;

                $('#issue-name').removeClass('is-invalid');
                if (arrIssueSelected.length > 0) {
                    var data = {
                        "issue_ids": arrIssueSelected
                    };
                    if (tmpCycleID != null) {
                        // $(_that).attr('disabled', true);
                        $('#dialog-issue-button-cancel').fadeOut();
                        $(this).append(spinner);
                        ajaxRequest(requestURL + '/plan/add_issues/' + tmpCycleID, "POST", JSON.stringify(data))
                            .done(function (data) {
                                // var data = JSON.parse(response);
                                var tableNested = $("#issue-table-sprint-" + tmpCycleID);
                                var dataNested = [];
                                var deferredList = [];
                                //reload nested table issues
                                $.each(data, function (index, value) {
                                    var dff = $.Deferred();
                                    deferredList.push(dff.promise());

                                    ajaxRequest(requestURL + '/issue/' + value.issue_id, "GET", null)
                                        .done(function (issue_response) {
                                            var typeId = issue_response.type_id;
                                            var urlExecution = '/plugins/servlet/ac/Infodation-tms/{DETERMINE_EXECUTION}?project.key='; // manual-execution | auto-execution

                                            if (issue_response.type === "manual") {
                                                urlExecution = urlExecution.replace('{DETERMINE_EXECUTION}', 'manual-execution');
                                            } else if (issue_response.type === "automation") {
                                                urlExecution = urlExecution.replace('{DETERMINE_EXECUTION}', 'auto-execution');
                                            } else {
                                                urlExecution = urlExecution.replace('{DETERMINE_EXECUTION}', 'manual-execution');
                                            }

                                            ajaxRequest(requestURL + '/plan?cycle_id=' + tmpCycleID + '&issue_id=' + value.issue_id, "GET", null)
                                                .done(function (plan_id) {
                                                    var _data = {
                                                        JIRAticket_key: issue_response.JIRAticket_key,
                                                        JIRAticket_title: issue_response.JIRAticket_title,
                                                        issue_id: value.issue_id,
                                                        cycle_id: tmpCycleID,
                                                        cycle_name: tmpCycle_name,
                                                        planId: plan_id[0].id,
                                                        created_by: value.created_by,
                                                        created_at: value.created_at,
                                                        modified_by: value.modified_by,
                                                        defects: value.defects,
                                                        // executed_by: value.modified_by,
                                                        executed_by: value.executed_by,
                                                        executed_on: value.updated_at,
                                                        urlExecution: urlExecution,
                                                        type: issue_response.type,
                                                        status: plan_id[0].status
                                                    };
                                                    // addDataToNestedTable(tableNested, _data);
                                                    dataNested.push(_data);
                                                    dff.resolve();
                                                });
                                        });
                                });
                                $.when.apply($, deferredList).done(function () {
                                    addNewRowToNestedTable(tableNested, dataNested);
                                    // tmpCycleID = null;
                                });
                            })
                            .catch(function (er) {
                                var error = JSON.parse(er.responseText);
                                $('#errorAddIssues').html('<label style="margin-top:10px;">* ' + error.error + '</label>');
                                $(_that).find('.spinner').remove();
                                $(_that).removeAttr('disabled');
                                $('#dialog-issue-button-cancel').fadeIn();
                                return;
                            });
                    }
                } else {
                    $('#issue-name').addClass('is-invalid');
                }
            }
        });
    });
    // Delete Cycle
    $('#dialog-delete-button').click(function () {
        $(this).append(spinner);
        $(this).attr('disabled', true);
        $('#dialog-no-button').fadeOut('fast');
        ajaxRequest(requestURL + '/cycle/' + tmpCycleID, "DELETE", null)
            .done(function (response) {
                $('#delete-cycle-dialog').modal('hide');
                $('#dialog-delete-button').find('.spinner').remove();
                $('#dialog-delete-button').removeAttr('disabled');
                dialogMessage.show("Delete success", response.success);

                // remove nested table cycle
                $("#issue-table-sprint-" + tmpCycleID).parentsUntil('tbody').remove();
                tableTestCycle.row(nRowTestCycleSelected).remove().draw();

                // tmpCycleID = -1;
            });
    });

    // [Add] issues to cycle
    $('#dialog-issue-button').click(function () {
        var _that = this;

        $('#issue-name').removeClass('is-invalid');
        if (arrIssueSelected.length > 0) {
            var data = {
                "issue_ids": arrIssueSelected
            };
            if (tmpCycleID != null) {
                $(_that).attr('disabled', true);
                $('#dialog-issue-button-cancel').fadeOut();
                $(this).append(spinner);
                ajaxRequest(requestURL + '/plan/add_issues/' + tmpCycleID, "POST", JSON.stringify(data))
                    .done(function (data) {
                        // var data = JSON.parse(response);
                        var tableNested = $("#issue-table-sprint-" + tmpCycleID);
                        var dataNested = [];
                        var deferredList = [];
                        //reload nested table issues
                        $.each(data, function (index, value) {
                            var dff = $.Deferred();
                            deferredList.push(dff.promise());

                            ajaxRequest(requestURL + '/issue/' + value.issue_id, "GET", null)
                                .done(function (issue_response) {
                                    var typeId = issue_response.type_id;
                                    var urlExecution = '/plugins/servlet/ac/Infodation-tms/{DETERMINE_EXECUTION}?project.key='; // manual-execution | auto-execution

                                    if (issue_response.type === "manual") {
                                        urlExecution = urlExecution.replace('{DETERMINE_EXECUTION}', 'manual-execution');
                                    } else if (issue_response.type === "automation") {
                                        urlExecution = urlExecution.replace('{DETERMINE_EXECUTION}', 'auto-execution');
                                    } else {
                                        urlExecution = urlExecution.replace('{DETERMINE_EXECUTION}', 'manual-execution');
                                    }

                                    ajaxRequest(requestURL + '/plan?cycle_id=' + tmpCycleID + '&issue_id=' + value.issue_id, "GET", null)
                                        .done(function (plan_id) {
                                            var _data = {
                                                JIRAticket_key: issue_response.JIRAticket_key,
                                                JIRAticket_title: issue_response.JIRAticket_title,
                                                issue_id: value.issue_id,
                                                cycle_id: tmpCycleID,
                                                cycle_name: tmpCycle_name,
                                                planId: plan_id[0].id,
                                                created_by: value.created_by,
                                                created_at: value.created_at,
                                                modified_by: value.modified_by,
                                                defects: value.defects,
                                                // executed_by: value.modified_by,
                                                executed_by: value.executed_by,
                                                executed_on: value.updated_at,
                                                urlExecution: urlExecution,
                                                type: issue_response.type,
                                                status: plan_id[0].status
                                            };
                                            // addDataToNestedTable(tableNested, _data);
                                            dataNested.push(_data);
                                            dff.resolve();
                                        });
                                });
                        });
                        $.when.apply($, deferredList).done(function () {
                            addNewRowToNestedTable(tableNested, dataNested);
                            // tmpCycleID = null;
                        });
                    })
                    .catch(function (er) {
                        var error = JSON.parse(er.responseText);
                        $('#errorAddIssues').html('<label style="margin-top:10px;">* ' + error.error + '</label>');
                        $(_that).find('.spinner').remove();
                        $(_that).removeAttr('disabled');
                        $('#dialog-issue-button-cancel').fadeIn();
                        return;
                    });
            }
        } else {
            $('#issue-name').addClass('is-invalid');
        }
    });

    function searchIssueByCycleName(projectId) {
        // console.log('search issue by cycle name');
        var deffer = $.Deferred();
        if (typeof projectId == 'undefined') {
            deffer.reject("error");
        } else {
            ajaxRequest(requestURL + '/issue?project_id=' + projectId, "GET", null)
                .done(function (response) {
                    // var data = JSON.parse(response);
                    deffer.resolve(response);
                });
        }
        return deffer.promise();
    }

    function getCycleDetailByCycleId(cycleId) {
        // console.log('get cycle detail', cycleId);
        var deffer = $.Deferred();
        ajaxRequest(requestURL + '/cycle/' + cycleId, "GET", null).done(function (response) {
            // var data = JSON.parse(response);
            deffer.resolve(response);
        });
        return deffer.promise();
    }

    function nestedTable(table, dataNested) {
        table.DataTable({
            paging: false,
            searching: false,
            info: false,
            // autoWidth: false,
            data: dataNested,
            columns: [{
                    data: 'issue_id',
                    visible: false
                }, {
                    width: '35px',
                    data: 'JIRAticket_key',
                    render: function (data, type, row, meta) {
                        return '<a target="_blank" href=' + projectBaseUrl + data + '>' + data + '</a>';
                    }
                }, {
                    width: '200px',
                    data: 'JIRAticket_title'
                },
                {
                    data: 'status',
                    width: '100px',
                    className: 'td-status',
                    render: function (data, type, row, meta) {
                        var color = 'badge-secondary';
                        switch (data.trim()) {
                            case 'FAIL':
                                color = 'badge-danger'; // FAIL
                                break;
                            case 'BLOCKED':
                                color = 'badge-warning'; // BLOCK
                                break;
                            case 'WIP':
                                color = 'badge-info'; // WIP
                                break;
                            case 'PASS':
                                color = 'badge-success'; // PASS
                                break;
                            default:
                                break;
                        }
                        return '<span class="btn badge badge-pill ' + color + ' change-status btn-block">' + data + '</span>';
                    }
                },
                // {
                //     data: 'created_by'
                // },
                // {
                //     // data: 'modified_by',
                //     data: function (row, type, val, meta) {
                //         if (row !== null && row.modified_by !== null) {
                //             var _user = lodash.find(usersJira, function (o) {
                //                 return o.emailAddress == row.modified_by;
                //             });
                //             if (typeof _user !== 'undefined') {
                //                 return _user.displayName;
                //             }
                //         }
                //         return row.modified_by;
                //     }
                // },


                {
                    data: 'defects',
                    width: '200px',
                    "render": function (data, type, row, meta) {
                        var tmpl = '';
                        $.each(data, function () {
                            tmpl += '<a class="btn ' + this.statusColor + ' badge badge-pill badge-secondary mr-1"  data-toggle="tooltip"  title="' + this.sumaryAndName + '" target="_blank" href=' + projectBaseUrl + this.value + '> ' + this.value + '</a>';
                        });
                        return '<div class="inner-td">' + tmpl + '</div>';
                    }
                },
                {
                    // data: 'executed_by',
                    data: function (row, type, val, meta) {
                        if (row !== null && row.executed_by !== null) {
                            var _user = lodash.find(usersJira, function (o) {
                                return o.emailAddress == row.executed_by;
                            });
                            if (typeof _user !== 'undefined') {
                                return _user.displayName;
                            }
                        }
                        return row.executed_by;
                    }
                },
                {
                    data: function (row, type, val, meta) {
                        if (row.executed_on === row.created_at) {
                            return '';
                        }
                        return row.executed_on ? moment(row.executed_on).format('DD/MM/YYYY') : '';
                    }
                },
                {
                    data: 'type',
                    orderable: false,
                },
                {
                    data: null,
                    width: 118,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return '<a class="btn btn-link py-1 px-2" href=' + baseUrl + data.urlExecution + JIRAproject_key +
                            '&ac.projectId=' + JIRAproject_id +
                            '&ac.issueId=' + data.issue_id +
                            '&ac.cycleId=' + data.cycle_id +
                            '&ac.cycleName=' + escape(data.cycle_name) +
                            '&ac.planId=' + data.planId + ' target="_parent"><span class="oi" data-glyph="play-circle"></span></a>' +
                            '<a href="javascript:;" class="btn btn-link delete_issue py-1 px-2"><span class="oi" data-glyph="trash"></span></a>';
                    }
                }
            ],
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(nRow).find('.delete_issue').on('click', function () {
                    tmpCycle_name = aData.cycle_name;
                    ShowConfirmDeleteCycleIssue(nRow, aData.cycle_id, aData.issue_id, aData.JIRAticket_key);
                });
            }
        });
    }

    function resetFormCycle() {
        $('#cycleId').val('');
        $('#cycle-name').val('');
        $('#cycle-description').val('');
        $('#build').val('');
        $('#environment').val('');
        $('#start-date').val('');
        $('#end-date').val('');
        $('#project_version').val('');
        nRowTestCycleSelected = -1;
        $('#lblErrorCreated').html('');
        $('#lblErrorCreated').removeClass('shake');
        $('#dialog-submit-button').find('.spinner').remove();
        $('#dialog-submit-button').removeAttr('disabled');
    }

    $("#newcycle-dialog").on('show.bs.modal', function () {
        if ($('#cycleId').val() == '') {
            // $('#project_version').val("UNSCHEDULE");
            $('#project_version').selectpicker('val', 'UNSCHEDULE');
        }
        $('#content').css('height', '695px');
    });

    $('#newcycle-dialog').on('hidden.bs.modal', function (e) {
        $('#content').css('height', '');
        $('#lblCreateCycle').html('Create New Test Cycle');
        $('#dialog-submit-button').find('.spinner').remove();
        $('#dialog-submit-button').removeAttr('disabled');
        $('#dialog-submit-button-cancel').fadeIn();
        $('#cycle-name-error').remove();
        $('#end-date-error').remove();
        $('#start-date-error').remove();
        $('.is-invalid').removeClass('is-invalid');
        resetFormCycle();
        // tmpCycleID = null;
    });

    $("#delete-cycle-issue-dialog").on('hidden.bs.modal', function () {
        $('#lblCycleIssueNameToDelete').html('');
        // $('#dialog-delete-cycleIssue-button').unbind('click');
        $('#dialog-no-cycleIssue-button').fadeIn('fast');
        $('.spinner').remove();
    });
});

function addNewRowToNestedTable(table, data) {
    if ($.fn.DataTable.isDataTable(table)) {
        new $.fn.dataTable.Api(table).rows.add(data);
        new $.fn.dataTable.Api(table).columns.adjust().draw();
    }
    getCycleDetail(tmpCycleID).done(function (res) {
        var newdata = tableTestCycle.row(nRowTestCycleSelected).data();
        newdata.issue_report = res.issue_report;
        tableTestCycle.row(nRowTestCycleSelected).data(newdata).draw();
        $('#add-issue-dialog').modal('hide');
    });
}

function removeRowNestedTable(table, tr) {
    if ($.fn.DataTable.isDataTable(table)) {
        new $.fn.dataTable.Api(table).row(tr).remove();
        new $.fn.dataTable.Api(table).columns.adjust().draw();
    }
}

function ShowConfirmDeleteCycleIssue(nRow, cycleId, issueId, titleIssue) {
    $('#lblCycleIssueNameToDelete').html(titleIssue);
    $('#delete-cycle-issue-dialog').modal('show');
    $('#dialog-delete-cycleIssue-button').unbind('click');
    $('#dialog-delete-cycleIssue-button').bind('click', function () {
        $('#dialog-delete-cycleIssue-button').append(spinner);
        $('#dialog-no-cycleIssue-button').fadeOut('fast');
        deleteCycleIssue(cycleId, issueId).done(function () {
            var nestTable = $(nRow).parents('table');
            removeRowNestedTable(nestTable, nRow);
        });
    });
}

function deleteCycleIssue(cycleId, issueId) {
    var deffer = $.Deferred();
    deffer.promise();
    ajaxRequest(requestURL + '/plan/delete_issues/' + cycleId + '/' + issueId, "DELETE", null)
        .done(function (response) {
            getCycleDetail(tmpCycleID).then(function (res) {
                var newdata = tableTestCycle.row(nRowTestCycleSelected).data();
                newdata.issue_report = res.issue_report;
                tableTestCycle.row(nRowTestCycleSelected).data(newdata).draw();
                $('#delete-cycle-issue-dialog').modal('hide');
                deffer.resolve();
            });
        });
    return deffer;
}

//search Issue name
function autoCompleteForIssueName(datasource) {
    $('#issue-name').autoComplete({
        minChars: 0,
        cache: false,
        delay: 0,
        source: function (term, suggest) {
            term = term.toLowerCase();
            var suggestions = [];
            for (i = 0; i < datasource.length; i++) {
                if (~datasource[i].JIRAticket_title.toLowerCase().indexOf(term)) {
                    var idx = lodash.findIndex(arrIssueSelected, function (o) {
                        return o == datasource[i].id;
                    });
                    if (idx == -1) {
                        suggestions.push(datasource[i]);
                    }
                } else if (~datasource[i].JIRAticket_key.toLowerCase().indexOf(term)) {
                    var _idx = lodash.findIndex(arrIssueSelected, function (o) {
                        return o == datasource[i].id;
                    });
                    if (_idx == -1) {
                        suggestions.push(datasource[i]);
                    }
                }
            }
            if (suggestions.length == 0) {
                suggest([{
                    id: -1,
                    message: "no data found"
                }]);
            } else
                suggest(suggestions);
        },
        renderItem: function (item, search) {
            search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
            if (item.id == -1) {
                return '<div class="autocomplete-suggestion" data-values="-1">' + item.message + '</div>';
            }
            return '<div class="autocomplete-suggestion" data-text="' + item.JIRAticket_key + '" data-values="' + item.id + '">' +
                '<span>' + item.JIRAticket_key.replace(re, "<b>$1</b>") + '</span> - ' +
                item.JIRAticket_title.replace(re, "<b>$1</b>") + '</div>';
        },
        onSelect: function (e, term, item) {
            if (item.data("values") == -1) {
                return;
            }
            var selectedIssue = '<div data-issueId="' + item.data("values") + '" class="selected-tag badge badge-pill badge-primary"><a target="_blank" href=' + projectBaseUrl + item.data("text") + '>' + item.data("text") + '</a><a href="javascript:;" onclick="unselectedIssue(' + item.data("values") + ')">&times;</a></div>';
            $('#selected-issues').append(selectedIssue);
            arrIssueSelected.push(item.data("values"));
            $('#hddSelectedIssueId').val(arrIssueSelected);
            $('#issue-name').blur();
        }
    });
}

function unselectedIssue(issueId) {
    var _issueSelected = lodash.pull(arrIssueSelected, issueId);
    arrIssueSelected = _issueSelected;
    $('#hddSelectedIssueId').val(arrIssueSelected);
    $('#selected-issues').find('.selected-tag[data-issueId="' + issueId + '"]').remove();
}

function generateCycleTestReport(reportData) {

    var countUnxecutedManual = lodash.filter(reportData, function (d) {
        return d.status === "UNEXECUTED" && d.type === "manual";
    }).length;
    var countUnxecutedAutomation = lodash.filter(reportData, function (d) {
        return d.status === "UNEXECUTED" && d.type === "automation";
    }).length;

    var countPassManual = lodash.filter(reportData, function (d) {
        return d.status === "PASS" && d.type === "manual";
    }).length;
    var countPassAutomation = lodash.filter(reportData, function (d) {
        return d.status === "PASS" && d.type === "automation";
    }).length;

    var countFailManual = lodash.filter(reportData, function (d) {
        return d.status === "FAIL" && d.type === "manual";
    }).length;
    var countFailAutomation = lodash.filter(reportData, function (d) {
        return d.status === "FAIL" && d.type === "automation";
    }).length;

    var countWipManual = lodash.filter(reportData, function (d) {
        return d.status === "WIP" && d.type === "manual";
    }).length;
    var countWipAutomation = lodash.filter(reportData, function (d) {
        return d.status === "WIP" && d.type === "automation";
    }).length;

    var countBlockedManual = lodash.filter(reportData, function (d) {
        return d.status === "BLOCKED" && d.type === "manual";
    }).length;
    var countBlockedAutomation = lodash.filter(reportData, function (d) {
        return d.status === "BLOCKED" && d.type === "automation";
    }).length;

    var countPass = countPassManual + countPassAutomation;
    var countFail = countFailManual + countFailAutomation;
    var countWip = countWipManual + countWipAutomation;
    var countBlocked = countBlockedManual + countBlockedAutomation;
    var countUnxecuted = countUnxecutedManual + countUnxecutedAutomation;

    var totalManual = countPassManual + countFailManual + countWipManual + countBlockedManual + countUnxecutedManual;
    var totalAutomation = countPassAutomation + countFailAutomation + countWipAutomation + countBlockedAutomation + countUnxecutedAutomation;

    var spanPa = '<span class="pass" style="width: ' + percent(countPass, reportData.length) + '%"></span>';
    var spanFa = '<span class="fail" style="width: ' + percent(countFail, reportData.length) + '%"></span>';
    var spanWi = '<span class="wip" style="width: ' + percent(countWip, reportData.length) + '%"></span>';
    var spanBloc = '<span class="blocked" style="width: ' + percent(countBlocked, reportData.length) + '%"></span>';
    var spanUn = '<span class="unxecuted" style="width: ' + percent(countUnxecuted, reportData.length) + '%"></span>';

    var tr1 = '<tr><td>Unexcuted</td><td>' + countUnxecutedManual + '</td><td>Unexcuted</td><td>' + countUnxecutedAutomation + '</td></tr>';
    var tr2 = '<tr><td>Blocked</td><td>' + countBlockedManual + '</td><td>Blocked</td><td>' + countBlockedAutomation + '</td></tr>';
    var tr3 = '<tr><td>WIP</td><td>' + countWipManual + '</td><td>WIP</td><td>' + countWipAutomation + '</td></tr>';
    var tr4 = '<tr><td>Fail</td><td>' + countFailManual + '</td><td>Fail</td><td>' + countFailAutomation + '</td></tr>';
    var tr5 = '<tr><td>Pass</td><td>' + countPassManual + '</td><td>Pass</td><td>' + countPassAutomation + '</td></tr>';
    var trTotal = '<tr><td>TOTAL</td><td>' + totalManual + '</td><td>TOTAL</td><td>' + totalAutomation + '</td></tr>'

    var table = '<table><tr><td>MANUAL</td><td></td><td>AUTOMATION</td><td></td></tr>' + tr1 + tr2 + tr3 + tr4 + tr5 + trTotal + '</table>';
    var tooltip = '<div>' + table + '</div>';

    return '<div class="w-report" data-container="#sprint-table_wrapper" data-toggle="tooltip" data-html="true" title="' + tooltip + '">' + spanPa + spanFa + spanWi + spanBloc + spanUn + '</div>'
}

function percent(number, total) {
    return number * 100 / total;
}

function getCycleDetail(cycleId) {
    return ajaxRequest(requestAPI.getCycleDetail + '/' + cycleId, 'GET', null)
}