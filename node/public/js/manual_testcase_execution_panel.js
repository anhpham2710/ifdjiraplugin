$(function () {
    var lodash = _;
    var arrTestCaseExecution = [];
    var JIRAproject_id = null;
    var JIRAproject_key = null;
    var arrJiraVersion = null;
    var usersJira = null;

    

    function getIssueDetail() {
        ajaxRequest(requestAPI.getIssueDetail, 'GET', {
                project_id: project_id,
                JIRAticket_key: issueKey
            })
            .done(function (res) {
                var mainGetIssuesDeffer = [];
                arrTestCaseExecution = [];
                var testcycles = res[0].cycles;
                var testplans = res[0].plans;
                var testplans_arr = [];
                $.each(testplans, function (index, plan) {
                    var testplan = {
                        status: plan.status,
                        // _defects: arrDefect,
                        defects: plan.defects,
                        // executed_by: plan.modified_by,
                        executed_by: plan.executed_by,
                        created_at: plan.created_at,
                        executed_on: plan.updated_at,
                        issue_id: plan.issue_id,
                        cycle_id: plan.cycle_id,
                        plan_id: plan.id
                    };
                    testplans_arr.push(testplan);

                });

                var testcycle_arry = [];
                $.each(testcycles, function (index, cycle) {
                    var testcycle = {
                        version: cycle.JIRAversion,
                        test_cycle: cycle.name,
                    };
                    testcycle_arry.push(testcycle);
                });
                $.each(testplans_arr, function (i, item) {
                    var TestCaseExecution = $.extend(true, testplans_arr[i], testcycle_arry[i]);
                    arrTestCaseExecution.push(TestCaseExecution);
                });
                $.each(arrTestCaseExecution, function (i, val) {
                    var mainDeferGetDefectDetail = [];
                    var arrDefect = [];
                    var _defects = val.defects != null ? val.defects.split('|') : [];
                    $.each(_defects, function (i, value) {
                        var def = $.Deferred();
                        mainDeferGetDefectDetail.push(def.promise());
                        mainGetIssuesDeffer.push(def.promise());
                        AP.request(baseUrl + '/rest/api/2/search?jql=issue=\'' + value + '\'&fields=summary,status', {
                            success: function (res) {
                                var detailDefect = {
                                    statusColor: "",
                                    sumaryAndName: '',
                                    value: '',
                                };
                                var datasource = JSON.parse(res);
                                var issue = datasource && datasource["issues"].length && datasource["issues"]["0"];
                                var status = issue && issue.fields ? issue.fields.status : undefined;
                                var summaryTextBug = issue.fields.summary;
                                var statusColor;
                                sumaryAndName = 'Summary : ' + summaryTextBug.toString() + "&#013;" + 'Status : ' + status.name.toString();

                                colorStatusBug = status.name;
                                if (colorStatusBug == "To Do") {
                                    statusColor = "statusToDoColor";
                                } else if (colorStatusBug == "Done") {
                                    statusColor = "statusDoneColor";
                                } else {
                                    statusColor = "statusInProgressColor";
                                }

                                detailDefect.statusColor = statusColor;
                                detailDefect.sumaryAndName = sumaryAndName;
                                detailDefect.value = value;
                                arrDefect.push(detailDefect);
                                def.resolve();
                            },
                            error: function () {
                                def.resolve();
                            }
                        });
                    });

                    $.when.apply($, mainDeferGetDefectDetail).done(function () {
                        arrTestCaseExecution[i].defects = arrDefect;
                    });
                });
            
                $.when.apply($, mainGetIssuesDeffer).done(function () {
                    setTimeout(function () {
                        tblTestCaseExcution.clear();
                        tblTestCaseExcution.rows.add(arrTestCaseExecution).draw();
                        $('#refresh-data .fas').removeClass('fa-spin');
                    }, 500);
                });


            })
            .catch(function (err) {
                $('#refresh-data .fas').removeClass('fa-spin');
            });
    }

    function getProjectDetailTestCase() {
        $('#refresh-data .fas').addClass('fa-spin');
        getProjectDetail().done(function (res) {
            var project_response = res[0];
            project_id = project_response.id;
            JIRAproject_id = project_response.JIRAproject_id;
            JIRAproject_key = project_response.JIRAproject_key;
            AP.request('/rest/api/2/user/assignable/search?project=' + JIRAproject_id, {
                success: function (res) {
                    usersJira = lodash.filter(JSON.parse(res), function (o) {
                        return o.emailAddress.indexOf("@connect.atlassian.com") === -1;
                    });
                }
            });
        
            IFDRequest.customRequest('/rest/api/2/project/' + JIRAproject_id + '/versions', "GET", "application/json", null)
                .then(function (response) {
                    arrJiraVersion = JSON.parse(response.body);
                })
                .catch(function (er) {
                    // console.log('Could not load Jira version', er);
                });
            if (issueKey == null) {
                AP.request('/rest/api/2/issue/' + issueKey, {
                    success: function (res) {
                        issueKey = resData.key;
                        getIssueDetail();
                    }
                });
            } else {
                getIssueDetail();
            }
        });
    }
    var tblTestCaseExcution = $('#manual_testcase_execution_panel').DataTable({
        paging: false,
        searching: false,
        info: false,
        ordering: false,
        autoWidth: false,
        data: null,
        columns: [{
                data: 'version',
                className: 'td-status',
                render: function (data, type, row, meta) {
                    var versionName = lodash.find(arrJiraVersion, function (o) {
                        return o.id == data;
                    });
                    return versionName ? versionName.name : data;
                }
            },
            {
                data: 'test_cycle'
            },
            {
                data: 'status',
                className: 'td-status',
                render: function (data, type, row, meta) {
                    var color = 'badge-secondary';
                    switch (data.trim()) {
                        case 'FAIL':
                            color = 'badge-danger'; // FAIL
                            break;
                        case 'BLOCKED':
                            color = 'badge-warning'; // BLOCK
                            break;
                        case 'WIP':
                            color = 'badge-info'; // WIP
                            break;
                        case 'PASS':
                            color = 'badge-success'; // PASS
                            break;
                        default:
                            break;
                    }
                    return '<span class="btn badge badge-pill ' + color + ' change-status btn-block">' + data + '</span>';
                }
            },

            {
                data: 'defects',
                width: '195px',
                "render": function (data, type, row, meta) {
                    var tmpl = '';
                    $.each(data, function () {
                        tmpl += '<a class="btn ' + this.statusColor + ' badge badge-pill badge-secondary mr-1"  data-toggle="tooltip"  title="' + this.sumaryAndName + '" target="_blank" href=' + projectBaseUrl + this.value + '>' + this.value + '</a>';
                    });
                    return '<div class="inner-td">' + tmpl + '</div>';
                }
            },
        
            {
                // data: 'executed_by'
                data: function (row, type, val, meta) {
                    if (row !== null && row.executed_by !== null) {
                        var _user = lodash.find(usersJira, function (o) {
                            return o.emailAddress == row.executed_by;
                        });
                        if (typeof _user !== 'undefined') {
                            return _user.displayName;
                        }
                    }
                    return row.executed_by;
                }
            },
            {
                // data: 'executed_on'
                data: function (row, type, val, meta) {
                    if (row.executed_on === row.created_at) {
                        return '';
                    }
                    return row.executed_on ? moment(row.executed_on).format('DD/MM/YYYY') : '';
                }
            },
            {
                data: null,
                width: 32,
                orderable: false,
                render: function (data, type, row, meta) {
                    return '<a class="btn btn-link py-1 px-2" href=' + baseUrl + '/plugins/servlet/ac/Infodation-tms/manual-execution?project.key=' + JIRAproject_key +
                        '&ac.projectId=' + JIRAproject_id +
                        '&ac.issueId=' + data.issue_id +
                        '&ac.cycleId=' + data.cycle_id +
                        '&ac.cycleName=' + escape(data.test_cycle) +
                        '&ac.planId=' + data.plan_id + ' target="_blank"><span class="oi" data-glyph="play-circle"></span></a>';
                }
            }
        ]
    });

    getProjectDetailTestCase();

    $('#refresh-data').on('click', function () {
        getProjectDetailTestCase();
    });


});