var requestURL = 'https://ifdtms.infodation.com:1235/api/v1';
var requestAPI = {
    getProjectDetail: requestURL + '/project', //GET: /project?company_name=' + company_name + '&JIRAproject_key=' + projectKey
    getTestExecution: requestURL + "/plan/",
    updateLatestTestcase: requestURL + "/plan/",
    manualTestCaseExecution: requestURL + '/manual_execution/',
    autoTestCaseExecution: requestURL + '/auto_execution/',
    autoTestCase: requestURL + '/auto_testcase/',
    manualTestCase: requestURL + '/manual_testcase/',
    scenario: requestURL + '/scenario',
    updateManualTestCaseExecution: requestURL + '/execution/manual_testcase/',
    updateAutoTestCaseExecution: requestURL + '/execution/auto_testcase/',
    updatePlanTestExecution: requestURL + '/plan/update/', // 'PUT /plan/update/{cycle_id}/{issue_id}'
    getIssueDetail: requestURL + '/issue', //GET: issue?project_id=' + project_id + '&JIRAticket_key=' + issueKey,
    getCycleDetail: requestURL + '/cycle',
    stepsDefinition: requestURL + '/steps_definition/',
    stepsParam: requestURL + '/steps_param/',
    scenarioSteps: requestURL + '/scenario_steps/',
    scenarioStepParam: requestURL + '/scenario_steps_param/',
};
var urlTemplateRender = '/js/templates/';

var testcaseType = {
    manual: 1,
    automation: 2
};
