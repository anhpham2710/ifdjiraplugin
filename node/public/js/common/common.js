/*jshint esversion: 6 */
const paramsUrl = getQueryParams(document.location.search);
const baseUrl = paramsUrl.xdm_e + paramsUrl.cp;
const hostBaseUrl = paramsUrl.xdm_e;
const projectBaseUrl = baseUrl + "/browse/";
const company_name = hostBaseUrl.match(/\/\/(.+)\.atlassian.net/)[1];
const jiraProjectId = paramsUrl.projectId;
const projectKey = paramsUrl.pkey;
const issueKey = paramsUrl.issueKey;
const optionsFuzzySearch = {
    shouldSort: true,
    threshold: 0.6,
    location: 0,
    distance: 100,
    maxPatternLength: 32,
    minMatchCharLength: 1,
    keys: [
        "key",
        "summary",
        "name",
        "keyword",
        "content"
    ]
};


var currentUser = null;
// get current user
AP.request('/rest/api/2/myself', {
    success: function (res) {
        currentUser = JSON.parse(res);
    }
});

function getClientKeyFromJWT(token) {
    var playload = JSON.parse(atob(token.split('.')[1]));
    return playload.iss;
}

function getQueryParams(qs) {
    qs = qs.split("+").join(" ");

    var params = {},
        tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
    }
    return params;
}


// message dialog
var dialogMessage = {
    show: function (title, message) {
        var tpl = $('<div class="modal modal-delete fade ifdModal" id="message-dialog" tabindex="-1" role="dialog" \aria-labelledby="exampleModalLabel" aria-hidden="true">\
            <div class="modal-dialog" role="document">\
                <div class="modal-content">\
                    <div class="modal-header">\
                        <h5 class="modal-title">' + title + '</h5>\
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">\
                            <span aria-hidden="true">&times;</span>\
                        </button>\
                    </div>\
                    <div class="modal-body">\
                        <div class="form-group m-0">' + message + '</div>\
                    </div>\
                    <div class="modal-footer">\
                        <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close">OK</button>\
                    </div>\
                </div>\
            </div>\
        </div>');
        $('body').append(tpl);
        $('#message-dialog').modal('show');
        $('#message-dialog').on('hidden.bs.modal', function () {
            $('#message-dialog').remove();
        });
    }
};

var jwt_token = paramsUrl.jwt;

function getAccessToken(_url, _method) {
    var clientKey = getClientKeyFromJWT(jwt_token);
    return $.ajax({
        url: requestURL + '/addon/generate_token',
        type: 'POST',
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify({
            client_key: clientKey,
            http_method: _method,
            request_url: _url
        })
    });
}
var ajaxRequest = function (_url, _method, _params) {
    var fakeUrl = _url;
    if (_method == 'GET' && _params != null) {
        var newParams = Object.keys(_params).map(function (k) {
            return encodeURIComponent(k) + "=" + encodeURIComponent(_params[k]);
        }).join('&');
        fakeUrl += '?' + newParams;
    }
    var def = $.Deferred();
    def.promise();
    getAccessToken(fakeUrl, _method).done(function (response) {
        $.ajax({
            url: _url,
            type: _method,
            contentType: "application/json",
            data: _params,
            dataType: "json",
            crossDomain: true,
            headers: {
                'Authorization': 'JWT ' + response.token,
                'Accept': '*/*',
                'Content-Type': 'application/json'
            },
            success: function (res) {
                def.resolve(res);
            },
            error: function (res) {
                def.reject(res);
            }
        });
    });
    return def;
};

var ajaxRequestImport = function (_url, _method, _params) {
    var fakeUrl = _url;
    if (_method == 'GET' && _params != null) {
        var newParams = Object.keys(_params).map(function (k) {
            return encodeURIComponent(k) + "=" + encodeURIComponent(_params[k]);
        }).join('&');
        fakeUrl += '?' + newParams;
    }
    var def = $.Deferred();
    def.promise();
    getAccessToken(fakeUrl, _method).done(function (response) {
        $.ajax({
            url: _url,
            type: _method,
            data: _params,
            contentType: false,
            processData: false,
            headers: {
                'Authorization': 'JWT ' + response.token,
                'Accept': '*/*'
            },
            success: function (res) {
                def.resolve(res);
            },
            error: function (res) {
                def.reject(res);
            }
        });
    });
    return def;
};

function getStatus() {
    return ajaxRequest(requestURL + '/status', "GET", null);
}

function updatePlanTestCase(cycleId, issueId, data) {
    var _data = Object.assign({
        // modified_by: currentUser.emailAddress
        executed_by: currentUser.displayName
    }, data);
    // var _data = JSON.stringify(data);
    return ajaxRequest(requestAPI.updatePlanTestExecution + cycleId + '/' + issueId, "PUT", JSON.stringify(_data));
}

var spinner = '<div id="floatingBarsG" class="spinner">' +
    '<div class="blockG" id="rotateG_01"></div>' +
    '<div class="blockG" id="rotateG_02"></div>' +
    '<div class="blockG" id="rotateG_03"></div>' +
    '<div class="blockG" id="rotateG_04"></div>' +
    '<div class="blockG" id="rotateG_05"></div>' +
    '<div class="blockG" id="rotateG_06"></div>' +
    '<div class="blockG" id="rotateG_07"></div>' +
    '<div class="blockG" id="rotateG_08"></div>' +
    '</div>';

function getProjectDetail() {
    var deffer = $.Deferred();
    IFDRequest.customRequest('/rest/api/2/project/' + projectKey, "GET", "application/json", null)
        .then(function (response) {
            JIRAproject_parsed = JSON.parse(response.body);
            var _jiraProjectId = JIRAproject_parsed.id;
            ajaxRequest(requestAPI.getProjectDetail, 'GET', {
                    company_name: company_name,
                    JIRAproject_id: _jiraProjectId
                })
                .done(function (res) {
                    deffer.resolve(res);
                })
                .catch(function (err) {
                    deffer.reject(err);
                });
        });
    return deffer.promise();
}

function paginate(array, page_size, page_number) {
    --page_number; // because pages logically start with 1, but technically with 0
    return array.slice(page_number * page_size, (page_number + 1) * page_size);
}

function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

function showNotify(strMessage, themeColor) {
    $.notify({
        message: strMessage
    }, {
        type: themeColor,
        placement: {
            from: "top",
            align: "center"
        },
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        offset: {
            y: 70,
        },
        delay: 3000,
    });
}

function showNotifyCustoms(strMessage, themeColor, align, delay) {
    $.notify({
        message: strMessage
    }, {
        type: themeColor,
        placement: {
            from: "top",
            align: align
        },
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        offset: {
            y: 100,
            x: 0
        },
        delay: delay
    });
}

function showElement(that) {
    that.show();
}

function hideElement(that) {
    that.hide();
}

function createIssueFromJira(params) {
    //this is a function to create new issue if it is not exist in database
    return ajaxRequest(requestAPI.getIssueDetail, 'POST', JSON.stringify(params));
}

function fuzzySearch(character, library) {
    var temp = library;
    var _results;
    if (character == '') {
        _results = temp;
    } else {
        var fuseSearch = new Fuse(temp, optionsFuzzySearch);
        _results = fuseSearch.search(character);
    }
    return _results;
}

function removeDuplicateObj(arr) {
    return arr.reduce(function (p, c) {
        var key = [c.keyword, c.name].join('|');
        if (p.temp.indexOf(key) === -1) {
            p.out.push(c);
            p.temp.push(key);
        }
        return p;
    }, {
        temp: [],
        out: []
    }).out;
}

function setupGherkin(element, readOnly = 'nocursor', mode = 'gherkin', lineNumbers = true) {
    return CodeMirror.fromTextArea(document.getElementById(element), {
        mode: mode,
        theme: "eclipse",
        readOnly: readOnly,
        lineNumbers: lineNumbers,
        autoRefresh: true,
        extraKeys: {
            "Ctrl-Space": "autocomplete",
            "F11": function (cm) {
                cm.setOption("fullScreen", !cm.getOption("fullScreen"));
            },
            "Esc": function (cm) {
                if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
            }
        }
    });
}

function getTemplateHtml(templateFile) {
    var result = $.get(urlTemplateRender + templateFile, function (template) {
        return template;
    });
    return result;
}

function autoCompleteFromLibs(element, sourceLibs) {
    var uiItem = null;
    element.autocomplete({
        minLength: 1,
        delay: 0,
        source: sourceLibs,
        select: function (event, ui) {
            uiItem = ui.item;
        }
    });
    return uiItem;
}

function linkedToIssue(jiraTicketKey, issueLinked, JiraTitle, JIRAType) {
    var jiraType;
    var def = $.Deferred();
    if (JIRAType == 'manual') {
        jiraType = "MANUAL-TEST";
    } else if (JIRAType == 'automation') {
        jiraType = "AUTO-TEST";
    }
    var data = {
        "globalId": "ifd-tms-" + Date.now(),
        "Relationship": "relates to",
        "object": {
            "url": projectBaseUrl + jiraTicketKey,
            // "title": issueLinked.fields.summary
            "title": jiraType + " : " + JiraTitle
        }
    };
    IFDRequest.customRequest(
            '/rest/api/latest/issue/' + issueLinked.key + '/remotelink',
            'POST', 'application/json', JSON.stringify(data))
        .then(function (res) {
            def.resolve(res);
        })
        .catch(function (error) {
            def.reject(error);
        });
    return def.promise();
}

// function linkedToBug(jiraTicketKey, issueLinked) {
//     debugger
//     var def = $.Deferred();
//     var data = {
//         "globalId": "ifd-tms-" + Date.now(),
//         "Relationship": "relates to",
//         "object": {
//             "url": projectBaseUrl + issueLinked.key,
//             "title": issueLinked.fields.summary
//         }
//     };
//     IFDRequest.customRequest(
//             '/rest/api/latest/issue/' + jiraTicketKey + '/remotelink',
//             'POST', 'application/json', JSON.stringify(data))
//         .then(function (res) {
//             debugger
//             def.resolve(res);
//         })
//         .catch(function (error) {
//             def.reject(error);
//         });
//     return def.promise();
// }

function exportManualTestcasetoExcel(tableId) {
    var table = new $.fn.DataTable.Api(tableId);
    var rows = [];
    var arrHead = ['Test Step', 'Test Data', 'Expected Result'];
    rows.push(arrHead);
    var data = table.rows().data();
    delete data.context;
    delete data.selector;
    delete data.ajax;
    var filename = "iTMS_Manual_Exported_Testcases.xlsx";
    var ws_name = "Manual-testcase";
    for (i = 0; i < data.length; i++) {
        var row = [data[i].test_step, data[i].test_data, data[i].expected_result];
        rows.push(row);
    }
    // console.info('rows', rows);
    var wb = XLSX.utils.book_new(),
        ws = XLSX.utils.aoa_to_sheet(rows);

    /* add worksheet to workbook */
    XLSX.utils.book_append_sheet(wb, ws, ws_name);
    XLSX.writeFile(wb, filename);
    //TODO: Should be export data from json which recieve from API.
}


function showNotifyCustoms(strMessage, themeColor, align, delay) {
    $.notify({
        message: strMessage
    }, {
        type: themeColor,
        placement: {
            from: "top",
            align: align
        },
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        offset: {
            y: 100,
            x: 0
        },
        delay: delay
    });
}

function showNotifyCustomsStepsScenario(strMessage, themeColor, align, delay) {
    $.notify({
        message: strMessage
    }, {
        type: themeColor,
        placement: {
            from: "top",
            align: align
        },
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        offset: {
            y: 550,
            x: 0
        },
        delay: delay
    });
}