var IFDRequest = function () {
    var customRequest = function (url, type, contentType, data) {
        return AP.request({
            url: url,
            type: type,
            data: data,
            contentType: contentType
        });
    };
    return {
        customRequest: customRequest
    };
}();

// AP.request({
//     url: '/rest/api/2/project/' + _jiraProjectId + '/versions',
//     type: 'GET',
//     contentType: 'application/json',
//     data: null,
//     success: function (responseText) {
//         console.log('request-success',responseText);
//     },
//     error: function (xhr, statusText, errorThrown) {
//         console.log(arguments);
//     }
// });