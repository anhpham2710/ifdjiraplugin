$(function () {
    var lodash = _;
    var arrTestCaseExecution = [];
    var JIRAproject_id = null;
    var JIRAproject_key = null;
    var arrJiraVersion = null;
    var usersJira = null;
    var execute_Object = {};
    var rawVersion;
   
    const topNumberRow = 3;

    AP.request('/rest/api/2/user/assignable/search?project=' + jiraProjectId, {
        success: function (res) {
            usersJira = lodash.filter(JSON.parse(res), function (o) {
                return o.emailAddress.indexOf("@connect.atlassian.com") === -1;
            });
        }
    });

    IFDRequest.customRequest('/rest/api/2/project/' + jiraProjectId + '/versions', "GET", "application/json", null)
        .then(function (response) {
            arrJiraVersion = JSON.parse(response.body);
        })
        .catch(function (er) {
            // console.log('Could not load Jira version', er);
        });

    var tblTestCaseExcution = $('#manual_testcase_execution_panel').DataTable({
        paging: false,
        searching: false,
        info: false,
        ordering: false,
        autoWidth: false,
        data: null,
        columns: [{
                data: 'version',
                className: 'td-status',
                render: function (data, type, row, meta) {
                    var versionName = lodash.find(arrJiraVersion, function (o) {
                        return o.id == data;
                    });
                    return versionName ? versionName.name : data;
                }
            },
            {
                data: 'test_cycle'
            },
            {
                data: 'status',
                className: 'td-status',
                render: function (data, type, row, meta) {
                    var color = 'badge-secondary';
                    switch (data.trim()) {
                        case 'FAIL':
                            color = 'badge-danger'; // FAIL
                            break;
                        case 'BLOCKED':
                            color = 'badge-warning'; // BLOCK
                            break;
                        case 'WIP':
                            color = 'badge-info'; // WIP
                            break;
                        case 'PASS':
                            color = 'badge-success'; // PASS
                            break;
                        default:
                            break;
                    }
                    return '<span class="btn badge badge-pill ' + color + ' change-status btn-block">' + data + '</span>';
                }
            },
          
            {
                data: 'defects',
                width: '195px',
                "render": function (data, type, row, meta) {                
                    var tmpl = '';
                    $.each(data, function () {
                        tmpl += '<a class="btn ' + this.statusColor + ' badge badge-pill badge-secondary mr-1"  data-toggle="tooltip"  title="' + this.sumaryAndName + '" target="_blank" href=' + projectBaseUrl + this.value + '>' + this.value + '</a>';
                    });
                    return '<div class="inner-td">' + tmpl + '</div>';
                }
            },


            {
                // data: 'executed_by'
                data: function (row, type, val, meta) {
                    if (row !== null && row.executed_by !== null) {
                        var _user = lodash.find(usersJira, function (o) {
                            return o.emailAddress == row.executed_by;
                        });
                        if (typeof _user !== 'undefined') {
                            return _user.displayName;
                        }
                    }
                    return row.executed_by;
                }
            },
            {
                // data: 'executed_on'
                data: function (row, type, val, meta) {
                    if (row.executed_on === row.created_at) {
                        return '';
                    }
                    return row.executed_on ? moment(row.executed_on).format('DD/MM/YYYY') : '';
                }
            },
            //     {
            //         data: null,
            //         orderable: false,
            //         width: 115,
            //         render: function (data, type, row, meta) {
            //             var param = {execution_type:'debug', action_tupe: 'status'};
            //             return '<fieldset class="group-border">' +
            //             '<div class="row" id="'+data.plan_id+'">'+
            //             '<div class="col-md-12">'+
            //                 '<button type="button" class="btn btn-custom-1 btn-block confirm_exec_debug debug_btn" action_type="execute" title="Execute"><span class="fa fa-bug"></span></button>'+
            //                 '<button type="button" class="btn btn-custom-1 btn-block get_status_debug debug_btn" action_type="status" title="Get Status"><span class="oi" data-glyph="info"></span></button>'+
            //                 '<button type="button" class="btn btn-custom-1 btn-block get_log_debug debug_btn" action_type="log" title="Get Log"><span class="oi" data-glyph="monitor"></span></button>'+
            //                 '<button type="button" class="btn btn-custom-1 btn-block confirm_stop_debug debug_btn" action_type="stop" title="Stop" data-toggle="modal" data-target="#modalConfirmStop"><span class="oi" data-glyph="ban"></span></button>'+
            //             '</div>'+
            //         '</div>'+
            //    '</fieldset>';
            //         }
            //     },
            {
                data: null,
                width: 32,
                orderable: false,
                render: function (data, type, row, meta) {
                    return '<a class="btn btn-link py-1 px-2" href=' + baseUrl + '/plugins/servlet/ac/Infodation-tms/auto-execution?project.key=' + JIRAproject_key +
                        '&ac.projectId=' + JIRAproject_id +
                        '&ac.issueId=' + data.issue_id +
                        '&ac.cycleId=' + data.cycle_id +
                        '&ac.cycleName=' + escape(data.test_cycle) +
                        '&ac.planId=' + data.plan_id + ' target="_blank"><span class="oi" data-glyph="play-circle"></span></a>';
                }
            }
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).find(".debug_btn").on('click', function () {

                var id = $(nRow).find(".group-border .row").attr('id');
                var url = requestURL + '/execution_automation/' + id;
                var type = $(this).attr('action_type');
                var data = {
                    'execution_type': 'debug',
                    'action_type': type
                };
                var method = 'GET';
                if (type == 'stop') {
                    method = 'POST';
                    url = requestURL + '/execution_automation/' + id + '?execution_type=debug&action_type=' + type;
                    data = '';
                }
                var that = this;
                if (type != 'execute') {
                    if ($(that).attr('disabled') == 'disabled') {
                        return false;
                    } else {
                        $(that).attr({
                            'disabled': 'disabled'
                        });
                    }
                    if (type == 'stop') {
                        $('#modalConfirmStop').modal('show');
                        $('#modalConfirmStop .modal-body').html('<p style="padding-left:5px; margin-bottom:0;">Do you want to stop?</p>');
                        $('#stopSure').attr({
                            'data-id': id
                        });
                        $(that).removeAttr('disabled');
                    } else {
                        getDebugActionResult(url, data, method, that, type);
                    }
                } else {
                    method = 'POST';
                    url = requestURL + '/execution_automation/' + id + '?execution_type=debug&action_type=' + type;
                    data = '';
                    method1 = 'GET';
                    var url1 = requestURL + '/execution_automation/' + id;
                    var data1 = {
                        'execution_type': 'debug',
                        'action_type': 'status'
                    };
                    showSpinner(that);
                    ajaxRequest(url1, method1, data1)
                        .done(function (res) {
                            hideSpinner(that);
                            if (res.message != 'running') {
                                execute_Object.url = url;
                                execute_Object.method = method;
                                $('#modal-exec-info').modal('show');
                                $('#exec_debug').attr({
                                    'data-id': id
                                });
                            } else {
                                alert('is running!');
                            }
                        })
                        .catch(function (err) {
                            hideSpinner(element);
                            // console.log(err);
                            $('#refresh-data .fas').removeClass('fa-spin');
                        });
                }

            });
            return nRow;
        },
    });

    getProjectDetailTestCase();

    $('#refresh-data').on('click', function () {
        getProjectDetailTestCase();
    });

    $('#exec_debug').on('click', function () {
        var id = $(this).data('id');
        var browser_mode = $('#modal-exec-info').find('input[name=browser]:checked').val();
        var wait_time = $('#wait_time').val();
        var print_logs = $('#print_logs').val();
        var url = execute_Object.url;
        url = url + '&browser=' + browser_mode + '&wait_time=' + wait_time + '&print_log=' + print_logs;
        var that = $('.row[id="' + id + '"] button[action_type="execute"]');
        getDebugActionResult(url, '', execute_Object.method, that);
        $('#modal-exec-info').modal('hide');
        //alert(browser_mode + '/' + wait_time + '/' + print_logs);
    });
    $('#cancel-exec-debug').on('click', function () {
        $('#modal-exec-info').modal('hide');
    });

    $('#stopSure').on('click', function () {
        var id = $(this).data('id');
        var type = 'stop';
        var url = requestURL + '/execution_automation/' + id + '?execution_type=debug&action_type=' + type;
        var method = 'POST';
        var data = "";
        var that = $('.row[id="' + id + '"] button[action_type="stop"]');
        $('#modalConfirmStop').modal('hide');
        getDebugActionResult(url, data, method, that, type);
    });

    function getIssueDetail() {
        ajaxRequest(requestAPI.getIssueDetail, 'GET', {
                project_id: project_id,
                JIRAticket_key: issueKey
            }).done(function (res) {
                var _issue_id = res[0].id;
                ajaxRequest(requestAPI.getTestExecution, 'GET', {
                        issue_id: _issue_id
                    }).done(function (res) { 
                        var mainGetIssuesDefferAuto = [];
                        var mainGetIssuesDeffer = [];
                        arrTestCaseExecution = [];
                            $.each(res, function () {
                                var deff = $.Deferred();
                                mainGetIssuesDeffer.push(deff.promise());
                                var plan = this;
                                // get cycle detail
                                ajaxRequest(requestAPI.getCycleDetail + '/' + plan.cycle_id, 'GET', null).done(function (res) {
                                        var testCaseExecution = {
                                            version: res.JIRAversion,
                                            test_cycle: res.name,
                                            status: plan.status,
                                            defects: plan.defects,
                                            // executed_by: plan.modified_by,
                                            executed_by: plan.executed_by,
                                            created_at: plan.created_at,
                                            executed_on: plan.updated_at,
                                            issue_id: plan.issue_id,
                                            cycle_id: plan.cycle_id,
                                            plan_id: plan.id
                                        };
                                        arrTestCaseExecution.push(testCaseExecution);
                                        deff.resolve();
                                    })
                                    .catch(function () {
                                        deff.resolve();
                                    });
                            });
                            // console.log("testCaseExecution testCaseExecution",arrTestCaseExecution);
                            $.when.apply($,mainGetIssuesDeffer).done(function(){
                                $.each(arrTestCaseExecution, function (i, val) {
                                    var mainDeferGetDefectDetail = [];
                                    var arrDefect = [];
                                    var _defects = val.defects != null ? val.defects.split('|') : [];
                                    $.each(_defects, function (i, value) {
                                        var def = $.Deferred();
                                        mainDeferGetDefectDetail.push(def.promise());
                                        mainGetIssuesDefferAuto.push(def.promise());
                                        AP.request(baseUrl + '/rest/api/2/search?jql=issue=\'' + value + '\'&fields=summary,status', {
                                            success: function (res) {
                                                var detailDefect = {
                                                    statusColor: "",
                                                    sumaryAndName: '',
                                                    value: '',
                                                };
                                                var datasource = JSON.parse(res);
                                                var issue = datasource && datasource["issues"].length && datasource["issues"]["0"];
                                                var status = issue && issue.fields ? issue.fields.status : undefined;
                                                var summaryTextBug = issue.fields.summary;
                                                var statusColor;
                                                sumaryAndName = 'Summary : ' + summaryTextBug.toString() + "&#013;" + 'Status : ' + status.name.toString();
                
                                                colorStatusBug = status.name;
                                                if (colorStatusBug == "To Do") {
                                                    statusColor = "statusToDoColor";
                                                } else if (colorStatusBug == "Done") {
                                                    statusColor = "statusDoneColor";
                                                } else {
                                                    statusColor = "statusInProgressColor";
                                                }
                
                                                detailDefect.statusColor = statusColor;
                                                detailDefect.sumaryAndName = sumaryAndName;
                                                detailDefect.value = value;
                                                arrDefect.push(detailDefect);
                                                def.resolve();
                                            },
                                            error: function () {
                                                def.resolve();
                                            }
                                        });
                                    });
                
                                    $.when.apply($, mainDeferGetDefectDetail).done(function () {
                                        arrTestCaseExecution[i].defects = arrDefect;
                                    });
                                });
                                $.when.apply($, mainGetIssuesDefferAuto).done(function () {
                                    setTimeout(function () {
                                        tblTestCaseExcution.clear();
                                        tblTestCaseExcution.rows.add(arrTestCaseExecution).draw();
                                        $('#refresh-data .fas').removeClass('fa-spin');
                                    }, 500);
                                });
                            });                          
                    })
                    .catch(function (err) {
                        $('#refresh-data .fas').removeClass('fa-spin');
                    });
            })
            .catch(function (err) {
                $('#refresh-data .fas').removeClass('fa-spin');
            });
    }

    function getProjectDetailTestCase() {
        $('#refresh-data .fas').addClass('fa-spin');
        getProjectDetail()
            .done(function (res) {
                var project_response = res[0];
                project_id = project_response.id;
                JIRAproject_id = project_response.JIRAproject_id;
                JIRAproject_key = project_response.JIRAproject_key;
                if (issueKey == null) {
                    AP.request('/rest/api/2/issue/' + issueKey, {
                        success: function (res) {
                            issueKey = resData.key;
                            getIssueDetail();
                        }
                    });
                } else {
                    getIssueDetail();
                }
            });
    }

    function getDebugActionResult(url, param, method, element, type) {
        // console.log('ele12 ', element);
        showSpinner(element);
        ajaxRequest(url, method, param).done(function (res) {
                $(element).removeAttr('disabled');
                hideSpinner(element);
                if (type) {
                    if (type == 'log' && res.message != 'This case has not been triggered for automation yet!') {
                        // getReport(res.message);
                        showLog(res.message, type);
                    } else {
                        showNotify(res.message, "custom-1")
                    }
                } else {
                    showNotify(res.message, "custom-1")
                }
                // if (type) {
                //     showLog(res.message, type);
                // } else {
                //     $('#modal-exec-info').modal('hide');
                //     alert(res.message);
                // }
            })
            .catch(function (err) {
                $(element).removeAttr('disabled');
                hideSpinner(element);
                // console.log(err);
                $('#refresh-data .fas').removeClass('fa-spin');
            });
    }

    function showLog(logContent, type) {
        $('.CodeMirror-code').html('');
        var title = 'Log';
        if (type == 'status') {
            title = 'Status';
        } else if (type == 'stop') {
            title = 'Stop';
        }
        $('#modal-show-log').find('.modal-title').html(title);
        $('#modal-show-log').addClass('full-width');
        $('#modal-show-log').modal('show');
        rawVersion = CodeMirror.fromTextArea(document.getElementById("txt-show-log"), {
            lineNumbers: false,
            mode: "text/javascript",
            readOnly: "nocursor",
            theme: "eclipse",
            extraKeys: {
                "Ctrl-Space": "autocomplete"
            }
        });
        setTimeout(function () {
            rawVersion.setValue(logContent);
        }, 700);
    }

    function showSpinner(that) {
        $(spinner).hide();
        $(that).append(spinner);
        $(that).find('.spinner').fadeIn();
    }

    function hideSpinner(that) {
        $(that).find('.spinner').fadeOut("slow", function () {
            $(that).find('.spinner').remove();
        });
    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
});