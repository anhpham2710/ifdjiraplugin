$(function () {
    $('.jiraSite').val(hostBaseUrl);

    $('.tab-pane input').on('keyup', function () {
        if ($(this).val() === '') {
            $(this).addClass('error');
        } else {
            $(this).removeClass('error');
        }
    });

    $('.select-file-import input').click(function () {
        var that = $(this);
        if (that.hasClass('excelfile')) {
            showImporterExcel(true, that);
        } else {
            showImporterExcel(false, that);
        }
    })

    $('#submitFormManual').on('click', function (e) {
        $.notifyClose();
        if (requiredInput($(this))) {
            showHideSpiner(true);
            importAllValueManual();
        }
    });

    $('#submitFormAuto').on('click', function (e) {
        $.notifyClose();
        if (requiredInput($(this))) {
            showHideSpiner(true);
            importAllValueAuto();
        }
    });

    $('#submitFormStepsDefinition').on('click', function (e) {
        $.notifyClose();
        if (requiredInput($(this))) {
            showHideSpiner(true);
            importAllValueStepsDefinition();
        }
    });

    $('#submitFormGlobalData').on('click', function (e) {
        $.notifyClose();
        if ($('#excelFileGlobal').is(':checked')) {
            if (requiredInput($(this))) {
                showHideSpiner(true);
                importAllValueGlobalData();
            }
        } else {
            if ($('#fileImportGlobalData').val() != '') {
                showHideSpiner(true);
                importAllValueGlobalData();
            } else {
                $('#fileImportGlobalData').addClass('error');
            }
        }
    });

    $('#submitFormObjectRepo').on('click', function (e) {
        $.notifyClose();
        if ($('#excelFileObj').is(':checked')) {
            if (requiredInput($(this))) {
                showHideSpiner(true);
                importAllValueObjectRepo();
            }
        } else {
            if ($('#fileImportObjectRepo').val() != '') {
                showHideSpiner(true);
                importAllValueObjectRepo();
            } else {
                $('#fileImportObjectRepo').addClass('error');
            }
        }
    });
});

function showHideSpiner(mode) {
    if (mode) {
        $('#body-loading-bar').show();
    } else {
        $('#body-loading-bar').hide();
    }
}

function importAllValueManual() {
    var fileImport = $('#fileImport')[0].files[0];
    var sheetName = $('#sheetName').val();
    var testCaseName = $('#testCaseName').val();
    var jiraTicketKey = $('#jiraTicketKey').val();
    var testStep = $('#testStep').val();
    var testData = $('#testData').val();
    var exceptedResult = $('#exceptedResult').val();
    var userName = $('#userName').val();
    var password = $('#password').val();
    var jiraSite = hostBaseUrl;

    var formData = new FormData();
    formData.append('datafile', fileImport);

    formData.append('jira_site', jiraSite);
    formData.append('jira_username', userName);
    formData.append('jira_password', password);
    formData.append('sheet_name', sheetName);
    formData.append('excel_column_ticket_key', jiraTicketKey);
    formData.append('excel_column_testcase_name', testCaseName);
    formData.append('excel_column_test_step', testStep);
    formData.append('excel_column_test_data', testData);
    formData.append('excel_column_expected_result', exceptedResult);
    formData.append('project_key', projectKey);

    var dataIm = {
        datafile: formData,
        jira_site: jiraSite,
        jira_username: userName,
        jira_password: password,
        sheet_name: sheetName,
        excel_column_ticket_key: jiraTicketKey,
        excel_column_testcase_name: testCaseName,
        excel_column_test_step: testStep,
        excel_column_test_data: testData,
        excel_column_expected_result: exceptedResult,
        project_key: projectKey
    }

    ajaxRequestImport(requestURL + '/import/manual_testcase', 'POST', formData).done(function (res) {
        showHideSpiner(false);
        if (jQuery.type(res.message) === "string") {
            showNotifyCustoms(res.message, 'success', 'center', 3000);
            $('#manualImporter input[type="file"]').val('');
        } else {
            var htmlImport = '';
            htmlImport += '<tr><td class="header-table">Test Case Name</td>' +
                '<td class="header-table">Test Step</td><td class="header-table">Test Data</td>' +
                '<td class="header-table">Excepted Result</td></tr>';
            $.each(res.message, function (index, value) {
                htmlImport += '<tr>';
                $.each(value[0], function (key, data) {
                    htmlImport += '<td>' + key + '</td>';
                    htmlImport += '<td>' + data[0].test_step + '</td>';
                    htmlImport += '<td>' + data[0].test_data + '</td>';
                    htmlImport += '<td>' + data[0].expected_result + '</td>';
                });
                htmlImport += '</tr>';
            });
            $('#modalImportSuccess .modal-body table').html(htmlImport);
            $('#modalImportSuccess').modal('show');
            $('#manualImporter input').val('');
            $('.jiraSite').val(hostBaseUrl);
        }
    }).catch(function (err) {
        showHideSpiner(false);
        showNotifyCustoms(err.responseJSON.error, 'danger', 'right', 0);
    });
}

function importAllValueAuto() {
    var fileImportAuto = $('#fileImportAuto')[0].files[0];
    var sheetNameAuto = $('#sheetNameAuto').val();
    var featureAuto = $('#featureAuto').val();
    var jiraTicketKeyAuto = $('#jiraTicketKeyAuto').val();
    var userNameAuto = $('#userNameAuto').val();
    var passwordAuto = $('#passwordAuto').val();
    var jiraSite = hostBaseUrl;

    var formData = new FormData();
    formData.append('datafile', fileImportAuto);

    formData.append('jira_site', jiraSite);
    formData.append('jira_username', userNameAuto);
    formData.append('jira_password', passwordAuto);
    formData.append('sheet_name', sheetNameAuto);
    formData.append('excel_column_ticket_key', jiraTicketKeyAuto);
    formData.append('excel_column_feature', featureAuto);
    formData.append('project_key', projectKey);

    ajaxRequestImport(requestURL + '/import/auto_testcase', 'POST', formData).done(function (res) {
        showHideSpiner(false);
        if (jQuery.type(res.message) === "string") {
            showNotifyCustoms(res.message, 'success', 'center', 3000);
            $('#fileImportAuto input[type="file"]').val('');
        } else {
            var htmlImport = '';
            htmlImport += '<tr><td class="header-table">Feature Name</td></tr>';
            $.each(res.message, function (index, value) {
                htmlImport += '<tr>';
                $.each(value[0], function (key, data) {
                    htmlImport += '<td>' + key + '</td>';
                });
                htmlImport += '</tr>';
            });
            $('#modalImportSuccess .modal-body table').html(htmlImport);
            $('#modalImportSuccess').modal('show');
            $('#automationImporter input').val('');
            $('.jiraSite').val(hostBaseUrl);
        }
    }).catch(function (err) {
        showHideSpiner(false);
        showNotifyCustoms(err.responseJSON.error, 'danger', 'right', 0);
    });
}

function importAllValueStepsDefinition() {
    var fileImportStepsDefinition = $('#fileImportStepsDefinition')[0].files[0];
    var sheetNameStepsDefinition = $('#sheetNameStepsDefinition').val();
    var nameStepsDefinition = $('#nameStepsDefinition').val();
    var contentStepsDefinition = $('#contentStepsDefinition').val();

    var formData = new FormData();
    formData.append('datafile', fileImportStepsDefinition);

    formData.append('sheet_name', sheetNameStepsDefinition);
    formData.append('excel_column_name', nameStepsDefinition);
    formData.append('excel_column_content', contentStepsDefinition);
    formData.append('project_key', projectKey);

    ajaxRequestImport(requestURL + '/import/steps_definition', 'POST', formData).done(function (res) {
        showHideSpiner(false);
        if (jQuery.type(res.message) === "string") {
            showNotifyCustoms(res.message, 'success', 'center', 3000);
            $('#fileImportStepsDefinition input[type="file"]').val('');
        } else {
            var htmlImport = '';
            htmlImport += '<tr><td class="header-table">Steps Definition Name</td></tr>';
            $.each(res.message, function (index, value) {
                htmlImport += '<tr>';
                htmlImport += '<td>' + value.name + '</td>';
                htmlImport += '</tr>';
            });
            $('#modalImportSuccess .modal-body table').html(htmlImport);
            $('#modalImportSuccess').modal('show');
            $('#stepsDefinition input').val('');
            $('.jiraSite').val(hostBaseUrl);
        }
    }).catch(function (err) {
        showHideSpiner(false);
        showNotifyCustoms(err.responseJSON.error, 'danger', 'right', 0);
    });
}

function importAllValueGlobalData() {
    var fileImportGlobalData = $('#fileImportGlobalData')[0].files[0];
    var sheetNameGlobalData = $('#sheetNameGlobalData').val();
    var nameGlobalData = $('#nameGlobalData').val();
    var contentGlobalData = $('#contentGlobalData').val();

    var formData = new FormData();
    formData.append('datafile', fileImportGlobalData);

    formData.append('sheet_name', sheetNameGlobalData);
    formData.append('excel_column_name', nameGlobalData);
    formData.append('excel_column_content', contentGlobalData);
    formData.append('project_key', projectKey);

    ajaxRequestImport(requestURL + '/import/data_global', 'POST', formData).done(function (res) {
        showHideSpiner(false);
        if (jQuery.type(res.message) === "string") {
            showNotifyCustoms(res.message, 'success', 'center', 3000);
            $('#fileImportGlobalData input[type="file"]').val('');
        } else {
            var htmlImport = '';
            htmlImport += '<tr><td class="header-table">Name</td><td class="header-table">Content</td></tr>';
            $.each(res.message, function (index, value) {
                htmlImport += '<tr>';
                htmlImport += '<td>' + value.name + '</td>';
                htmlImport += '<td>' + value.content + '</td>';
                htmlImport += '</tr>';
            });
            $('#modalImportSuccess .modal-body table').html(htmlImport);
            $('#modalImportSuccess').modal('show');
            $('#globalData .form-group input').val('');
            $('.jiraSite').val(hostBaseUrl);
        }
    }).catch(function (err) {
        showHideSpiner(false);
        showNotifyCustoms(err.responseJSON.error, 'danger', 'right', 0);
    });
}

function importAllValueObjectRepo() {
    var fileImportObjectRepo = $('#fileImportObjectRepo')[0].files[0];
    var sheetNameObjectRepo = $('#sheetNameObjectRepo').val();
    var nameObjectRepo = $('#nameObjectRepo').val();
    var contentObjectRepo = $('#contentObjectRepo').val();

    var formData = new FormData();
    formData.append('datafile', fileImportObjectRepo);

    formData.append('sheet_name', sheetNameObjectRepo);
    formData.append('excel_column_name', nameObjectRepo);
    formData.append('excel_column_content', contentObjectRepo);
    formData.append('project_key', projectKey);

    ajaxRequestImport(requestURL + '/import/object_repo', 'POST', formData).done(function (res) {
        showHideSpiner(false);
        if (jQuery.type(res.message) === "string") {
            showNotifyCustoms(res.message, 'success', 'center', 3000);
            $('#fileImportObjectRepo input[type="file"]').val('');
        } else {
            var htmlImport = '';
            htmlImport += '<tr><td class="header-table">Object Repository Name</td></tr>';
            $.each(res.message, function (index, value) {
                htmlImport += '<tr>';
                htmlImport += '<td>' + value.name + '</td>';
                htmlImport += '</tr>';
            });
            $('#modalImportSuccess .modal-body table').html(htmlImport);
            $('#modalImportSuccess').modal('show');
            $('#objectRepo .form-group input').val('');
            $('.jiraSite').val(hostBaseUrl);
        }
    }).catch(function (err) {
        showHideSpiner(false);
        showNotifyCustoms(err.responseJSON.error, 'danger', 'right', 0);
    });
}


function requiredInput(that) {
    that.closest('.tab-pane').find('input').each(function () {
        if ($(this).val() === '') {
            $(this).addClass('error');
            a = false;
        } else {
            $(this).removeClass('error');
        }
    });
    if (that.closest('.tab-pane').find('input.error').length > 0) {
        return false;
    } else {
        return true;
    }
}

function showImporterExcel(mode, that) {
    that.closest('fieldset').find('input').removeClass('error');
    if (mode) {
        that.closest('fieldset').find('input[type="file"]').attr('accept', '.xlsx').val('');
        that.closest('fieldset').find('.importer-excel-file').show();
    } else {
        that.closest('fieldset').find('input[type="file"]').attr('accept', '.yml').val('');
        that.closest('fieldset').find('.importer-excel-file').hide();
    }
}