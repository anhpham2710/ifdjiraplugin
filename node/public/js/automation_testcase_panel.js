var currentUser = null;
var userId = paramsUrl.user_id;
var userKey = paramsUrl.user_key;

$(function () {
    $('.jiraSite').val(hostBaseUrl);
    AP.request('/rest/api/2/myself', {
        success: function (res) {
            currentUser = JSON.parse(res);
        }
    });

    rawVersion = setupGherkin('text-area-raw-version');

    init();

    function loadFeature() {
        $('#refresh-data span').addClass('fa-spin');
        ajaxRequest(requestAPI.autoTestCase, "GET", {issue_id: issueId}).done(function (res) {
            var responseAutoData = res[0];
            rawVersion.setValue(responseAutoData.gherkin_content);
            rawVersion.save();
            showSpinner(false);
            afterShowData();
            $('#refresh-data span').removeClass('fa-spin');
        }).catch(function (err) {
            $('#screenRawVersionAll').hide();
            ('#refresh-data span').removeClass('fa-spin');
            showSpinner(false);
        });
    }

    function init() {
        issueId = $('#issueId').val();
        if($('#text-area-raw-version').val()=== ''){
            $('#screenRawVersionAll').hide();
        } else{
            afterShowData();
        }
        $('#refresh-data span').removeClass('fa-spin');
        showSpinner(false);

        // getProjectDetail(IFDRequest, projectKey).done(function (res) {
        //     var project = res[0];
        //     console.log('res ', res);
        //     project_id = project.id;
        //     ajaxRequest(requestAPI.getIssueDetail, 'GET', {project_id: project.id, JIRAticket_key: issueKey}).done(function (res) {
        //     }).catch(function(err){
                // if (err.status == 404) {
                //     var params = {
                //         "JIRAticket_id": project.JIRAproject_id,
                //         "JIRAticket_key": project.JIRAproject_key,
                //         "JIRAticket_title": jiraTicketTitle,
                //         "project_id": project.id,
                //         "created_by": userId,
                //         "type_id": 2
                //     }
                //     createIssueFromJira(params).done(function (res) {
                //         issueId = res.id;
                //         showHidePanelSelectTestCase(false, true);
                //         showSpinner(false);
                //     });
                // }
        //     });
        // });    
    }

    function showSpinner(show){
        if(show){
            $('#spinnerScenario').fadeIn();
        } else{
            $('#spinnerScenario').fadeOut();
        }
    }

    var tooltip = '';
    tooltip += '<div>When cursor is in the editor press:<div>'+
    '<div>F11 to full screen</div>'+
    '<div>Esc to exit full screen</div>'+
    '<div>Ctrl-F / Cmd-F to search</div>'+
    '<div>Ctrl-G / Cmd-G to find next</div>'+
    '<div>Shift-Ctrl-G / Shift-Cmd-G to find previous</div>'+
    '<div>Shift-Ctrl-F / Cmd-Option-F to replace</div>'+
    '<div>Shift-Ctrl-R / Shift-Cmd-Option-F to replace all</div>';
    $('#tooltip-guides').attr('title', tooltip);
    $('#tooltip-guides').tooltip();

    function showImportFile(){
        $('#form-import').show();
        $('#body-loading-bar').hide();
    }

    function showHideSpiner(mode){
        if(mode){
            $('#body-loading-bar').show();
            $('#form-import').hide();
        } else{
            $('#form-import').show();
            $('#body-loading-bar').hide();
        }
    }

    function requiredInput(that){
        that.closest('.tab-pane').find('input').each(function(){
            if($(this).val()===''){
                $(this).addClass('error');
                a = false;
            } else{
                $(this).removeClass('error');
            }
        });
        if(that.closest('.tab-pane').find('input.error').length>0){
            return false;
        } else{
            return true;
        }
    }

    function afterShowData(){
        $('#screenRawVersionAll').show();
        $('.button-switch-version').find('.tooltip-guides').show();
        $('.button-switch-version').find('#import-file').hide();
        $('#button-action-top').hide();
        $('#button-action-top').find('.placeholder-section__feature').hide();
        $('#button-action-top').find('.tab-pane').hide();
        $('#body-loading-bar').hide();
    }

    function importAllValueAuto(){
        var uploadFile = $('#uploadFile')[0].files[0];
        var formData = new FormData();
        formData.append('datafile', uploadFile);
        formData.append('issue_id', issueId);
        ajaxRequestImport(requestURL + '/import/auto_testcase', 'POST', formData).done(function(res){
            ajaxRequest(requestAPI.autoTestCase, "GET", {issue_id: issueId}).done(function (res) {
                var responseAutoData = res[0];
                rawVersion.setValue(responseAutoData.gherkin_content);
                rawVersion.save();
                showSpinner(false);
            });
            afterShowData();
            if(jQuery.type(res.message)==="string"){
                showNotifyCustoms('Import Success', 'success', 'center', 3000);
                $('#uploadFile input[type="file"]').val('');
            } else{
                $('#form-import input').val('');
                $('.jiraSite').val(hostBaseUrl);
            }
        }).catch(function(err){
            showHideSpiner(false);
            showNotifyCustoms(err.responseJSON.error, 'danger', 'right', 0);
        });
    }

    $('#import-file .fa-cloud-upload-alt').on('click', function(){
        showImportFile();
    });

    $('#submitAuto').on('click', function(e){
        $.notifyClose();
        if(requiredInput($(this))){
            showHideSpiner(true);
            importAllValueAuto();
        }
    });

    $('#refresh-data').click(function () {
        loadFeature();
    });

});
