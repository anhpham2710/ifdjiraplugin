/// <reference path="./common/common.js" />
var arrIssueSelected = [];
var lodash = _;
$(function () {
    // get Project Cycle
    IFDRequest.customRequest('/rest/api/2/project/' + projectKey, "GET", "application/json", null)
        .then(function (response) {
            JIRAproject_parsed = JSON.parse(response.body);
            _jiraProjectId = JIRAproject_parsed.id;
            ajaxRequest(requestURL + '/project?company_name=' + company_name + '&JIRAproject_id=' + _jiraProjectId, "GET", null)
                .done(function (project_response) {            
                    project_id = project_response[0].id;
                    _jiraProjectId = project_response[0].JIRAproject_id;
                    JIRAproject_key = project_response[0].JIRAproject_key;
                    ajaxRequest(requestURL + '/cycle?project_id=' + project_id + '', "GET", null)
                        .done(function (testCycleData) {   
                            if (testCycleData.length > 0) {
                                $('#project_cycle option').remove();
                                $('#project_cycle').removeAttr('disabled');
                                // var cycleName = $('<optgroup label="Select Test Cycle"></optgroup>');
                                $.each(testCycleData, function () {
                                    var option = '<option value="' + this.id + '">' + this.name + '</option>';
                                    // cycleName.append(option);
                                    $('#selectCycle').append(option);
                                });
                                // $('#project_cycle').append(cycleName);
                            }
                            $('#add-test-cycle').removeAttr('disabled');
                        })
                        .catch(function (er) {
                            console.log('Could not load cycles data', er);
                            $('#project_cycle').html('<option>No cycles found</option>');
                        });
                });
        });

    $('#cancel-test-cycle').on('click', function () {
        closeDialog();
    });

    // get project detail
    getProjectDetail().done(function (res) {
        var project = res[0];
        // get issue detail
        project_id = project.id;

        ajaxRequest(requestAPI.getIssueDetail, 'GET', {
            project_id: project_id,
            JIRAticket_key: issueKey
        }).done(function (res) {
            arrIssueSelected.push(res[0].id);
        });
    });

    // get user from JIRA
    IFDRequest.customRequest('/rest/api/2/user/assignable/search?project=' + projectKey, 'GET', 'application/json', null).then(function (res) {
        userJira = lodash.filter(JSON.parse(res.body), function (o) {
            return o.emailAddress.indexOf("@connect.atlassian.com") === -1;
        });
        $('#project_user option').remove();
        $('#project_user').removeAttr('disabled');
        $.each(userJira, function () {
            var that = this;
            var optionTag = '<option value=' + that.emailAddress + '>' + that.displayName + '</option>';
            $('#project_user').append(optionTag);
        });
    });

    $('#add-test-cycle').on('click', function () {
        var projectCycleId = $('#project_cycle').val();
        if (arrIssueSelected.length > 0) {
            var data = {
                "issue_ids": arrIssueSelected
            };
            $('#add-test-cycle').append(spinner);
            ajaxRequest(requestURL + '/plan/add_issues/' + projectCycleId, "POST", JSON.stringify(data))
                .done(function (res) {
                    $('#add-test-cycle').find('.spinner').remove();
                    // closeDialog();
                    if ($('#project_user').val() !== '') {
                        updatePlanTestCase(projectCycleId, res[0].issue_id, {
                                assigned_to: $('#project_user').val()
                            }).done(function (res) {
                                closeDialog();
                            })
                            .catch(function (err) {
                                $('#error').html(err.error);
                            });
                    } else {
                        closeDialog();
                    }
                })
                .catch(function (err) {
                    console.log('error', err);
                    $('#error').html(err.responseJSON.error);
                    $('#add-test-cycle').find('.spinner').remove();
                });
        }
    });
});

function closeDialog() {
    AP.require(['events', 'dialog'], function (events, dialog) {
        dialog.close();
        events.emit('custom-event');
    });
}