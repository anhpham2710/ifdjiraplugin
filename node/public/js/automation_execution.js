var lodash = _;
var issueId = paramsUrl.issueId;
var cycleId = paramsUrl.cycleId;
var cycleName = paramsUrl.cycleName;
var planId = paramsUrl.planId;
var flagUpdatePlanComment = false;
var oldContentPlanComment = '';
var testExecution = null;
var log = null;
var execute_Object = {};
var JIRAticket_key = null;
var statusSumaryBug = [];
var JiraTitle;
var JIRAType;
$("#projectKey").html(projectKey).attr("href", projectBaseUrl + projectKey);
$("#cycleName").html(cycleName);
$(function () {
    ajaxRequest(requestURL + '/issue/' + issueId, 'GET', null).done(function (response) {
        // console.log("response response ",response);
        JIRAticket_key = response.JIRAticket_key;
        JiraTitle =response.JIRAticket_title;
        JIRAType = response.type;
        $("#issueKey").html('<a target="_blank" href="' + projectBaseUrl + JIRAticket_key + '">' + JIRAticket_key + '</a>');
    });

    var automationExecutionHistoryTable = $('#automation-execution-history-table').DataTable({
        data: null,
        columns: [{
                data: 'field_name'
            },
            {
                data: 'original_value'
            },
            {
                data: 'new_value'
            }
        ]
    });

    $('#comment').on('keypress', function () {
        flagUpdatePlanComment = true;
    });

    $('#comment').on('blur', function () {
        if (flagUpdatePlanComment) {
            flagUpdatePlanComment = false;
        } else {
            return;
        }
        if ($.trim($(this).val()) !== $.trim(oldContentPlanComment)) {
            $(this).parents('.form-group').find('label').append(spinner);
            var that = this;
            $(that).attr('disabled', true);
            updatePlanTestCase(cycleId, issueId, {
                comment: $(this).val()
            }).done(function (res) {
                $(that).parents('.form-group').find('.spinner').remove();
                oldContentPlanComment = res.comment;
                flagUpdatePlanComment = false;
                $(that).removeAttr('disabled');
            });
        }
    });



    $('#drpStatus').on('change', function () {
        $(this).parents('.form-group').find('label').append(spinner);
        var that = this;
        $(that).attr('disabled', true);
        updatePlanTestCase(cycleId, issueId, {
            status_id: $(this).val()
        }).done(function () {
            $(that).parents('.form-group').find('.spinner').remove();
            $(that).removeAttr('disabled');
        });
    });

    var urlExecution = requestURL + '/execution_automation/' + planId;

    $("#exec_test").click(function () {
        var browser_mode = $('#modal-exec-info').find('input[name=browser]:checked').val();
        var wait_time = $('#wait_time').val();
        var print_logs = $('#print_logs').val();
        var url = execute_Object.url;
        url = url + '&browser=' + browser_mode + '&wait_time=' + wait_time + '&print_log=' + print_logs;
        var that = this;
        getDebugActionResult(url, '', execute_Object.method, that);
    });

    $('#cancel-exec-test').on('click', function () {
        $('#modal-exec-info').modal('hide');
    });

    $('#stopSure').on('click', function () {
        var id = $(this).data('id');
        var type = 'stop';
        var url = requestURL + '/execution_automation/' + id + '?execution_type=debug&action_type=' + type;
        var method = 'POST';
        var data = "";
        var that = $('button[action_type="stop"]');
        $('#modalConfirmStop').modal('hide');
        getDebugActionResult(url, data, method, that, type);
    });

    function getDebugActionResult(url, param, method, element, type) {
        showSpinner(element);
        ajaxRequest(url, method, param).done(function (res) {
            $(element).removeAttr('disabled');
            hideSpinner(element);
            if (type) {
                // if (type == 'log' && res.message != 'This case has not been triggered for automation yet!'){
                // getReport(res.message);
                if (type == 'log') {
                    getReportTest();
                } else {
                    showNotifyCustoms(res.message, "custom-1", "center", 3000);
                }
            } else {
                $('#modal-exec-info').modal('hide');
                showNotifyCustoms(res.message, "custom-1", "center", 3000)
            }
        }).catch(function (err) {
            $(element).removeAttr('disabled');
            hideSpinner(element);
            $('#refresh-data .fas').removeClass('fa-spin');
        });
    }

    var tmpReport = '';

    $.get(urlTemplateRender + 'automation-report.html', function (template) {
        tmpReport = template;
    });
    getReportTest();

    function getReportTest() {
        feature_name = 'test';
        elapsed = 100;
        scenarios_count = 50;
        fails_count = 5;
        success_count = 10;
        errors_count = 15;
        skips_count = 20;
        details = 'sss';
        var ctxD = document.getElementById("doughnutChart").getContext('2d');
        var myLineChart = new Chart(ctxD, {
            type: 'doughnut',
            data: {
                labels: ["Fail", "Success", "Error", "Skip"],
                datasets: [{
                    data: [fails_count, success_count, errors_count, skips_count],
                    backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1"],
                    hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5"],
                    borderWidth: 0
                }]
            },
            options: {
                responsive: true,
                legend: {
                    display: false
                },
            }
        });
        $('#modal-show-report').find('.modal-title').html('Automation Report');
        $('#modal-show-report').find('.top-report .feature-name').html(feature_name);
        $('#modal-show-report').find('.center-chart .fails_count').html(fails_count);
        $('#modal-show-report').find('.center-chart .success_count').html(success_count);
        $('#modal-show-report').find('.center-chart .errors_count').html(errors_count);
        $('#modal-show-report').find('.center-chart .skips_count').html(skips_count);
        $('#modal-show-report').find('.right-chart .scenarios_count').html(scenarios_count);
        $('#modal-show-report').find('.right-chart .elapsed_count').html(elapsed);
        var htmlTableReportDetail = '';
        htmlTableReportDetail += '<tr>';
        htmlTableReportDetail += '<td>' + details + '</td>';
        htmlTableReportDetail += '<td>' + details + '</td>';
        htmlTableReportDetail += '<td>' + details + '</td>';
        htmlTableReportDetail += '<td>' + details + '</td>';
        htmlTableReportDetail += '</tr>';
        $('#modal-show-report').find('.detail-chart table tbody').html(htmlTableReportDetail);
        $('#modal-show-report').modal('show');
    }

    function getReport(data) {
        if (data && data != "") {
            if (data === 'false') {
                showNotifyCustoms("Something went wrong with your scenario configuration.</br>To Debug, please go back <a target='_blank' href='" + projectBaseUrl + JIRAticket_key + "'>" + JIRAticket_key + "</a>", "danger", "right", 0);
            } else {
                var testcase_results = data;
                feature_name = testcase_results['suite_name'];
                elapsed = testcase_results['elapsed'];
                scenarios_count = testcase_results['scenarios'];
                fails_count = testcase_results['fails'];
                success_count = testcase_results['success'];
                errors_count = testcase_results['errors'];
                skips_count = testcase_results['skips'];
                details = testcase_results['details'];
                var ctxD = document.getElementById("doughnutChart").getContext('2d');
                var myLineChart = new Chart(ctxD, {
                    type: 'doughnut',
                    data: {
                        labels: ["Fail", "Success", "Error", "Skip"],
                        datasets: [{
                            data: [fails_count, success_count, errors_count, skips_count],
                            backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1"],
                            hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5"],
                            borderWidth: 0
                        }]
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: false
                        },
                    }
                });
                $('#modal-show-report').find('.modal-title').html('Automation Report');
                $('#modal-show-report').find('.top-report .feature-name').html(feature_name);
                $('#modal-show-report').find('.center-chart .fails_count').html(fails_count);
                $('#modal-show-report').find('.center-chart .success_count').html(success_count);
                $('#modal-show-report').find('.center-chart .errors_count').html(errors_count);
                $('#modal-show-report').find('.center-chart .skips_count').html(skips_count);
                $('#modal-show-report').find('.right-chart .scenarios_count').html(scenarios_count);
                $('#modal-show-report').find('.right-chart .elapsed_count').html(elapsed);
                var htmlTableReportDetail = '';
                $.each(details, function (index, value) {
                    htmlTableReportDetail += '<tr>';
                    htmlTableReportDetail += '<td>' + details[index].name + '</td>';
                    htmlTableReportDetail += '<td>' + details[index].result + '</td>';
                    htmlTableReportDetail += '<td>' + details[index].error_detail + '</td>';
                    htmlTableReportDetail += '<td>' + details[index].time + '</td>';
                    htmlTableReportDetail += '</tr>';
                })
                $('#modal-show-report').find('.detail-chart table tbody').html(htmlTableReportDetail);
                $('#modal-show-report').modal('show');
            }
        } else {
            showNotifyCustoms("No data", "danger", "center", 3000);
        }
    }

    $('.debug_btn').on('click', function () {
        $.notifyClose();
        var url = requestURL + '/execution_automation/' + planId;
        var type = $(this).attr('action_type');
        var data = {
            'execution_type': 'test',
            'action_type': type
        };
        var method = 'GET';
        if (type == 'stop') {
            method = 'POST';
            url = requestURL + '/execution_automation/' + planId + '?execution_type=test&action_type=' + type;
            data = '';
        }
        var that = this;
        if (type != 'execute') {
            if ($(that).attr('disabled') == 'disabled') {
                return false;
            } else {
                $(that).attr({
                    'disabled': 'disabled'
                });
            }
            if (type == 'stop') {
                $('#modalConfirmStop').modal('show');
                $('#modalConfirmStop .modal-body').html('<p style="padding-left:5px; margin-bottom:0;">Do you want to stop?</p>');
                $('#stopSure').attr({
                    'data-id': planId
                });
                $(that).removeAttr('disabled');
            } else {
                getDebugActionResult(url, data, method, that, type);
            }
        } else {
            method = 'POST';
            url = requestURL + '/execution_automation/' + planId + '?execution_type=test&action_type=' + type;
            data = '';
            showSpinner(that);
            ajaxRequest(requestURL + '/execution_automation/' + planId, 'GET', {
                'execution_type': 'test',
                'action_type': 'status'
            }).done(function (res) {
                hideSpinner(that);
                if (res.message != 'running') {
                    execute_Object.url = url;
                    execute_Object.method = method;
                    $('#modal-exec-info').modal('show');
                } else {
                    alert('running!');
                }
            }).catch(function (err) {
                hideSpinner(element);
                $('#refresh-data .fas').removeClass('fa-spin');
            });
        }
    });

    function showSpinner(that) {
        $(spinner).hide();
        $(that).append(spinner);
        $(that).find('.spinner').fadeIn();
    }

    function hideSpinner(that) {
        $(that).find('.spinner').fadeOut("slow", function () {
            $(that).find('.spinner').remove();
        });
    }

    function initData() {
        // log = CodeMirror.fromTextArea(document.getElementById("txt-show-log"), {
        //     lineNumbers: false,
        //     mode: "application/x-powershell",
        //     readOnly: "nocursor",
        //     theme: "base16-light",
        //     autofocus: true
        // });

        var params = {
            cycle_id: cycleId,
            issue_id: issueId
        };
        var allDefer = [];
        var deferTestExcution = $.Deferred();

        allDefer.push(deferTestExcution.promise());

        var deferredInit = [];
        var defferGetStatus = $.Deferred();
        deferredInit.push(defferGetStatus.promise());

        function getStatus() {
            return ajaxRequest(requestURL + '/status', "GET", null);
        }

        getStatus().done(function (response) {
            if (response) {
                arrStatus = response;
                $.each(response, function () {
                    var obj = this;
                    var option = $('<option value="' + obj.id + '">' + obj.name + '</option>');
                    $('#drpStatus').append(option);
                });
            }
            defferGetStatus.resolve();
            $('#drpStatus').selectpicker();
        });

        var defferGetTestExcution = $.Deferred();
        deferredInit.push(defferGetTestExcution.promise());
        ajaxRequest(requestAPI.getTestExecution, "GET", params).done(function (res) {
            testExecution = res[0];
            defferGetTestExcution.resolve();
        });

        $.when.apply($, deferredInit).done(function () {
            $('#drpStatus').selectpicker('val', testExecution.status_id);
            var defects = [];
            if(testExecution.defects != ''){
                var arrDefects = testExecution.defects != null ? testExecution.defects.split('|') : [];
                $.each(arrDefects, function (index, value) {
                    if (value !== '') {
                        defects.push(value);
                    }
                });
            } 
            var tmpl = '';
            var colorStatusBug;
            var arrDef = [];
            $.each(defects, function () {
                var _defDefect = $.Deferred();
                _defDefect.promise();
                arrDef.push(_defDefect);
                var _defect = this;
                var sumaryAndName = "";
                if (_defect != '' && typeof _defect != 'undefined') {
                    // console.log('_defect', _defect);
                    AP.request(baseUrl + '/rest/api/2/search?jql=issue=\'' + _defect + '\'&fields=summary,status', {
                        success: function (res) {
                            var datasource = JSON.parse(res);
                            var issue = datasource && datasource["issues"].length && datasource["issues"]["0"];
                            var status = issue && issue.fields ? issue.fields.status : undefined;
                            var statusColor;
                             var summaryTextBug = issue.fields.summary;
                            sumaryAndName = 'Summary : '+summaryTextBug.toString() +"&#013;"+ 'Status : '+status.name.toString();
                            colorStatusBug = status.name;
                            if (colorStatusBug == "To Do") {
                                statusColor = "statusToDoColor";
                            } else if (colorStatusBug == "Done") {
                                statusColor = "statusDoneColor";
                            } else {
                                statusColor = "statusInProgressColor";
                            }
                            tmpl += '<li class="list-inline-item"><span  class="defects" data-toggle="tooltip"  title="' + sumaryAndName + '"><a class="ticket ' + statusColor + '" target="_blank" href=' + projectBaseUrl + _defect +
                                '>' + _defect + '</a><a class="update-defect remove-ticket ' + statusColor + '" data-issue="' + _defect +
                                '" onclick="removeDefectInMainExecuton(this)" href="javascript:;">x</a></span></li>';
                            _defDefect.resolve();
                        },
                        error: function () {
                            _defDefect.reject();
                        }
                    });

                }

            });

            // arrDef[done1,done2,done3]==> execute next code
            $.when.apply($, arrDef).done(function () {
                ulMainDefect.prepend(tmpl);
            });

            onKeyUpOfSearchMainDefect();
            var user = lodash.filter(userJira, function (o) {
                return o.emailAddress === testExecution.assigned_to;
            });
            $('#dropdownAssignee').selectpicker('val', user.length > 0 ? user[0].emailAddress : '');
            $('#comment').val(testExecution.comment);
            oldContentPlanComment = testExecution.comment;

            //finish get test execution
            deferTestExcution.resolve();
        });

        //Automation Execution History
        var deferredInit2 = [];
        var defferGetAutomationExecutionHistory = $.Deferred();
        deferredInit2.push(defferGetAutomationExecutionHistory.promise());
        ajaxRequest(requestAPI.autoTestCase, "GET", {
            issue_id: issueId
        }).done(function (res) {
            $.each(res, function () {
                var diff = $.Deferred();
                deferredInit2.push(diff.promise());
                var tc = this;
                var testcase_id = tc.id;
                ajaxRequest(requestAPI.autoTestCaseExecution + "?auto_testcase_id=" + testcase_id + "&cycle_issue_execution_id=" + planId, "GET", null).done(function (res) {
                    $.each(res, function () {
                        var tc = this;
                        var testcase_execution_id = tc.id;
                        ajaxRequest(requestAPI.autoTestCaseExecution + testcase_execution_id + "/execution_history", "GET", null).done(function (res) {
                            var d = [];
                            $.each(res, function () {
                                var _d = $.Deferred();
                                d.push(_d.promise());
                                var tce = this;
                                var objAutoExecHistory = {
                                    field_name: tce.field_name,
                                    original_value: tce.old_value,
                                    new_value: tce.new_value
                                };
                                automationExecutionHistoryTable.row.add(objAutoExecHistory);
                                _d.resolve();
                            });
                            diff.resolve();
                            $.when.apply($, d).done(function () {
                                defferGetAutomationExecutionHistory.resolve();
                            });
                        });

                    });
                });
            });
        });

        $.when.apply($, deferredInit2).done(function () {
            setTimeout(function () {
                // automationHistoryTable.draw();
                automationExecutionHistoryTable.columns.adjust().draw();
            }, 1000);
        });

        // var deferredInit = [];

        // var defferGetTestExcution = $.Deferred();
        // deferredInit.push(defferGetTestExcution.promise());
        // ajaxRequest(requestAPI.getTestExecution, "GET", _params).done(function (res) {
        //     testExecution = res[0];
        //     defferGetTestExcution.resolve();
        // });

        // Get testcase by issue id
        ajaxRequest(requestAPI.autoTestCase, "GET", {
            issue_id: issueId
        }).done(function (res) {
            $('#scenario').val(res[0].gherkin_content);
            rawVersionScenario = setupGherkin('scenario');
        });

        // $.when.apply($, deferredInit).done(function () {
        //     $('#defects').val(testExecution.defects);
        //     // var user = lodash.filter(userJira, function (o) {
        //     //     return o.emailAddress === testExecution.assigned_to;
        //     // });
        //     $('#comment').val(testExecution.comment);
        //     oldContentPlanComment = testExecution.comment;
        // });

    }

    initData();

    $('#searchMainDefect').autoComplete({
        minChars: 1,
        cache: false,
        source: function (term, suggest) {
            AP.request({
                url: '/rest/api/2/issue/picker',
                type: 'GET',
                data: {
                    query: term,
                    currentProjectId: jiraProjectId,
                    showSubTasks: true,
                    showSubTaskParent: true
                },
                contentType: 'application/json',
                success: function (res) {
                    $('#add_defect .spinner').remove();
                    term = term.toLowerCase();
                    var suggestions = [];
                    var arrOldDefect = testExecution.defects != null ? testExecution.defects.split("|") : [];
                    var datasource = JSON.parse(res).sections[0].issues;
                    for (var i = 0; i < datasource.length; i++) {
                        var key = datasource[i].key.toUpperCase();
                        if ((~datasource[i].key.toLowerCase().indexOf(term) || ~datasource[i].summaryText.toLowerCase().indexOf(term)) && !arrOldDefect.includes(key)) {
                            suggestions.push(datasource[i]);
                        }
                    }
                    if (suggestions.length == 0) {
                        suggest([{
                            id: -1,
                            message: "No issues found. Create new?"
                        }]);
                    } else
                        suggest(suggestions);
                },
                error: function (xhr, statusText, errorThrown) {
                    suggest([{
                        id: -1,
                        message: "No issues found. Create new?"
                    }]);
                }
            });
        },
        renderItem: function (item, search) {
            search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
            if (item.id == -1) {
                return '<div title="' + item.message + '" class="autocomplete-suggestion" data-values="-1">' + item.message + '</div>';
            }
            return '<div class="autocomplete-suggestion" data-values="' + item.key + '">' +
                '<span>' + item.key.replace(re, "<b>$1</b>") + '</span> - ' +
                unescape(escape(item.summaryText)).replace(re, "<b>$1</b>") + '</div>';
        },
        onSelect: function (e, term, item) {
            if ($('#add_defect .spinner').length == 0) {
                $('#add_defect').find('label').append(spinner);
            }
            if (item.data("values") == -1) {
                openPanelCreateIssueDialog();
            } else {
                var tmpMainDefect = testExecution.defects != null ? testExecution.defects.split('|') : [];
                tmpMainDefect.push(item.data("values"));
                testExecution.defects = lodash.join(tmpMainDefect, '|');
                inputSearchMainDefect.val('');
                inputSearchMainDefect.focus();
                updateDefectMainTestCase(cycleId, issueId, tmpMainDefect).done(function () {
                    updateHTMLMainDefects(tmpMainDefect);
                });
            }
        }
    });

    $('#_searchMainDefect').on('blur', function () {
        $('#searchMainDefect').trigger('blur.autocomplete');
    });

    $('#addNewIssue').click(function () {
        openPanelCreateIssueDialog();
    })

    $('.w_add_defect_main').on('click', function () {
        $('#_searchMainDefect').focus();
    });

});

function openPanelCreateIssueDialog() {
    $('#add_defect .spinner').remove();
    AP.jira.openCreateIssueDialog(function callback(issues) {
        console.info('issues', issues);
        linkedToIssue(JIRAticket_key, issues[0],JiraTitle,JIRAType);
        if (issues.length > 0) {
            var tmpMainDefect = testExecution.defects != null ? testExecution.defects.split('|') : [];
            tmpMainDefect.push(issues[0].key);
            testExecution.defects = lodash.join(tmpMainDefect, '|');
            updateDefectMainTestCase(cycleId, issueId, tmpMainDefect).done(function () {
                updateHTMLMainDefects(tmpMainDefect);
                inputSearchMainDefect.val('').blur();
            });
            statusSumaryBug = issues[0].fields.status.name + "-" + issues[0].fields.summary;
        }
    }, {
        pid: jiraProjectId
    });
    return;
}

function resizeColumnTable() {
    var pressed = false;
    var start;
    var startX, startWidth;

    $("table th").mousedown(function (e) {
        start = $(this);
        pressed = true;
        startX = e.pageX;
        startWidth = $(this).width();
        $(start).addClass("resizing");
    });

    $(document).mousemove(function (e) {
        if (pressed) {
            $(start).width(startWidth + (e.pageX - startX));
        }
    });

    $(document).mouseup(function () {
        if (pressed) {
            $(start).removeClass("resizing");
            pressed = false;
        }
    });
}

function getTestCaseExcutionHistory(res, tcExecHistoryTable) {
    ajaxRequest(requestAPI.testCaseExecution + res.id + "/execution_history", "GET", null).done(function (ress) {
        var lastObj = lodash.last(ress);
        var objTestExecHistory = {
            field_name: lastObj.field_name,
            original_value: lastObj.old_value,
            new_value: lastObj.new_value,
            updated_at: lastObj.updated_at
        };
        tcExecHistoryTable.row.add(objTestExecHistory);
        tcExecHistoryTable.draw(false);
    });
}
/**
 *
 * @param {*} cycleId
 * @param {*} issueId
 * @param {array} issueKey: array of Issues/Defects
 */
function updateDefectMainTestCase(cycleId, issueId, issueKey) {
    var strIssue = lodash.join(issueKey, '|');
    if ($('#add_defect .spinner').length == 0) {
        $('#add_defect').find('label').append(spinner);
    }
    return updatePlanTestCase(cycleId, issueId, {
        defects: strIssue
    });
}

function updateHTMLMainDefects(_tmpMainDefect) {
    var defects = [];
        $.each(_tmpMainDefect, function (index, value) {
            if (value !== '') {
                defects.push(value);
            }
        });
    var tmpl = '';
    var colorStatusBug;
    var arrDef = []; //Arry[waiting1, waiting2,wating3] ==> Array[done1,done2,done3]==> execute next code
    $.each(defects, function () {
        var _defDefect = $.Deferred();
        _defDefect.promise();
        arrDef.push(_defDefect);
        var _defect = this;
        var sumaryAndName = "";
        if (_defect != '' && typeof _defect != 'undefined') {
            // console.log('_defect', _defect);
            AP.request(baseUrl + '/rest/api/2/search?jql=issue=\'' + _defect + '\'&fields=summary,status', {
                success: function (res) {
                    var datasource = JSON.parse(res);
                    var issue = datasource && datasource["issues"].length && datasource["issues"]["0"];
                    var status = issue && issue.fields ? issue.fields.status : undefined;
                    var summaryTextBug = issue.fields.summary;
                    var statusColor;
                    sumaryAndName = 'Summary : '+summaryTextBug.toString() + "&#013;" + 'Status : '+status.name.toString();
                    // console.log("sumaryAndName", sumaryAndName);
                    colorStatusBug = status.name;
                    if (colorStatusBug == "To Do") {
                        statusColor = "statusToDoColor";
                    } else if (colorStatusBug == "Done") {
                        statusColor = "statusDoneColor";
                    } else {
                        statusColor = "statusInProgressColor";
                    }
                    tmpl += '<li class="list-inline-item"><span  class="defects" data-toggle="tooltip"  title="' + sumaryAndName + '"><a class="ticket ' + statusColor + '" target="_blank" href=' + projectBaseUrl + _defect +
                        '>' + _defect + '</a><a class="update-defect remove-ticket ' + statusColor + '" data-issue="' + _defect +
                        '" onclick="removeDefectInMainExecuton(this)" href="javascript:;">x</a></span></li>';
                    _defDefect.resolve();
                },
                error: function () {
                    _defDefect.reject();
                }
            });

        }

    });

    // arrDef[done1,done2,done3]==> execute next code
    $.when.apply($, arrDef).done(function () {
        ulMainDefect.find('>li').not('.search-issue').remove();
        ulMainDefect.not('>li').prepend(tmpl);
        $('#add_defect .spinner').remove();
    });
}

function updateTestCaseDetail(id, data) {
    var _data = Object.assign({
        modified_by: currentUser.emailAddress
    }, data);
    return ajaxRequest(requestAPI.updateAutoTestCaseExecution + id, "PUT", JSON.stringify(_data));
}

function updateHighOfTxtComment(that) {
    var content = $(that).val();
    var line = content.substr(0, content.length).split('\n').length;
    $(that).attr('rows', line);
}

function updateAssigneeOfPlan(cycleId, issueId, email) {
    $('#add_assign').find('label').append(spinner);
    var that = this;
    updatePlanTestCase(cycleId, issueId, {
        assigned_to: email
    }).done(function (res) {
        $('#add_assign').find('.spinner').remove();
        var user = lodash.filter(userJira, function (o) {
            return o.emailAddress == email;
        });
        $('#dropdownAssignee').selectpicker('val', user[0].emailAddress);
    });
}

var addMainDefect = $('#add_defect .w_add_defect_main');
var ulMainDefect = $('#add_defect .w_add_defect_main ul.list-inline');
var liSearchMainDefect = $('#add_defect .search-issue');
var inputSearchMainDefect = $('#_searchMainDefect');
var canvas = document.getElementById("tempCanvas");
var ctx = canvas.getContext("2d");
ctx.font = "14px Arial";

function onKeyUpOfSearchMainDefect() {
    $('#searchMainDefect').val(inputSearchMainDefect.val());
    if ($('#add_defect .spinner').length == 0) {
        $('#add_defect').find('label').append(spinner);
    }
    if (inputSearchMainDefect.val().length < 3) {
        $('#add_defect .spinner').remove();
    }
    $('#searchMainDefect').trigger('keyup.autocomplete');
    var widthText = ctx.measureText(inputSearchMainDefect.val()).width;
    var w = addMainDefect.width();
    if (widthText < w) {
        if (widthText < 88)
            liSearchMainDefect.css('width', 88);
        else liSearchMainDefect.css('width', widthText + 10);
    } else liSearchMainDefect.css('width', w);
}

function removeDefectInMainExecuton(that) {
    var currentDefects = testExecution.defects != null ? testExecution.defects.split('|') : [];
    lodash.remove(currentDefects, function (o) {
        return o == $(that).data('issue');
    });
    testExecution.defects = lodash.join(currentDefects, '|');
    $(that).closest('.list-inline-item').remove();
    updateDefectMainTestCase(cycleId, issueId, currentDefects).done(function () {
        // updateHTMLMainDefects(currentDefects);
        $('#add_defect .spinner').remove();
    });
}