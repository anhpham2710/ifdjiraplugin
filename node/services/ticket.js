/*jshint esversion: 6 */

var COMMON = require('../common/common');
var projectService = require('../services/project');

function createTicket(ticketType) {
    'use strict';
}

function getAllIssuesFromJiraByJiraProjectKey(JIRAprojectKey, httpClient) {
    var defGetIssues = COMMON.Q.defer();
    httpClient.get('/rest/api/2/search?jql=project=\'' + JIRAprojectKey + '\'&fields=summary,issuetype',
        (err, response, body) => {
            var tescaseBody = JSON.parse(body);
            if (tescaseBody.code === 404) {
                defGetIssues.reject(err);
            } else {
                defGetIssues.resolve(tescaseBody.issues);
            }

        });
    return defGetIssues.promise;
}

function getIssueFromJira(jiraIssueKey, httpClient) {
    'use strict';
    var defGetIssues = COMMON.Q.defer();
    httpClient.get(`/rest/api/2/issue/${jiraIssueKey}?fields=summary,description,issuetype,status`,
        (err, response, issue) => {
            defGetIssues.resolve(issue);
        });
    return defGetIssues.promise();
}

function getAllStepDefinition() {

}

module.exports = {
    createTicket: createTicket,
    getIssueFromJira: getIssueFromJira,
    getAllIssuesFromJiraByJiraProjectKey: getAllIssuesFromJiraByJiraProjectKey,
    getAllStepDefinition: getAllStepDefinition
};