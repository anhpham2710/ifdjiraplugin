/*jshint esversion: 6 */
var COMMON = require('../common/common');

function createCompany(company_name, companyURL, clientKey) {
    var defer = COMMON.Q.defer();
    COMMON.generateToken(companyURL, clientKey, "post").then(function (body) {
        COMMON.request.post({
            url: COMMON.companyURL,
            headers: {
                "Authorization": "JWT " + body.token
            },
            json: {
                "name": company_name,
                "status": "active"
            }
        }, (res) => {
            defer.resolve(res);
        });
    });
    return defer.promise;
}

function updateCompany(company_name, companyURL, clientKey) {
    var url = companyURL + "?company_name=" + company_name;
    var defer = COMMON.Q.defer();
    getCompany(url, clientKey).done((res) => {
        companyId = res.id;
        var updateCompanyURL = companyURL + companyId;
        COMMON.generateToken(updateCompanyURL, clientKey, "put")
            .then(function (body) {
                COMMON.request.put({
                    url: updateCompanyURL,
                    headers: {
                        "Authorization": "JWT " + body.token
                    },
                    json: {
                        "status": "active"
                    }
                }, (res) => {
                    defer.resolve(res);
                });
            });
    });
    return defer.promise;
}

function getCompany(urlCompany, clientKey) {
    var defer = COMMON.Q.defer();
    COMMON.generateToken(urlCompany, clientKey, "get").then(function (body) {
        var options = {
            headers: {
                "Authorization": "JWT " + body.token
            }
        };
        COMMON.request.get(urlCompany, options, function (err, response, body) {
            if (response.statusCode == 404) {
                // console.log("======== Get company Error =======");
                // console.log(err);
                defer.reject(response.statusCode);

            } else if (response.statusCode == 200) {
                // console.log("======== Get company Success =======");
                // console.log(body);
                // COMMON.request to update company status
                defer.resolve(JSON.parse(body)[0]);
            }
        });

    });
    return defer.promise;
}
module.exports = {
    createCompany: createCompany,
    getCompany: getCompany,
    updateCompany: updateCompany
};