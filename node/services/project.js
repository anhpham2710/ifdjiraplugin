/*jshint esversion: 6 */
var COMMON = require('../common/common');
var companyService = require('../services/company');

function createProject(jiraProject, companyId) {
    var defer = COMMON.Q.defer();
    COMMON.generateToken(COMMON.projectURL, clientKey, "post")
        .then((tokenPostProject) => {
            var JIRAprojectName = jiraProject.name;
            var JIRAprojectKey = jiraProject.key;
            var JIRAprojectId = jiraProject.id;

            COMMON.request.post({
                url: COMMON.projectURL,
                headers: {
                    "Authorization": "JWT " + tokenPostProject.token
                },
                json: {
                    "JIRAproject_key": JIRAprojectKey,
                    "JIRAproject_name": JIRAprojectName,
                    "JIRAproject_id": JIRAprojectId,
                    "company_id": companyId
                }
            }, (res) => {
                defer.resolve(res);
            });
        });


    return defer.promise;
}

function getAllProjectFromJira(httpClient) {
    var defer = COMMON.Q.defer();
    httpClient.get('/rest/api/2/project', function (err, res, projectObj) {
        var parsed_projects = JSON.parse(projectObj);
        defer.resolve(parsed_projects);
    });
    return defer.promise;
}

function getProject(urlProjectByCompanyByProjectID) {
    'use strict';

    var def = COMMON.Q.defer();
    COMMON.generateToken(urlProjectByCompanyByProjectID, "get")
        .then((body) => {
            var options = {
                headers: {
                    "Authorization": "JWT " + body.token
                }
            };
            COMMON.request.get(urlProjectByCompanyByProjectID, options, (err, response, project_body) => {
                if (err) {
                    def.reject(err);
                }
                def.resolve(JSON.parse(project_body)[0]);
            });
        })
        .catch((err) => {
            def.reject(err);
        });
    return def.promise();
}

function checkProjectInDB(queryProjectByCompanyByProjectID, jiraProject, clientKey) {

    var defer = COMMON.Q.defer();
    COMMON.generateToken(queryProjectByCompanyByProjectID, clientKey, "get").then(function (resToken1) {
        var options1 = {
            headers: {
                "Authorization": "JWT " + resToken1.token
            }
        };
        COMMON.request.get(queryProjectByCompanyByProjectID, options1,
            (err, project_response, project_body) => {
                var _projectResponseBody = JSON.parse(project_response.body);

                if (project_response.statusCode == 404 ||
                    _projectResponseBody === null ||
                    _projectResponseBody.length === 0) {
                    //---> Get company param, parse from query project by company by project Id.
                    var companyName = queryProjectByCompanyByProjectID.split("?")[1].split("&")[0];
                    var companyNameQuery = "?" + companyName;

                    companyService.getCompany(COMMON.companyURL + companyNameQuery, clientKey).done(company => {
                        createProject(jiraProject, company.id).done(res => {
                            defer.resolve(res);
                        });
                    });
                } else {
                    // console.log("====== Do not Create project =====");
                    defer.resolve();
                }
            });
    });
    return defer.promise;
}

module.exports = {
    checkProjectInDB: checkProjectInDB,
    create: createProject,
    getProject: getProject,
    getAllProjectFromJira: getAllProjectFromJira
};