/*jshint esversion: 6 */
const request = require('request');
const rp = require('request-promise');
const TMS_API_URL = "https://ifdtms.infodation.com:1235/api/v1";
const Q = require('q');
var httpClient = null;
var clienKey = null;
var hostBaseUrl = null;
function generateToken(requestURL, clientKey, method) {
    var options = {
        method: "POST",
        uri: TMS_API_URL + "/addon/generate_token",
        json: {
            "client_key": clientKey,
            "http_method": method,
            "request_url": requestURL
        }
    };
    return rp(options);
}

function getAllStepDefinitionByProjectId(queryAllStepsByProjectId, clientKey) {

    var objSteps = null;
    var defGetStepDefinition = Q.defer();

    generateToken(queryAllStepsByProjectId, clientKey, "get").then((body) => {
        var options = {
            headers: {
                "Authorization": "JWT " + body.token
            }
        };
        request.get(queryAllStepsByProjectId, options, (err, response, steps) => {
            if (err) {
                console.log('get all step definitions error', err);
                defGetStepDefinition.reject(err);
            }
            console.log('get all step definitions work ok');
            objSteps = JSON.parse(steps);
            if (objSteps.code === 404) {
                objSteps = null;
            }
            defGetStepDefinition.resolve(objSteps);
        });
    });
    return defGetStepDefinition.promise;
}

module.exports = {
    request: request,
    TMS_API_URL: TMS_API_URL,
    generateToken: generateToken,
    companyURL: TMS_API_URL + "/company/",
    projectURL: TMS_API_URL + "/project/",
    issueURL: TMS_API_URL + "/issue/",
    Q: Q,
    httpClient: httpClient,
    clienKey: clienKey,
    hostBaseUrl:hostBaseUrl,
    getAllStepDefinitionByProjectId: getAllStepDefinitionByProjectId
};
